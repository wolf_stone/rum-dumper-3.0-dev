#include <cstdlib>
#include <log4cplus/logger.h>
#include <log4cplus/configurator.h>
#include <log4cplus/loggingmacros.h>
//#include <log4cplus/ndc.h>
//#include <log4cplus/layout.h>
//include <log4cplus/helpers/loglog.h>
//#include <log4cplus/helpers/stringhelper.h>

using namespace std;
using namespace log4cplus;
//using namespace log4cplus::helpers;

Logger logger = Logger::getInstance(LOG4CPLUS_TEXT("DispatcherFileLogger.DISPLOG"));
Logger logger2 = Logger::getInstance(LOG4CPLUS_TEXT("SchedulerFileLogger.SCHDLOG"));    

int main()
{

    PropertyConfigurator::doConfigure(LOG4CPLUS_TEXT("/etc/rum/log.properties"));

    //logger.setLogLevel(WARN_LOG_LEVEL);
    
    //subTest.setLogLevel(ERROR_LOG_LEVEL);

    for(int i=0; i<1000000000000000;i++)    {        
        LOG4CPLUS_INFO(logger, "Entering loop # " << i);
        LOG4CPLUS_WARN(logger, "Entering loop ##" << i);
        LOG4CPLUS_ERROR(logger, "Entering loop ###" << i);
        LOG4CPLUS_FATAL(logger, "Entering loop ####" << i);
        LOG4CPLUS_DEBUG(logger, "Entering loop #####" << i);

        LOG4CPLUS_INFO(logger2, "Entering loop #" << i);
        LOG4CPLUS_WARN(logger2, "Entering loop ##" << i);
        LOG4CPLUS_ERROR(logger2, "Entering loop ###" << i);
        LOG4CPLUS_FATAL(logger2, "Entering loop ####" << i);
        LOG4CPLUS_DEBUG(logger2, "Entering loop #####" << i);
    }        

    return 0;
}
