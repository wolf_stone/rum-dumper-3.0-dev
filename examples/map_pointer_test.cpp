#include <iostream>
#include <stdlib.h>
#include "dumper_common.h"
#include "SessionHash.h"

RumSessionHashlist* gpHashListProducer = NULL;

int main()
{
    rum_session_timebrk_t t1, t2;
    t1.service_id = 1;
    t2.service_id = 2;
    std::list<rum_session_timebrk_t> ll;
    ll.push_back(t1);
    ll.push_back(t2);

    std::cout << "sizeof SessionHashlist: " << sizeof(SessionHashlist) << std::endl;

    //gpHashListProducer = (SessionHashlistPtr) malloc (sizeof(SessionHashlist));
    //
    gpHashListProducer = new(std::nothrow) RumSessionHashlist();
    if( gpHashListProducer == NULL )
    {
        std::cout << "Failed to alloc memory for gpHashListProducer" << std::endl;
    }

    /*(*gpHashListProducer)[1] =  ll;
    std::cout << "key counts in SessionHashlist: " << (*gpHashListProducer).size() << std::endl;
    if( gpHashListProducer == NULL )
    {
        std::cout << "Failed to alloc memory for gpHashListProducer" << std::endl;
    } 
    //(*gpHashListProducer)[1] =  ll;
    //std::cout << *gpHashListProducer;
    */
    delete gpHashListProducer;
    return 0;
}
