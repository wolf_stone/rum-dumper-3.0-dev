#include <iostream>
#include "Config.h"  

int main()  
{  
    std::string redisIP;  
    std::string dbIP;  
    std::string redisPort;  
    std::string dbPort;  
    std::string logLevel;  
    const char ConfigFile[]= "/etc/rum/rum-server.conf";   
    Config configSettings(ConfigFile);  
      
    redisIP = configSettings.Read("redisIP", redisIP);  
    redisPort = configSettings.Read("redisPort", redisPort);  
    logLevel = configSettings.Read("logLevel", logLevel);  
    dbIP = configSettings.Read("dbIP", dbIP);  
    dbPort = configSettings.Read("dbPort", dbPort);  
    std::cout<<"redisIP: "<<redisIP<<std::endl;  
    std::cout<<"redisPort: "<<redisPort<<std::endl;  
    std::cout<<"logLevel:"<<logLevel<<std::endl;  
    std::cout<<"dbIP:"<<dbIP<<std::endl;  
    std::cout<<"dbPort:"<<dbPort<<std::endl;  
      
    return 0;  
}  
