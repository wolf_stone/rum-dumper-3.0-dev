# encoding=UTF-8
"""
Created on 2012-3-28
@author: 永刚
"""

from CommonModules import LogClient
import psycopg2, psycopg2.extras
import time, threading

class MyThread(threading.Thread) :
    def __init__(self, server_ip, user, service_id, logger):
        threading.Thread.__init__(self)
        self.db_host = server_ip
        self.db_user = user
        self.service_id = service_id
        self.logger = logger

    def run(self):
        self.logger.info("%s: data of service_id=%d will been removed " % (threading.currentThread().getName(), service_id))
        service_id = self.service_id
        #conn = psycopg2.connect(database = "cmdb", host=server_ip, port=5432, user=user)
        conn = psycopg2.connect(database = "cmdb", host=self.db_host, port=5432, user=self.db_user)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cmd_string_day = "DELETE FROM day_service_db WHERE service_id=" + str(service_id)
        cmd_string_hour = "DELETE FROM hour_service_db WHERE service_id=" + str(service_id)
        cmd_string_minute = "DELETE FROM minute_service_db WHERE service_id=" + str(service_id)
        cmd_string_event = "DELETE FROM rd_session_event WHERE service_id=" + str(service_id)

        try:
            cursor.execute( cmd_string_day )
            cursor.execute( cmd_string_hour )
            cursor.execute( cmd_string_minute )
            cursor.execute( cmd_string_event )
            self.logger.info("%s: data of service_id=%d have been removed successfully" % (threading.currentThread().getName(),service_id))
            conn.commit()
            cursor.close()
            if conn != None :
                conn.close()
            return 0

        except Exception, e :
            cursor.close()
            errMsg = "[delete service related data failed:"
            errMsg = errMsg + "service_id=" + str(service_id)
            errMsg = errMsg + "]"
            self.logger.error(errMsg)
            errmsg = str(e)
            self.logger.error("with reason: %s" % errmsg)
            return -1


class DatabaseClient:
    def __init__(self, server_ip = "127.0.0.1", server_port = 5432, user = 'postgres',password='', logger = None, db='cmdb'):
        self.server_ip = server_ip
        self.server_port = server_port
        self.db_user = user
        self.db_password = password

        self.errmsg=""
        self.conn = None
        if logger == None :
            self.logger = LogClient("db.log")
        else :
            self.logger  = logger

        try:
            self.conn = psycopg2.connect(database = db, host=server_ip, port=server_port, user=user, password=password)
        except Exception, e:
            self.logger.error("failed to initilize database")
            self.errmsg = str(e)
            self.logger.error("with reason: %s" % self.errmsg)
            return None
#######

    def getErrMsg(self) :
        return self.errmsg

#######

    def isConnected(self) :
        try :
            pid = self.conn.get_backend_pid()
            return True
        except Exception, e:
            self.logger.info("conn object have been destroyed")
            self.logger.info("with reason: %s" % str(e))
            return False

    def confirmConnected(self) :
        if self.isConnected() :
            return True
        else :
            self.logger.warning("conn object disconnnected")
            try :
                self.conn = psycopg2.connect(database = "cmdb", host=server_ip, port=server_port, user=user, password=password)

            except Exception, e:
                self.logger.error("Failed to reconnect DB")
                self.errmsg = str(e)
                self.logger.error("with reason = %s" % self.errmsg)
                return False

######

    def addServiceInfo(self, service_info):      #return service_id

        if self.confirmConnected() == False :
            self.logger.error("Can not connect to DB, please check the status of DataBase")
            return -1

        cursor = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cmd_string = "INSERT INTO rd_service_db(service_name,service_ip,service_port,service_type \
                    ,service_url,performance_zero,performance_full \
                    ,performance_lower,performance_upper,availability_lower,availability_upper,collector_id \
                    ,event_period, event_counts_warning, event_counts_critical) \
                    VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) RETURNING service_id;"

        try :
            cursor.execute( cmd_string,(service_info['service_name'] \
                        ,service_info['service_ip'],service_info['service_port'] \
                        ,service_info['service_type'],service_info['service_url'] \
                        ,service_info['performance_zero'],service_info['performance_full'] \
                        ,service_info['performance_lower'],service_info['performance_upper'] \
                        ,service_info['availability_lower'],service_info['availability_upper'] \
                        ,service_info['collector_id'],service_info['event_period'] \
                        ,service_info['event_counts_warning'],service_info['event_counts_critical']))

            self.conn.commit()
            item = cursor.fetchone()
            if item == None:
                cursor.close()
                errMsg = "[addServiceInfo failed:"
                errMsg = errMsg + "service_name=" + service_info['service_name']
                errMsg = errMsg + "]"
                self.logger.error(errMsg)
                return -1
            else:
                cursor.close()
                return item[0]
        except Exception, e :
            errMsg = "[addServiceInfo failed:"
            errMsg = errMsg + "service_name=" + service_info['service_name']
            errMsg = errMsg + "]"
            self.logger.error(errMsg)
            self.errmsg = str(e)
            self.logger.error("with reason: %s" % self.errmsg)
            return -1
#######

    def deleteServiceInfo(self, service_id):
    #return -1 if failed, return 0 if success
        if self.confirmConnected() == False :
            self.logger.error("Can not connect to DB, please check the status of DataBase")
            return -1

        cursor = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cmd_string_service = "DELETE FROM rd_service_db WHERE service_id=" + str(service_id)

        try:
            cursor.execute( cmd_string_service )

            #t = MyThread(self.server_ip,self.db_user,service_id,self.logger)
            #t.start()
            self.conn.commit()
            cursor.close()
            return 0

        except Exception, e :
            cursor.close()
            errMsg = "[deleteServiceInfo failed:"
            errMsg = errMsg + "service_id=" + str(service_id)
            errMsg = errMsg + "]"
            self.logger.error(errMsg)
            self.errmsg = str(e)
            self.logger.error("with reason: %s" % self.errmsg)
            return -1

    def deleteServiceByCollector(self, collector_id): #when delete collector happens
    #delete all service belones to collector_id
    #return -1 if failed, return 0 if success

        if self.confirmConnected() == False :
            self.logger.error("Can not connect to DB, please check the status of DataBase")
            return -1

        cursor = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cmd_string = "DELETE FROM rd_service_db WHERE collector_id=" + str(collector_id)

        try:
            cursor.execute( cmd_string )
            self.conn.commit()
            cursor.close()
            return 0

        except Exception, e :
            cursor.close()
            errMsg = "[deleteServiceByCollector failed:"
            errMsg = errMsg + "collector_id=" + str(collector_id)
            errMsg = errMsg + "]"
            self.logger.error(errMsg)
            self.errmsg = str(e)
            self.logger.error("with reason: %s" % self.errmsg)
            return -1
######

    def updateServiceInfo(self, service_info):    #service_info here have a more service_id

        if self.confirmConnected() == False :
            self.logger.error("Can not connect to DB, please check the status of DataBase")
            return -1

        cursor = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cmd_string = "UPDATE rd_service_db set service_name=%s,service_ip=%s,service_port=%s \
                    ,service_type=%s,service_url=%s,performance_zero=%s,performance_full=%s \
                    ,performance_lower=%s,performance_upper=%s \
                    ,availability_lower=%s,availability_upper=%s \
                    ,collector_id=%s, event_period=%s \
                    ,event_counts_warning=%s, event_counts_critical=%s \
                    WHERE service_id=%s;"

        try:
            cursor.execute( cmd_string,(service_info['service_name'],service_info['service_ip'] \
                    ,service_info['service_port'],service_info['service_type'],service_info['service_url'] \
                    ,service_info['performance_zero'],service_info['performance_full'] \
                    ,service_info['performance_lower'],service_info['performance_upper'] \
                    ,service_info['availability_lower'],service_info['availability_upper'] \
                    ,service_info['collector_id'],service_info['event_period'] \
                    ,service_info['event_counts_warning'],service_info['event_counts_critical'] \
                    ,service_info['service_id']) )
            self.conn.commit()
            cursor.close()
            return 0

        except Exception, e :
            cursor.close()
            errMsg = "[modifyServiceInfo failed:"
            errMsg = errMsg + "service_name=" + service_info['service_name']
            errMsg = errMsg + "]"
            self.logger.error(errMsg)
            self.errmsg = str(e)
            self.logger.error("with reason: %s" % self.errmsg)
            return -1
######

    def addCollectorInfo(self, collector_info):

        if self.confirmConnected() == False :
            self.logger.error("Can not connect to DB, please check the status of DataBase")
            return -1

        cursor = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cmd_string = "INSERT INTO rd_collector_db(collector_ip,collector_name) \
            VALUES(%s,%s) RETURNING collector_id;"
        try :
            cursor.execute( cmd_string,(collector_info['collector_ip'],collector_info['collector_name']) )
            self.conn.commit()
            item = cursor.fetchone()
            if item == None:
                cursor.close()
                errMsg = "[addCollectorInfo failed:"
                errMsg = errMsg + "collector_ip=" + collector_info['collector_ip']
                errMsg = errMsg + "]"
                self.logger.error(errMsg)
                return -1
            else:
                cursor.close()
                return item[0]

        except Exception, e :
            self.errmsg = str(e)
            self.logger.error("with reason: %s" % self.errmsg)
            return -1
######

    def deleteCollectorInfo(self, collector_id):
    #return -1 if failed, return 0 if success

        if self.confirmConnected() == False :
            self.logger.error("Can not connect to DB, please check the status of DataBase")
            return -1

        cursor = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cmd_string = "DELETE FROM rd_collector_db WHERE collector_id=" + str(collector_id)

        try:
            cursor.execute( cmd_string )
            self.conn.commit()
            cursor.close()
            return 0
        except Exception, e:
            cursor.close()
            errMsg = "[deleteCollectorInfo failed:"
            errMsg = errMsg + "collector_id=" + str(collector_id)
            errMsg = errMsg + "]"
            self.logger.error(errMsg)
            self.errmsg = str(e)
            self.logger.error("with reason: %s" % self.errmsg)
            return -1
######

    def updateCollectorInfo(self, collector_info):

        if self.confirmConnected() == False :
            self.logger.error("Can not connect to DB, please check the status of DataBase")
            return -1

        cursor = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cmd_string = "UPDATE rd_collector_db set collector_ip=%s,collector_name=%s WHERE collector_id=%s;"

        try:
            cursor.execute( cmd_string,(collector_info['collector_ip'],collector_info['collector_name'],collector_info['collector_id']) )
            self.conn.commit()
            cursor.close()
            return 0

        except Exception, e:
            cursor.close()
            errMsg = "[deleteCollectorInfo failed:"
            errMsg = errMsg + "collector_ip=" + collector_info['collector_ip']
            errMsg = errMsg + "]"
            self.logger.error(errMsg)
            self.errmsg = str(e)
            self.logger.error("with reason: %s" % self.errmsg)
            return -1
######

    def loginAuthentication(self, login_info):

        if self.confirmConnected() == False :
            self.logger.error("Can not connect to DB, please check the status of DataBase")
            return None

        self.logger.debug("login_info = %s" % str(login_info))
        cursor = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cmd_string = "SELECT user_group FROM authentication_db WHERE user_name=%s AND password=%s"

        try :
            cursor.execute( cmd_string, (login_info['user_name'],login_info['password']))
            self.conn.commit()
            item = cursor.fetchone()
            if item == None:
                cursor.close()
                #self.conn.close()
                self.logger.warning("user = %s login failed" % login_info['user_name'])
                return None
            else:
                cursor.close()
                #self.conn.close()
                return item[0]

        except Exception, e:
            self.logger.error("DB failed to verify login with user = %s" % login_info['user_name'])
            self.errmsg = str(e)
            self.logger.error("with reason: %s" % self.errmsg)
            return None
######

    def changePassword(self, account_info):

        if self.confirmConnected() == False :
            self.logger.error("Can not connect to DB, please check the status of DataBase")
            return -1

        cursor = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cmd_string = "UPDATE authentication_db set password=%s WHERE user_id=%s AND user_group=%s;"
        self.logger.debug("account_info = %s" % str(account_info))

        try:
            cursor.execute( cmd_string,(account_info['password'],str(account_info['user_id']),account_info['user_group']) )
            self.conn.commit()
            cursor.close()
            return 0

        except Exception, e:
            cursor.close()
            errMsg = "[changePassword failed:"
            errMsg = errMsg + "user_id=" + str(account_info['user_id'])
            errMsg = errMsg + "]"
            self.logger.error(errMsg)
            self.errmsg = str(e)
            self.logger.error("with reason: %s" % self.errmsg)
            return -1
######

    def eventCount(self, start_time, end_time, collector_id):
    #return is a count number (>= 0); -1 if failed

        if self.confirmConnected() == False :
            self.logger.error("Can not connect to DB, please check the status of DataBase")
            return None

        cursor = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        #cmd_string = "SELECT count(*) FROM event_db WHERE event_time>=%s AND event_time<=%s AND collector_id=%s"
        #cmd_string = "SELECT count(*) FROM event_db WHERE start_time>=%s AND end_time<=%s AND collector_id=%s"
        cmd_string = "SELECT count(*) FROM rd_session_event WHERE event_time>=%s AND event_time<=%s AND collector_id=%s"
        try :
            cursor.execute( cmd_string,(start_time, end_time, collector_id))
            self.conn.commit()
            item = cursor.fetchone()
            if item == None:
                cursor.close()
                return -1
            else:
                cursor.close()
                return item[0]

        except Exception, e:
            self.logger.error("DB failed get eventcount")
            self.errmsg = str(e)
            self.logger.error("with reason: %s" % self.errmsg)
            return -1
######

    def eventCountByService(self, start_time, end_time, service_id):
    #return is a count number (>= 0); -1 if failed

        if self.confirmConnected() == False :
            self.logger.error("Can not connect to DB, please check the status of DataBase")
            return -1

        cursor = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cmd_string = "SELECT count(*) FROM rd_session_event WHERE event_time>=%s AND event_time<=%s AND service_id=%s"
        try :
            cursor.execute( cmd_string,(start_time, end_time, service_id))
            self.conn.commit()
            item = cursor.fetchone()
            if item == None:
                cursor.close()
                return -1
            else:
                cursor.close()
                return item[0]

        except Exception, e:
            self.logger.error("DB failed get eventcount by service")
            self.errmsg = str(e)
            self.logger.error("with reason: %s" % self.errmsg)
            return -1
######

    def eventCountByServices(self, start_time, end_time, collector_id):
    #return is a list of dictionary:[{'service_name':'taobao', 'event_count':123},...]

        if self.confirmConnected() == False :
            self.logger.error("Can not connect to DB, please check the status of DataBase")
            return None

        cursor = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        #cmd_string = "SELECT service_id,COUNT(*) FROM event_db WHERE event_time>=%s \
        #      AND event_time<=%s AND collector_id=%s GROUP BY service_id \
        #      ORDER BY service_id"
        cmd_string = "SELECT service_id,COUNT(*) FROM rd_session_event WHERE event_time>=%s \
              AND event_time<=%s AND collector_id=%s GROUP BY service_id \
              ORDER BY service_id"
        try :
            cursor.execute( cmd_string,(start_time, end_time, collector_id))
            li = []
            for record in cursor:
                d = {'service_id':record[0],'event_count':record[1]}
                li.append(d)

            cursor.close()

            return li

        except Exception, e:
            self.logger.error("DB failed get eventcount by services")
            self.errmsg = str(e)
            self.logger.error("with reason: %s" % self.errmsg)
            return None
######

    def eventCountBySeverity(self, start_time, end_time, collector_id):
    #return is a list of dictionary:[{'type_name':'SLA EVENT', 'event_count':123},...]

        if self.confirmConnected() == False :
            self.logger.error("Can not connect to DB, please check the status of DataBase")
            return None

        cursor = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        #cmd_string = "SELECT severity_level,COUNT(*) FROM event_db WHERE event_time>=%s \
        #      AND event_time<=%s AND collector_id=%s GROUP BY severity_level"
        cmd_string = "SELECT severity_level,COUNT(*) FROM rd_session_event WHERE event_time>=%s \
              AND event_time<=%s AND collector_id=%s GROUP BY severity_level"
        try :
            cursor.execute( cmd_string,(start_time, end_time, collector_id))
            li = []
            for record in cursor:
                d = {'severity_level':record[0],'event_count':record[1]}
                li.append(d)

            cursor.close()

            return li

        except Exception, e:
            self.logger.error("DB failed get eventcount by Severity")
            self.errmsg = str(e)
            self.logger.error("with reason: %s" % self.errmsg)
            return None

    def eventCountByType(self, start_time, end_time, collector_id):
    #return is a list of dictionary:[{'type_name':'SLA EVENT', 'event_count':123},...]

        if self.confirmConnected() == False :
            self.logger.error("Can not connect to DB, please check the status of DataBase")
            return None

        cursor = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        #cmd_string = "SELECT severity_level,COUNT(*) FROM event_db WHERE event_time>=%s \
        #      AND event_time<=%s AND collector_id=%s GROUP BY severity_level"
        cmd_string = "SELECT event_type,COUNT(*) FROM rd_session_event WHERE start_time>=%s \
              AND end_time<=%s AND collector_id=%s GROUP BY event_type"
        try :
            cursor.execute( cmd_string,(start_time, end_time, collector_id))
            li = []
            for record in cursor:
                d = {'event_type':record[0],'event_count':record[1]}
                li.append(d)

            cursor.close()

            return li

        except Exception, e:
            self.logger.error("DB failed get eventcount by Severity")
            self.errmsg = str(e)
            self.logger.error("with reason: %s" % self.errmsg)
            return None

######
    def queryEventInfo(self, request_info) :

        if self.confirmConnected() == False :
            self.logger.error("Can not connect to DB, please check the status of DataBase")
            return None

        cursor = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        li = []
        conditions_list = [request_info['start_time']]
        conditions_list.append(request_info['end_time'])
        conditions_list.append(request_info['collector_id'])
        try :
            cmd_string = "SELECT service_id, event_time, event_name, source_ip FROM rd_session_event WHERE event_time>=%s \
                    AND event_time<=%s AND collector_id=%s "
            sufix_string = "ORDER BY event_time DESC"

            request_info.setdefault('service_id',0)
            request_info.setdefault('event_type','ALL')

            if request_info['service_id'] > 0:
                cmd_string = cmd_string + "AND service_id=%s "
                sufix_string = sufix_string + ", service_id"
                conditions_list.append(request_info['service_id'])

            if request_info['event_type'].upper() != 'ALL' :
                cmd_string = cmd_string + "AND event_type=%s "
                conditions_list.append(request_info['event_type'])

            cmd_string = cmd_string + sufix_string

            tt = tuple(conditions_list)

            cursor.execute( cmd_string,tt )

            for record in cursor:
                #print "record = %s\n" % record
                d = {'service_id':record['service_id']      \
                     ,'event_time':record['event_time'] \
                     ,'event_name':record['event_name'] \
                     ,'source_ip':record['source_ip']}
                li.append(d)

            cursor.close()

            return li

        except Exception, e:
            self.logger.error("DB failed to query eventinfo: %s" % str(request_info))
            self.errmsg = str(e)
            self.logger.error("with reason: %s" % self.errmsg)
            return None


    def queryEventDetails(self, event_time,service_id) :
        if self.confirmConnected() == False :
            self.logger.error("Can not connect to DB, please check the status of DataBase")
            return None

        cursor = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cmd_string = ''
        d = {}
        try :
            cmd_string = "SELECT * FROM rd_session_event WHERE event_time=%s AND service_id=%s"

            cursor.execute( cmd_string,(event_time, service_id))
            if cmd_string != '':
                for record in cursor:
                    d = { 'event_time':record['event_time']                     \
                         ,'event_name':record['event_name']                     \
                         ,'event_type':record['event_type']                     \
                         ,'service_id':record['service_id']                     \
                         ,'collector_id':record['collector_id']                 \
                         ,'source_ip':record['source_ip']                       \
                         ,'source_port':record['source_port']                   \
                         ,'service_url':record['service_url']                   \
                         ,'refer_url':record['refer_url']                       \
                         ,'response_time':record['response_time']               \
                         ,'performance_lower':record['performance_lower']       \
                         ,'performance_upper':record['performance_upper']
                    }
            cursor.close()

            return d

        except Exception, e:
            self.logger.error("DB failed to query event details with service_id = %d and event_time = %s" %(service_id, time.ctime(event_time)))
            self.errmsg = str(e)
            self.logger.error("with reason: %s" % self.errmsg)
            return None

######

    def getMinuteRecords(self, start_time, end_time, service_id = None, collector_id = None):

        if self.confirmConnected() == False :
            self.logger.error("Can not connect to DB, please check the status of DataBase")
            return None

        cursor = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        li = []

        cmd_string = "SELECT * FROM rd_session_time_breakdown_minute WHERE start_time>=%s AND end_time<=%s "
        #option_string = " ORDER BY service_id,start_time"
        option_string = " ORDER BY start_time,service_id"

        if service_id != None:
            cmd_string = cmd_string + " AND service_id=" + str(service_id)
        if collector_id != None:
            cmd_string = cmd_string + " AND collector_id=" + str(collector_id)

        cmd_string = cmd_string + option_string

        try :
            cursor.execute( cmd_string,(start_time, end_time))
            for record in cursor:
                d = {'service_id':record['service_id'],'start_time':record['start_time'] \
                     ,'end_time':record['end_time'],'retry_num':record['retry_num'] \
                     ,'retry_time':record['retry_time'],'connection_requests':record['connection_requests'] \
                    ,'success_connections':record['success_connections'],'connection_time':record['connection_time'] \
                    ,'server_time':record['server_time'],'throughput':record['throughput'] \
                    ,'request_num':record['request_num'],'success_responses':record['success_responses'] \
                    ,'server_time2first_buf':record['server_time2first_buffer'],'download_time':record['download_time'] \
                    ,'download_size':record['download_size'],'collector_id':record['collector_id']}
                li.append(d)

            cursor.close()

            return li

        except Exception, e:
            self.logger.error("Failed to get Minute Records from DB")
            self.errmsg = str(e)
            self.logger.error("with reason: %s" % self.errmsg)
            return None
######

    def getHourRecords(self, start_time, end_time, service_id = None, collector_id = None):
    #same as above

        if self.confirmConnected() == False :
            self.logger.error("Can not connect to DB, please check the status of DataBase")
            return None

        cursor = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        li = []

        cmd_string = "SELECT * FROM rd_session_time_breakdown_hour WHERE start_time>=%s AND end_time<=%s"
        option_string = " ORDER BY start_time,service_id"

        if service_id != None:
            cmd_string = cmd_string + " AND service_id=" + str(service_id)
        if collector_id != None:
            cmd_string = cmd_string + " AND collector_id=" + str(collector_id)
        cmd_string = cmd_string + option_string

        try :
            cursor.execute( cmd_string,(start_time, end_time))
            for record in cursor:
                d = {'service_id':record['service_id'],'start_time':record['start_time'] \
                     ,'end_time':record['end_time'],'retry_num':record['retry_num'] \
                     ,'retry_time':record['retry_time'],'connection_requests':record['connection_requests'] \
                    ,'success_connections':record['success_connections'],'connection_time':record['connection_time'] \
                    ,'server_time':record['server_time'],'throughput':record['throughput'] \
                    ,'request_num':record['request_num'],'success_responses':record['success_responses'] \
                    ,'server_time2first_buf':record['server_time2first_buffer'],'download_time':record['download_time'] \
                    ,'download_size':record['download_size'],'collector_id':record['collector_id']}
                li.append(d)

            cursor.close()
            #self.conn.close()

            return li

        except Exception, e:
            self.logger.error("Failed to get Hour Records from DB")
            self.errmsg = str(e)
            self.logger.error("with reason: %s" % self.errmsg)
            return None
######

    def getDayRecords(self, start_time, end_time, service_id = None, collector_id = None):
    #same as above

        if self.confirmConnected() == False :
            self.logger.error("Can not connect to DB, please check the status of DataBase")
            return None

        cursor = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        li = []

        cmd_string = "SELECT * FROM rd_session_time_breakdown_day WHERE start_time>=%s AND end_time<=%s "
        option_string = " ORDER BY start_time,service_id"

        if service_id != None:
            cmd_string = cmd_string + " AND service_id=" + str(service_id)
        if collector_id != None:
            cmd_string = cmd_string + " AND collector_id=" + str(collector_id)

        cmd_string = cmd_string + option_string

        try :
            cursor.execute( cmd_string,(start_time, end_time))
            for record in cursor:
                d = {'service_id':record['service_id'],'start_time':record['start_time'] \
                     ,'end_time':record['end_time'],'retry_num':record['retry_num'] \
                     ,'retry_time':record['retry_time'],'connection_requests':record['connection_requests'] \
                    ,'success_connections':record['success_connections'],'connection_time':record['connection_time'] \
                    ,'server_time':record['server_time'],'throughput':record['throughput'] \
                    ,'request_num':record['request_num'],'success_responses':record['success_responses'] \
                    ,'server_time2first_buf':record['server_time2first_buffer'],'download_time':record['download_time'] \
                    ,'download_size':record['download_size'],'collector_id':record['collector_id']}
                li.append(d)

            cursor.close()

            return li

        except Exception, e:
            self.logger.error("Failed to get Day Records from DB")
            self.errmsg = str(e)
            self.logger.error("with reason: %s" % self.errmsg)
            return None
######

    def __del__(self) :
        if self.conn != None :
            self.conn.close()
