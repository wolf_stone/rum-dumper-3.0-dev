import redis
import json
import logging

class RedisClient:
    def __init__(self, server_ip = "127.0.0.1", server_port = 6379, logger=logging ):
        self.errmsg = ""
	self.logger = logger

        try:
            #rds = redis.Redis(server_ip, server_port)
            #rds = redis.StrictRedis(server_ip, server_port, db=0)
            #self.subObj = rds.pubsub()

            self.pool = redis.ConnectionPool(host=server_ip, port=server_port, db=0)
            self.logger.info("success to connect to redis server")

        except Exception, e:
            self.logger.error("failed connect to redis server with ip=%s and port = %d" %(server_ip,server_port))
            self.errmsg = str(e)
            self.logger.error("with reason: %s" % self.errmsg)
            return None

    def getErrMsg(self) :
        return self.errmsg
######

    def pubSub(self) :
        try :
            rds = redis.Redis(connection_pool=self.pool)
            pubsub = rds.pubsub()
        except Exception, e :
            self.logger.error("Redis failed to create pubsub object")
            self.errmsg = str(e)
            self.logger.error("with reason = %s" % self.errmsg)

        return pubsub
######

    def getRealinfo(self, collector_id) :

        try:
            rds = redis.Redis(connection_pool=self.pool)
            output = rds.get('rum:0_result:collector_%d' % collector_id)
            output = json.loads(output)
            self.logger.debug("realinfo from redis server = %s\n" % str(output))
            return output

        except Exception, e:
            self.logger.warning("failed to get realinfo list with key = 'rum:0_result:collector_%d'" % collector_id)
            self.errmsg = str(e)
            self.logger.warning("with reason = %s" % self.errmsg)
            return None
######

    def getCollectorInfo(self, collector_id = None):
        collectorList = []
        try:
            rds = redis.Redis(connection_pool=self.pool)
            if collector_id == None :
                output = rds.hgetall('rum:collector_map')
                for key in output:
                    collectorList.append(json.loads(output[key]))

                return collectorList

            else:
                output = rds.hget('rum:collector_map', collector_id)
                return json.loads(output)

        except Exception, e:
            self.errmsg = "failed to get collector list from key='rum:collector_map'"
            if collector_id != None :
                self.errmsg = self.errmsg + " with collector_id=%d" % collector_id

            self.logger.warning(self.errmsg)

            self.errmsg = str(e)
            self.logger.warning("with reason = %s" % self.errmsg)

            return None
######

    def setCollectorInfo(self, collector_info):
        try:
            rds = redis.Redis(connection_pool=self.pool)
            rds.hset('rum:collector_map', collector_info['collector_id'], json.dumps(collector_info))
            return True

        except Exception, e:
            self.errmsg = "failed to set collector info:\n %s" % str(collector_info)
            self.logger.warning(self.errmsg)

            self.errmsg = str(e)
            self.logger.warning("with reason = %s" % self.errmsg)

            return False
######

    def deleteCollectorInfo(self, collector_id):
        try:
            rds = redis.Redis(connection_pool=self.pool)
            #TODO: publish the delete action to collector, collector will close then
            rds.hdel('rum:collector_map', collector_id)
            return True

        except Exception, e:
            self.errmsg = "failed to delete collector info with collector_id = %d" % collector_id
            self.logger.warning(self.errmsg)

            self.errmsg = str(e)
            self.logger.warning("with reason = %s" % self.errmsg)
            return False
######

    def getServiceInfo(self, collector_id, service_id = None):
        service_dict = {}
        try:
            rds = redis.Redis(connection_pool=self.pool)
            if service_id == None :
                output = rds.hgetall('rum:service_map:collector_%d' % collector_id)
                for key in output:
                    #serviceList.append(json.loads(output[key]))
                    service_dict[int(key)] = (json.loads(output[key]))

                #return serviceList
            else:
                output = rds.hget('rum:service_map:collector_%d' % collector_id, service_id)
                service_dict = json.loads(output)
                #return json.loads(output)

            return service_dict

        except Exception, e:
            self.errmsg = "failed to get service info list "
            if service_id != None :
                self.errmsg = self.errmsg + "with collector_id=%d, service_id=%d" % (collector_id, service_id)
            else :
                self.errmsg = self.errmsg + "with collector_id=%d" % collector_id
            self.logger.warning(self.errmsg)

            self.errmsg = str(e)
            self.logger.warning("with reason = %s" % self.errmsg)

            return None
######

    def setServiceInfo(self, collector_id, service_info):
        try:
            rds = redis.Redis(connection_pool=self.pool)
            rds.hset('rum:service_map:collector_%d' % collector_id, service_info['service_id'], json.dumps(service_info))
            return True

        except Exception, e:
            self.errmsg = "failed to set service info with collector_id = %d and service_info = \n%s" % (collector_id, str(service_info))
            self.logger.warning(self.errmsg)

            self.errmsg = str(e)
            self.logger.warning("with reason = %s" % self.errmsg)

            return False
######

    def deleteServiceInfo(self, collector_id, service_id):
        try:
            rds = redis.Redis(connection_pool=self.pool)
            rds.hdel('rum:service_map:collector_%d' % collector_id, service_id)
            return True

        except Exception, e:
            errmsg = "failed to delete service info"
            errmsg = errmsg + " with collector_id = %d, service_id = %d" % (collector_id,service_id)
            self.logger.warning(errmsg)

            self.errmsg = str(e)
            self.logger.warning("with reason = %s" % self.errmsg)
            return False
######

    def getServiceThresInfo(self, collector_id, service_id = None):
        thres_dict = {}
        try:
            rds = redis.Redis(connection_pool=self.pool)
            if service_id == None :
                output = rds.hgetall('rum:service_thres_map:collector_%d' % collector_id)
                for key in output:
                    #thresList.append(json.loads(output[key]))
                    thres_dict[int(key)] = (json.loads(output[key]))
            else:
                output = rds.hget('rum:service_thres_map:collector_%d' % collector_id, service_id)
                thres_dict[service_id] = (json.loads(output))

            return thres_dict

        except Exception, e:
            errmsg = "failed to get service thres list: "
            if service_id != None :
                errmsg = errmsg + "\twith collector_id=%d, service_id=%d" % (collector_id,service_id)
            else :
                errmsg = errmsg + "\twith collector_id=%d" % collector_id
            self.logger.warning(errmsg)

            self.errmsg = str(e)
            self.logger.warning("with reason = %s" % self.errmsg)

            return None
######

    def setServiceThresInfo(self, collector_id, thres_info):
        try:
            rds = redis.Redis(connection_pool=self.pool)
            rds.hset('rum:service_thres_map:collector_%d' % collector_id, thres_info['service_id'], json.dumps(thres_info))
            return True

        except Exception, e:
            errmsg = "set service thres info failed: \n%s" % str(thres_info)
            self.logger.warning(errmsg)

            self.errmsg = str(e)
            self.logger.warning("with reason = %s" % self.errmsg)

            return False
######

    def deleteServiceThresInfo(self, collector_id, service_id):
        try:
            rds = redis.Redis(connection_pool=self.pool)
            rds.hdel('rum:service_thres_map:collector_%d' % collector_id, service_id)
            return True

        except Exception, e:
            errmsg = "failed to delete service thres info "
            errmsg = errmsg + "with collector_id = %d, service_id = %d" % (collector_id,service_id)
            self.logger.warning(errmsg)

            self.errmsg = str(e)
            self.logger.warning("with reason = %s" % self.errmsg)

            return False
######

    def pipeGetServiceInfo(self, collector_id, service_id = None) :

        service_dict = {}
        thres_dict = {}
        service_info = {}

        try:
            rds = redis.Redis(connection_pool=self.pool)
            pipe = rds.pipeline()

            if service_id == None :
                pipe.hgetall('rum:service_map:collector_%d' % collector_id)
                pipe.hgetall('rum:service_thres_map:collector_%d' % collector_id)
                output = pipe.execute()
                service_dict = output[0]
                thres_dict = output[1]
                for key in service_dict :
                    service_info[int(key)] = (json.loads(service_dict[key]))
                    service_info[int(key)].update(json.loads(thres_dict[key]))
            else :
                pipe.hget('rum:service_map:collector_%d' % collector_id, service_id)
                pipe.hget('rum:service_thres_map:collector_%d' % collector_id, service_id)
                output = pipe.execute()
                service_dict = output[0]
                thres_dict = output[1]
                service_info = (json.loads(service_dict))
                service_info.update(json.loads(thres_dict))

            return service_info

        except Exception, e:
            errmsg = "Failed to pipe get service list: "
            if service_id != None :
                errmsg = errmsg + "\twith collector_id=%d, service_id=%d" % (collector_id,service_id)
            else :
                errmsg = errmsg + "\twith collector_id=%d" % collector_id
            self.logger.warning(errmsg)

            self.errmsg = str(e)
            self.logger.warning("with reason = %s" % self.errmsg)

            return None
######

    def pipeAddServiceInfo(self, collector_id, service_dict, thres_dict,
                            responsetime_info, availability_info, time_info) :

        try :
            rds = redis.Redis(connection_pool=self.pool)
            pipe = rds.pipeline()

            pipe.hset('rum:service_map:collector_%d' % collector_id, service_dict['service_id'], json.dumps(service_dict))
            pipe.hset('rum:service_thres_map:collector_%d' % collector_id, service_dict['service_id'], json.dumps(thres_dict))
            pipe.hset('rum:event_responsetime_map', service_dict['service_id'], json.dumps(responsetime_info))
            pipe.hset('rum:event_availability_map', service_dict['service_id'], json.dumps(availability_info))
            pipe.hset('rum:dumper_timerecords_map', service_dict['service_id'], json.dumps(time_info))
            pipe.publish('rum:pubsub_service_add:collector_%d' % collector_id, json.dumps(service_dict))

            pipe.execute()
            return True

        except Exception, e:

            self.logger.warning("Failed to pipe add service info with collector_id = %d" % collector_id)
            self.logger.warning("service_dict = %s" % str(service_dict))
            self.logger.warning("thres_dict = %s" % str(thres_dict))

            self.errmsg = str(e)
            self.logger.warning("with reason = %s" % self.errmsg)

            return False
######

    def pipeDeleteServiceInfo(self, collector_id, service_dict) :

        try :
            rds = redis.Redis(connection_pool=self.pool)
            pipe = rds.pipeline()
            #TODO: rum:dumper_timerecords_map

            pipe.hdel('rum:service_map:collector_%d' % collector_id, service_dict['service_id'])
            pipe.hdel('rum:service_thres_map:collector_%d' % collector_id, service_dict['service_id'])
            pipe.hdel('rum:event_responsetime_map', service_dict['service_id'])
            pipe.hdel('rum:event_availability_map', service_dict['service_id'])
            pipe.hdel('rum:dumper_timerecords_map', service_dict['service_id'])
            pipe.publish('rum:pubsub_service_delete:collector_%d' % collector_id, json.dumps(service_dict))
            pipe.execute()
            return True

        except Exception, e:
            self.logger.warning("Failed to pipe delete service %s from redis server" % str(service_dict))

            self.errmsg = str(e)
            self.logger.warning("with reason = %s" % self.errmsg)

            return False
######

    def pipeUpdateServiceInfo(self, collector_id, service_dict, thres_dict, orig_service_dict) :

        try :
            rds = redis.Redis(connection_pool=self.pool)
            pipe = rds.pipeline()

            if service_dict['service_type'] == 'TCP' :
                service_dict['service_url'] = ""
            pipe.hset('rum:service_map:collector_%d' % collector_id, service_dict['service_id'], json.dumps(service_dict))
            pipe.hset('rum:service_thres_map:collector_%d' % collector_id, thres_dict['service_id'], json.dumps(thres_dict))

            if orig_service_dict != service_dict :
                pipe.publish('rum:pubsub_service_update:collector_%d' % collector_id, json.dumps(service_dict))

            pipe.execute()

            return True

        except Exception, e:
            self.logger.warning("Failed to pipe update service from redis server")

            self.errmsg = str(e)
            self.logger.warning("with reason = %s" % self.errmsg)
            return False
######

    def publishAddService(self, collector_id, service_info):

        try:
            rds = redis.Redis(connection_pool=self.pool)
            rds.publish('rum:pubsub_service_add:collector_%d' % collector_id, json.dumps(service_info))

            return True

        except Exception, e:
            errmsg = "failed to pulish add service with collector_id=%d:" % collector_id
            errmsg = errmsg + " and service_info=\n%s" % str(service_info)
            self.logger.warning(errmsg)

            self.errmsg = str(e)
            self.logger.warning("with reason = %s" % self.errmsg)

            return False
######

    def publishDeleteService(self, collector_id, service_info):

        try:
            rds = redis.Redis(connection_pool=self.pool)
            rds.publish('rum:pubsub_service_delete:collector_%d' % collector_id, json.dumps(service_info))
            return True

        except Exception, e:
            errmsg = "failed to pulish delete service, collector_id=%d" % collector_id
            errmsg = errmsg + " and service_info=\n\t%s" % str(service_info)
            self.logger.warning(errmsg)

            self.errmsg = str(e)
            self.logger.warning("with reason = %s" % self.errmsg)

            return False
######

    def publishUpdateService(self, collector_id, service_info):

        try:
            rds = redis.Redis(connection_pool=self.pool)
            rds.publish('rum:pubsub_service_update:collector_%d' % collector_id, json.dumps(service_info))
            return True

        except Exception, e:
            errmsg = "failed to pulish update service, collector_id=%d" % collector_id
            errmsg = errmsg + " and service_info=\n\t%s" % str(service_info)
            self.logger.warning(errmsg)

            self.errmsg = str(e)
            self.logger.warning("with reason = %s" % self.errmsg)

            return False
######

    def __del__(self) :
        self.pool.disconnect()
        self.logger.info("close redisclient object")
