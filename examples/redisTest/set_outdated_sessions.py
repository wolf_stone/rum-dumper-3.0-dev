import os,time
import redis, json
from decimal import Decimal
from datetime import datetime
rdsobj = redis.Redis('192.168.1.15')

RUM_SERVICE_TYPE_TCP = 1
RUM_SERVICE_TYPE_TCPSSL = 2
RUM_SERVICE_TYPE_HTTP = 3
RUM_SERVICE_TYPE_HTTPS = 4

counter = 0
now_secs = 1376378338
session_timebrk = {}
session_timebrk = {'collector_id':1, 'service_id':1, 'start_time':(now_secs-5), 'end_time':now_secs, 'source_ip':"192.168.1.111", 'retry_time':10000, 'connection_time':84000, 'connection_requests':1, 'success_connections':1, 'server_time':445000, 'throughput':1024, 'ssl_success_connections':1, 'ssl_shake_time':190000, 'request_num':1, 'success_responses':1,'network_time2first_buffer':156000, 'server_time2first_buffer':513000, 'download_time':267000, 'download_size':2048}#, 'response_time',Decimal(1234.5)}#, 'availability':90.2, 'service_type':"HTTPS", 'source_port':443, 'refer_url':"https://www.baidu.com/refer", 'service_url':"https://www.baidu.com/page"}
session_timebrk['retry_num'] = 12
session_timebrk['ssl_requests'] = 1
session_timebrk['authentication_requests'] = 1
session_timebrk['success_authentications'] = 1
session_timebrk['authentication_time'] = 123
session_timebrk['service_type'] = RUM_SERVICE_TYPE_HTTPS
session_timebrk['source_port'] = 5678
session_timebrk['refer_url'] = 'https://www.baidu.com/refer'
session_timebrk['service_url'] = 'https://www.baidu.com/page'

session_event = {}
session_event['collector_id'] = 1
session_event['service_id'] = 1
session_event['event_time'] = now_secs
session_event['event_type'] = 'HTTP'
session_event['event_name'] = 'HTTP:001'
session_event['source_ip'] = '192.168.1.10'
session_event['source_port'] = 5679
session_event['refer_url'] = 'https://www.baidu.com/refer'
session_event['service_url'] = 'https://www.baidu.com/page'

session_timebrk2 = {}
session_timebrk2 = session_timebrk.copy()
session_timebrk2['service_id'] = 2
session_timebrk2['retry_num'] = 7
session_timebrk2['connection_time'] = 11111
session_timebrk2['server_time'] =  843000
session_timebrk2['network_time2first_buffer'] =  159080
session_timebrk2['server_time2first_buffer'] =  330000
session_timebrk2['download_time'] =  199001
session_timebrk2['download_size'] =  4096
session_timebrk2['service_type'] = RUM_SERVICE_TYPE_HTTP
session_timebrk2['source_port'] = 1234
session_timebrk2['refer_url'] = 'https://www.beilizhu.com/refer'
session_timebrk2['service_url'] = 'https://www.beilizhu.com/page'

session_timebrk3 = {}
session_timebrk3 = session_timebrk.copy()
session_timebrk3['service_id'] = 3
session_timebrk3['retry_num'] = 4
session_timebrk3['retry_time'] = 40000
session_timebrk3['connection_time'] = 97543
session_timebrk3['server_time'] =  183200
session_timebrk3['throughput'] = 39765487
session_timebrk3['service_type'] = RUM_SERVICE_TYPE_TCP
session_timebrk3['source_ip'] = '192.168.0.10'
session_timebrk3['source_port'] = 8987
session_timebrk3['collector_id'] = 3

session_timebrk4 = {}
session_timebrk4 = session_timebrk.copy()
session_timebrk4['service_id'] = 4
session_timebrk4['retry_num'] = 1
session_timebrk4['retry_time'] = 40709
session_timebrk4['connection_time'] = 88843
session_timebrk4['server_time'] =  1853200
session_timebrk4['throughput'] = 3987
session_timebrk4['service_type'] = RUM_SERVICE_TYPE_TCP
session_timebrk4['source_ip'] = '192.168.0.10'
session_timebrk4['source_port'] = 8977
session_timebrk4['collector_id'] = 3

dd = datetime.now()
now_secs = int(time.mktime(dd.timetuple()))

try:
    while(True):

        session_timebrk['end_time'] = now_secs
        session_timebrk['start_time'] = session_timebrk['end_time'] - 1

        session_timebrk2['end_time'] = now_secs
        session_timebrk2['start_time'] = session_timebrk2['end_time'] - 2

        session_timebrk3['end_time'] = now_secs
        session_timebrk3['start_time'] = session_timebrk3['end_time'] - 3

        session_timebrk4['end_time'] = now_secs
        session_timebrk4['start_time'] = session_timebrk4['end_time'] - 4

        rdsobj.rpush('rum:list_session_timebrk', json.dumps(session_timebrk))
        rdsobj.rpush('rum:list_session_timebrk', json.dumps(session_timebrk2))
        rdsobj.rpush('rum:list_session_timebrk', json.dumps(session_timebrk3))
        rdsobj.rpush('rum:list_session_timebrk', json.dumps(session_timebrk4))
        #rdsobj.rpush('rum:list_session_event', json.dumps(session_event))


        if now_secs % 30 == 0:
            session_timebrk['end_time'] = now_secs - 20
            session_timebrk['start_time'] = session_timebrk['end_time'] - 1
            rdsobj.rpush('rum:list_session_timebrk', json.dumps(session_timebrk))

            session_timebrk['end_time'] = now_secs - 28
            session_timebrk['start_time'] = session_timebrk['end_time'] - 1
            rdsobj.rpush('rum:list_session_timebrk', json.dumps(session_timebrk))

            session_timebrk['end_time'] = now_secs - 30
            session_timebrk['start_time'] = session_timebrk['end_time'] - 1
            rdsobj.rpush('rum:list_session_timebrk', json.dumps(session_timebrk))

            session_timebrk['end_time'] = now_secs - 31
            session_timebrk['start_time'] = session_timebrk['end_time'] - 1
            rdsobj.rpush('rum:list_session_timebrk', json.dumps(session_timebrk))

            session_timebrk['end_time'] = now_secs - 35
            session_timebrk['start_time'] = session_timebrk['end_time'] - 1
            rdsobj.rpush('rum:list_session_timebrk', json.dumps(session_timebrk))

            dd = datetime.now()
            now_secs = int(time.mktime(dd.timetuple()))
        else:
            now_secs = now_secs + 1


        if now_secs % 300 == 0:
            session_timebrk['end_time'] = now_secs - 299
            session_timebrk['start_time'] = session_timebrk['end_time'] - 1
            rdsobj.rpush('rum:list_session_timebrk', json.dumps(session_timebrk))

            session_timebrk['end_time'] = now_secs - 300
            session_timebrk['start_time'] = session_timebrk['end_time'] - 1
            rdsobj.rpush('rum:list_session_timebrk', json.dumps(session_timebrk))

            session_timebrk['end_time'] = now_secs - 301
            session_timebrk['start_time'] = session_timebrk['end_time'] - 1
            rdsobj.rpush('rum:list_session_timebrk', json.dumps(session_timebrk))

            session_timebrk['end_time'] = now_secs - 250
            session_timebrk['start_time'] = session_timebrk['end_time'] - 1
            rdsobj.rpush('rum:list_session_timebrk', json.dumps(session_timebrk))

            session_timebrk['end_time'] = now_secs - 350
            session_timebrk['start_time'] = session_timebrk['end_time'] - 1
            rdsobj.rpush('rum:list_session_timebrk', json.dumps(session_timebrk))

        counter = counter + 1
        time.sleep(1)

        #session_event['event_time'] = now_secs

except KeyboardInterrupt, e:
    print "counter: %s" % counter
    exit(0)

