import os,time
import redis, json
#from RedisClient import RedisClient

#rdsobj = RedisClient('192.168.1.60')
#rdsobj.publishDeleteService(service_dict)
rdsobj = redis.Redis('192.168.1.10')

counter = 0
#rum:pubsub_session_timebrk
try:
    while(True):
        session_timebrk = {'service_id':1, 'source_ip':'192.168.1.10'}
        session_event = {'service_id':1, 'source_ip':'192.168.1.10'}
        rdsobj.publish('rum:pubsub_session_timebrk:collector_%d' % 1, json.dumps(session_timebrk))
        rdsobj.publish('rum:pubsub_session_event:collector_%d' % 1, json.dumps(session_event))
        counter = counter + 2
        time.sleep(1)

except KeyboardInterrupt, e:
    print "counter: %s" % counter
    exit(0)
