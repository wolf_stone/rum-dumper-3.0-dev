from RedisClient import RedisClient
from DatabaseClient import DatabaseClient

rdsobj = RedisClient('192.168.1.15')
dbobj = DatabaseClient('192.168.1.15',5432)

collector_id = 3

service_dict = {}
#service_dict['service_id'] = '2'
service_dict['collector_id'] = collector_id
#service_dict['service_name'] = 'redis'
service_dict['service_name'] = 'database_pgsql'
service_dict['service_ip'] = '192.168.1.15'
service_dict['service_port'] = '5432'
service_dict['service_type'] = 'TCP'
#service_dict['service_url'] = 'http://dummy.beilizhu.com/default'
service_dict['service_url'] = ''

service_thres_dict['performance_lower'] = 800
service_thres_dict['performance_upper'] = 1600
service_thres_dict['performance_zero'] = 0
service_thres_dict['performance_full'] = 2000

service_thres_dict['availability_lower'] = 90
service_thres_dict['availability_upper'] = 95

service_thres_dict['event_period'] = 300
service_thres_dict['event_counts_warning'] = 80
service_thres_dict['event_counts_critical'] = 120

service_id =dbobj.addServiceInfo(service_dict)
print service_id

if service_id > 0 :
    service_dict['service_id'] = service_id
    service_thres_dict['service_id'] = service_id
    rdsobj.setServiceInfo(collector_id, service_dict)
    rdsobj.setServiceThresInfo(collector_id, service_thres_dict)

print "get service_list: %s" % rdsobj.getServiceInfo(collector_id)
