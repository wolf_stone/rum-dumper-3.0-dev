#from RedisClient import RedisClient
import redis

rdsobj = redis.Redis('192.168.1.10')
psubobj = rdsobj.pubsub()
psubobj.psubscribe("rum:pubsub_session_*")

for item in psubobj.listen():
    print "channel: %s" % item['channel'], ":", "data: %s" % item['data']

