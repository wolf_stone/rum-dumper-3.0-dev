import logging

class LogClient:

    def __init__(self, log_name,level=logging.NOTSET):
        self.logger = logging.getLogger()
        self.logger.setLevel(level)
        self.hdlr = logging.FileHandler(log_name)
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        self.hdlr.setFormatter(formatter)
        self.logger.addHandler(self.hdlr)
        self.logger.setLevel(logging.NOTSET)

    def debug(self, msg):
        self.logger.addHandler(self.hdlr)
        self.logger.debug(msg)
        self.logger.removeHandler(self.hdlr)

    def info(self, msg):
        self.logger.addHandler(self.hdlr)
        self.logger.info(msg)
        self.logger.removeHandler(self.hdlr)

    def warning(self, msg):
        self.logger.addHandler(self.hdlr)
        self.logger.warning(msg)
        self.logger.removeHandler(self.hdlr)

    def error(self, msg):
        self.logger.addHandler(self.hdlr)
        self.logger.error(msg)
        self.logger.removeHandler(self.hdlr)


def display_service( logger, service_item ) :

    logger.debug("---------------Start to Display Service_t---------------")

    logger.debug("service_id = %d" % service_item['service_id'])
    logger.debug("service_name = %s" % service_item['service_name'])
    logger.debug("service_ip = %s" % service_item['service_ip'])
    logger.debug("service_port = %s" % service_item['service_port'])
    logger.debug("app_type = %s" % service_item['app_type'])
    logger.debug("url = %s" % service_item['url'])
    logger.debug("perfor_zero = %f" % service_item['perfor_zero'])
    logger.debug("perfor_full = %f" % service_item['perfor_full'])
    logger.debug("perfor_lower = %f" % service_item['perfor_lower'])
    logger.debug("perfor_upper = %f" % service_item['perfor_upper'])
    logger.debug("availa_lower = %f" % service_item['availa_lower'])
    logger.debug("availa_upper = %f" % service_item['availa_upper'])
    logger.debug("collector_id = %d" % service_item['collector_id'])

    logger.debug("---------------End of Display Service_t---------------")

