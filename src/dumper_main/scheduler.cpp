#include "threads_api.h"
#include "dumper_common.h"
#include "debug.h"

static Logger logger = Logger::getInstance(LOG4CPLUS_TEXT("SchedulerFileLogger.SCHDLOG"));

bool updateHashListConsumer()
{

    if (!gListTasks.empty())
    {
        pthread_mutex_lock ( &gListMutex );
        gpHashListConsumer = gListTasks.front();
        gListTasks.pop_front();
        pthread_mutex_unlock ( &gListMutex);

        return true;
    }
    return false;
}

void taskSchedule (zmq::socket_t* pPairRecvSock, zmq::context_t* pZmqContext)
{
    dumperMsgInfoPtr pMsgInfo;
    int zmqSndHWM = 100000;
    bool myRet = false;

    if (DumperParamers.logLevel == "DEBUG")
    {
        logger.setLogLevel(DEBUG_LOG_LEVEL);
    }
    else if (DumperParamers.logLevel == "WARNING")
    {
        logger.setLogLevel(WARN_LOG_LEVEL);
    }
    else if (DumperParamers.logLevel == "ERROR")
    {
        logger.setLogLevel(ERROR_LOG_LEVEL);
    }
    else if (DumperParamers.logLevel == "FATAL")
    {
        logger.setLogLevel(FATAL_LOG_LEVEL);
    }
    else
    {
        logger.setLogLevel(INFO_LOG_LEVEL);
    }

    LOG4CPLUS_INFO(logger, "Start Scheduler LOG4CPLUS");

    try
    {
        zmq::message_t recvMsg(RUM_DUMPER_MSG_LEN);

        zmq::socket_t eventSock (*pZmqContext, ZMQ_PUSH);
        zmq::socket_t timebrkSock (*pZmqContext, ZMQ_PUSH);
        zmq::socket_t taskSock (*pZmqContext, ZMQ_PUSH);

        eventSock.setsockopt(ZMQ_SNDHWM, &zmqSndHWM, sizeof(int));
        timebrkSock.setsockopt(ZMQ_SNDHWM, &zmqSndHWM, sizeof(int));
        taskSock.setsockopt(ZMQ_SNDHWM, &zmqSndHWM, sizeof(int));

        while(1)
        {
            try{
                taskSock.bind(RUM_DUMPER_TASK_READY_CONNECTION);
                LOG4CPLUS_INFO(logger, "Success bind to \"" << RUM_DUMPER_TASK_READY_CONNECTION << "\"");
                break;
            }
            catch (zmq::error_t err)
            {
                LOG4CPLUS_ERROR(logger, "Failed to bind to \"" << RUM_DUMPER_TASK_READY_CONNECTION << "\" with reason: " << err.what());
                sleep(2);
                continue;
            }
        }

        while(1){
            try{
                eventSock.connect(RUM_DUMPER_EVENT_CONNECTION);
                LOG4CPLUS_INFO(logger, "Success connect to \"" << RUM_DUMPER_EVENT_CONNECTION << "\"");
                break;
            }
            catch (zmq::error_t err)
            {
                LOG4CPLUS_ERROR(logger, "Failed to connect to \"" << RUM_DUMPER_EVENT_CONNECTION << "\" with reason: " << err.what());
                sleep(2);
                continue;
            }
        }

        while(1){
            try{
                timebrkSock.connect(RUM_DUMPER_TIMEBRK_CONNECTION);
                LOG4CPLUS_INFO(logger, "Success connect to \"" << RUM_DUMPER_TIMEBRK_CONNECTION << "\"");
                break;
            }
            catch (zmq::error_t err)
            {
                LOG4CPLUS_ERROR(logger, "Failed to connect to \"" << RUM_DUMPER_TIMEBRK_CONNECTION << "\" with reason: " << err.what());
                sleep(2);
                continue;
            }
        }


        while(1)
        {
            try{
                myRet = (*pPairRecvSock).recv(&recvMsg);//, ZMQ_NOBLOCK);
                if( myRet == false )
                {
                    continue;
                }
                else
                {
                    LOG4CPLUS_DEBUG(logger, "Received data from Dispatcher");
                    pMsgInfo = (dumperMsgInfoPtr)recvMsg.data();
                }
            }
            catch (zmq::error_t err)
            {
                LOG4CPLUS_ERROR(logger, "Failed to receive data in scheduler with reason: " << err.what());
                continue;
            }

            if( pMsgInfo->msgType == RUM_DUMPER_MSG_SESSION_TIMEBRK )
            {
                zmq::message_t sendMsg(RUM_DUMPER_MSG_DATA_LEN);

                LOG4CPLUS_INFO(logger, "Scheduler received outdated SESSION_TIMEBRK");

                rum_session_timebrk_t* pSessionTimeBrk = (rum_session_timebrk_t*)pMsgInfo->msgBody;
                LOG4CPLUS_DEBUG(logger, "end_time of outdated SESSION_TIMEBRK" << pSessionTimeBrk->end_time);

                memcpy(sendMsg.data(), pSessionTimeBrk, RUM_DUMPER_MSG_DATA_LEN);
                myRet = timebrkSock.send(sendMsg);
                if (myRet == false)
                {
                    LOG4CPLUS_ERROR(logger, "Scheduler failed to send timebrk message to sinkAggregate");
                }
                else
                {
                    LOG4CPLUS_INFO(logger, "Scheduler success to send timebrk message to sinkAggregate");
                }
            }
            else if (pMsgInfo->msgType == RUM_DUMPER_MSG_SESSION_EVENT)
            {
                zmq::message_t sendMsg(RUM_DUMPER_MSG_DATA_LEN);
                LOG4CPLUS_INFO(logger, "Scheduler received SESSION_EVENT");
                rum_session_event_t* pSessionEvent = (rum_session_event_t*)pMsgInfo->msgBody;
                memcpy(sendMsg.data(), pSessionEvent, RUM_DUMPER_MSG_DATA_LEN);
                LOG4CPLUS_DEBUG(logger, "event_time in session_event(Scheduler): " << pSessionEvent->event_time);

                myRet = eventSock.send(sendMsg);
                if (myRet == false)
                {
                    LOG4CPLUS_ERROR(logger, "Scheduler failed to send event message to sinkEvent");
                }
                else
                {
                    LOG4CPLUS_DEBUG(logger, "Scheduler success to send event message to sinkEvent");
                }
            }
            else if (pMsgInfo->msgType == RUM_DUMPER_MSG_TASK_READY)
            {
                LOG4CPLUS_INFO(logger, "Scheduler received TASK_READY");
                zmq::message_t sendMsg(RUM_DUMPER_MSG_DATA_LEN);

                while(1) 
                {
                    if (__sync_lock_test_and_set(&gpHashListConsumer, gpHashListConsumer)  != NULL)
                    {
                        sleep(1);
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }
                myRet = updateHashListConsumer();
                if (myRet == true)
                {
                    LOG4CPLUS_DEBUG(logger, "Display gpHashListConsumer: ");
                    //gpHashListConsumer->DetailsDisplay("gpHashListConsumer");
                    gpHashListConsumer->GetSessionHashlistPtr();
                    LOG4CPLUS_DEBUG(logger, "Display gpHashListConsumer End");

                    vector<uint32_t> idArray;
                    gpHashListConsumer->GetIdArray(idArray);
                    vector<uint32_t>::iterator iter;
                    for ( iter = idArray.begin() ; iter != idArray.end() ; iter++ )
                    {
                        uint32_t service_id = *iter;
                        memcpy(sendMsg.data(), &service_id, sizeof(uint32_t));
                        taskSock.send(sendMsg);
                        LOG4CPLUS_DEBUG(logger, "Service_id(Scheduler): " << service_id);
                    }
                }
                else
                {
                    LOG4CPLUS_WARN(logger, "gListTasks is empty");
                    break;                  
                }
            }
            else
            {
                LOG4CPLUS_ERROR(logger, "Scheduler received unknown message");
            }
        }
    }
    catch (zmq::error_t err)
    {
        LOG4CPLUS_ERROR(logger, "Failed to initilize scheduler with reason: " << err.what());
    }
}
