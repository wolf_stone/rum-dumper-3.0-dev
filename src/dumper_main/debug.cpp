#include "dumper_common.h"
#include "debug.h"

void sessionDisplay(const std::string prefix, const rum_session_timebrk_t& rSessionTimebrk)
{
    LOG4CPLUS_DEBUG(*gpLogger, endl);
    LOG4CPLUS_DEBUG(*gpLogger, prefix << " collector_id: " << rSessionTimebrk.collector_id);
    LOG4CPLUS_DEBUG(*gpLogger, prefix << " service_id: " << rSessionTimebrk.service_id);
    LOG4CPLUS_DEBUG(*gpLogger, prefix << " start_time: " << rSessionTimebrk.start_time);
    LOG4CPLUS_DEBUG(*gpLogger, prefix << " end_time: " << rSessionTimebrk.end_time);
    LOG4CPLUS_DEBUG(*gpLogger, prefix << " service_type: " << rSessionTimebrk.service_type);
    LOG4CPLUS_DEBUG(*gpLogger, prefix << " source_ip: " << rSessionTimebrk.source_ip);
    LOG4CPLUS_DEBUG(*gpLogger, prefix << " source_port: " << rSessionTimebrk.source_port);
    LOG4CPLUS_DEBUG(*gpLogger, prefix << " tcp_retry_num: " << rSessionTimebrk.tcp_retry_num);
    LOG4CPLUS_DEBUG(*gpLogger, prefix << " tcp_retry_time: " << rSessionTimebrk.tcp_retry_time);
    LOG4CPLUS_DEBUG(*gpLogger, prefix << " tcp_connection_requests: " << rSessionTimebrk.tcp_connection_requests);
    LOG4CPLUS_DEBUG(*gpLogger, prefix << " tcp_success_connections: " << rSessionTimebrk.tcp_success_connections);
    LOG4CPLUS_DEBUG(*gpLogger, prefix << " tcp_connection_time: " << rSessionTimebrk.tcp_connection_time);
    LOG4CPLUS_DEBUG(*gpLogger, prefix << " tcp_server_time: " << rSessionTimebrk.tcp_server_time);
    LOG4CPLUS_DEBUG(*gpLogger, prefix << " tcp_exchange_size: " << rSessionTimebrk.tcp_exchange_size);
    LOG4CPLUS_DEBUG(*gpLogger, prefix << " ssl_requests: " << rSessionTimebrk.ssl_requests);
    LOG4CPLUS_DEBUG(*gpLogger, prefix << " ssl_success_connections: " << rSessionTimebrk.ssl_success_connections);
    LOG4CPLUS_DEBUG(*gpLogger, prefix << " ssl_shake_time: " << rSessionTimebrk.ssl_shake_time);
    LOG4CPLUS_DEBUG(*gpLogger, prefix << " http_status_code: " << rSessionTimebrk.http_status_code);
    LOG4CPLUS_DEBUG(*gpLogger, prefix << " http_request_url: " << rSessionTimebrk.http_request_url);
    LOG4CPLUS_DEBUG(*gpLogger, prefix << " http_refer_url: " << rSessionTimebrk.http_refer_url);
    LOG4CPLUS_DEBUG(*gpLogger, prefix << " http_request_num: " << rSessionTimebrk.http_request_num);
    LOG4CPLUS_DEBUG(*gpLogger, prefix << " http_success_responses: " << rSessionTimebrk.http_success_responses);
    LOG4CPLUS_DEBUG(*gpLogger, prefix << " http_network_time: " << rSessionTimebrk.http_network_time);
    LOG4CPLUS_DEBUG(*gpLogger, prefix << " http_server_time: " << rSessionTimebrk.http_server_time);
    LOG4CPLUS_DEBUG(*gpLogger, prefix << " http_download_time: " << rSessionTimebrk.http_download_time);
    LOG4CPLUS_DEBUG(*gpLogger, prefix << " http_download_size: " << rSessionTimebrk.http_download_size);
}
