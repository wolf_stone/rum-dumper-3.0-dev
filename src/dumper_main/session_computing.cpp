#include "session_computing.h"

void sessionTimebrkSummation(const rum_session_timebrk_t& timebrkItem, rum_session_timebrk_t& timebrkResult)
{
    timebrkResult.tcp_retry_num += timebrkItem.tcp_retry_num; 
    timebrkResult.tcp_retry_time += timebrkItem.tcp_retry_time;

    timebrkResult.tcp_connection_requests += timebrkItem.tcp_connection_requests;
    timebrkResult.tcp_success_connections += timebrkItem.tcp_success_connections;
    timebrkResult.tcp_connection_time += timebrkItem.tcp_connection_time;

    timebrkResult.tcp_server_time += timebrkItem.tcp_server_time;
    timebrkResult.tcp_exchange_size += timebrkItem.tcp_exchange_size;

    timebrkResult.ssl_requests += timebrkItem.ssl_requests;
    timebrkResult.ssl_success_connections += timebrkItem.ssl_success_connections;
    timebrkResult.ssl_shake_time += timebrkItem.ssl_shake_time;


    timebrkResult.http_request_num += timebrkItem.http_request_num;
    timebrkResult.http_success_responses += timebrkItem.http_success_responses;
    timebrkResult.http_network_time += timebrkItem.http_network_time;
    timebrkResult.http_server_time += timebrkItem.http_server_time;
    timebrkResult.http_download_time += timebrkItem.http_download_time;
    timebrkResult.http_download_size += timebrkItem.http_download_size;
}
