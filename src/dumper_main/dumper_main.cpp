#include <iostream>
#include <sstream> 
#include <exception>
#include <vector>
#include <algorithm>

#include "czmq.h"
#include "Config.h"
#include "dumper_common.h"
#include "zmq.hpp"
#include "redisclient.hpp"
#include "hiredis.h"
#include "threads_api.h"
#include <boost/archive/text_iarchive.hpp>

using namespace std;

DumperParams DumperParamers;

RumSessionHashlist* gpHashListProducer = NULL; 
//RumSessionHashlist* gpHashListRouter = NULL;
RumSessionHashlist* gpHashListConsumer = NULL;  

list<RumSessionHashlist*> gListTasks;
pthread_mutex_t gListMutex;
//RumSessionHashlist* gpHashListScavenger = NULL;  

static Logger logger_root = Logger::getInstance(LOG4CPLUS_TEXT("DispatcherFileLogger.DISPLOG"));
__thread Logger* gpLogger = &logger_root;

/* Parse configuration file */
void parseConf(const char* pConfigFile)
{

    LOG4CPLUS_INFO(*gpLogger, "config file: " << pConfigFile);
    try{
        Config configSettings( pConfigFile );  

        DumperParamers.dbSrvIp = configSettings.Read("db_host", "127.0.0.1");
        DumperParamers.dbSrvPort = configSettings.Read("db_port", 5432);
        DumperParamers.dbSrvUser = configSettings.Read("db_user", "postgres");
        DumperParamers.dbSrvPasswd = configSettings.Read("db_passwd", "");
        DumperParamers.dbName = configSettings.Read("db_name", "rumdb");

        DumperParamers.redisSrvIp = configSettings.Read("redis_host", "127.0.0.1");
        DumperParamers.redisSrvPort = configSettings.Read("redis_port", 6379);
        DumperParamers.redisSrvUser = configSettings.Read("redis_user", "");
        DumperParamers.redisSrvPasswd = configSettings.Read("redis_passwd", "");

        DumperParamers.workerCopies = configSettings.Read("worker_copies", 1);

        DumperParamers.timebrkReadTimeout = configSettings.Read("timebrk_read_timeout", 1);

        DumperParamers.produceTimerTimeout = configSettings.Read("produce_timer_timeout", 30);

        DumperParamers.dashboardTimerTimeout = configSettings.Read("dashboard_refresh_period", 2);
        DumperParamers.dashboardDuration = configSettings.Read("dashboard_duration", 30);

        DumperParamers.logLevel = configSettings.Read("log_level", "WARNING");
        transform((DumperParamers.logLevel).begin(),(DumperParamers.logLevel).end(),(DumperParamers.logLevel).begin(),::toupper);
        DumperParamers.logPath = configSettings.Read("log_path", "/var/log/rum");
    }
    catch(Config::File_not_found& notFound)
    {
        LOG4CPLUS_ERROR(*gpLogger, "Failed to initialize from \"" << pConfigFile << "\"" << "with reason: File not Found!");
    }

    LOG4CPLUS_DEBUG(*gpLogger, "dbSrvIP: " << DumperParamers.dbSrvIp);
    LOG4CPLUS_DEBUG(*gpLogger, "dbSrvPort: " << DumperParamers.dbSrvPort);
    LOG4CPLUS_DEBUG(*gpLogger, "dbSrvUser: " << DumperParamers.dbSrvUser);
    LOG4CPLUS_DEBUG(*gpLogger, "dbSrvPasswd: " << DumperParamers.dbSrvPasswd);
    LOG4CPLUS_DEBUG(*gpLogger, "dbName: " << DumperParamers.dbName);
    LOG4CPLUS_DEBUG(*gpLogger, "redisSrvIp: " << DumperParamers.redisSrvIp);
    LOG4CPLUS_DEBUG(*gpLogger, "redisSrvPort: " << DumperParamers.redisSrvPort);
    LOG4CPLUS_DEBUG(*gpLogger, "redisSrvUser: " << DumperParamers.redisSrvUser);
    LOG4CPLUS_DEBUG(*gpLogger, "redisSrvPasswd: " << DumperParamers.redisSrvPasswd);
    LOG4CPLUS_DEBUG(*gpLogger, "workerCopies: " << DumperParamers.workerCopies);

    LOG4CPLUS_DEBUG(*gpLogger, "LogLevel(Config): " << DumperParamers.logLevel);
    LOG4CPLUS_DEBUG(*gpLogger, "LogPath: " << DumperParamers.logPath);
}

bool validateAndCheckEvent(rum_session_timebrk_t& sessionBrk, rum_session_event_t& sessionEvent)
{
    bool isEvent = false;

    sessionEvent.collector_id = sessionBrk.collector_id;
    sessionEvent.service_id = sessionBrk.service_id;
    sessionEvent.event_time = sessionBrk.end_time;

    sessionEvent.source_port = sessionBrk.source_port;

    if (sessionBrk.service_type == RUM_SERVICE_TYPE_HTTP || sessionBrk.service_type == RUM_SERVICE_TYPE_HTTPS)
    {
        uint16_t codeHeader = sessionBrk.http_status_code / 100;
        if (codeHeader == 4 || codeHeader == 5)
        {
            isEvent = true;
            sessionEvent.event_type = RUM_EVENT_TYPE_HTTP;
            sprintf(sessionEvent.event_name,"%s:%d", "HTTP", sessionBrk.http_status_code);

            strncpy(sessionEvent.source_ip, sessionBrk.source_ip, RUM_IP_NAME_LEN);
            strncpy(sessionEvent.http_request_url, sessionBrk.http_request_url, RUM_DATA_LEN);
            strncpy(sessionEvent.http_refer_url, sessionBrk.http_refer_url, RUM_DATA_LEN);

            LOG4CPLUS_DEBUG(*gpLogger, "HTTP event_name: " << sessionEvent.event_name);
            LOG4CPLUS_DEBUG(*gpLogger, "HTTP source_ip: " << sessionEvent.source_ip);
            LOG4CPLUS_DEBUG(*gpLogger, "HTTP source_port: " << sessionEvent.source_port);

            sessionBrk.http_server_time = 0;
            sessionBrk.http_download_time = 0;
            sessionBrk.http_download_size = 0;

            return isEvent;
        }
    }
    else    //Workaround
    {
        sessionBrk.http_request_num = 0;
        sessionBrk.http_success_responses = 0;
        sessionBrk.http_network_time = 0;
        sessionBrk.http_server_time = 0;
        sessionBrk.http_download_time = 0;
        sessionBrk.http_download_size = 0;
    }
            LOG4CPLUS_DEBUG(*gpLogger, "TCP event_name: " << sessionEvent.event_name);
            LOG4CPLUS_DEBUG(*gpLogger, "TCP source_ip: " << sessionEvent.source_ip);
            LOG4CPLUS_DEBUG(*gpLogger, "TCP source_port: " << sessionEvent.source_port);
            LOG4CPLUS_DEBUG(*gpLogger, "isEvent: " << isEvent);

    /*if (sessionBrk.tcp_status_code != 0)
    {
        isEvent = true;
        sessionEvent.event_type = RUM_EVENT_TYPE_TCP;
        sprintf(sessionEvent.event_name,"%s:%d", "TCP", sessionBrk.tcp_status_code);
        strncpy(sessionEvent.source_ip, sessionBrk.source_ip, RUM_IP_NAME_LEN);
    }*/

    return isEvent;
}

static void taskDispatcher ( zmq::socket_t& rDispatchSendSock,  zmq::socket_t& rRealtimeSendSock)
{
    bool recvRet = false;
    bool isEvent = false;
    bool sendRet = false;
    int mySubType = RUM_DUMPER_MSG_UNKNOWN;
    //redisAsyncContext* pAsyncRedisCtx = NULL;
    LOG4CPLUS_DEBUG(*gpLogger, "Dispatcher start to work");

    //redisContext* pRedisCtx = NULL;
    //pRedisCtx = RedisClient::initRedisContext(DumperParamers.redisSrvIp, DumperParamers.redisSrvPort );    

    int zmqRecvHWM = 10000;
    zmq::context_t context (1);
    zmq::socket_t recvSock (context, ZMQ_PULL);
    recvSock.setsockopt(ZMQ_RCVHWM, &zmqRecvHWM, sizeof(int));

    while(1)
    {
        try{
            recvSock.bind(RUM_DUMPER_READ_SESSION_TIMEBRK);
            LOG4CPLUS_INFO(*gpLogger, "Success bind to \"" << RUM_DUMPER_READ_SESSION_TIMEBRK << "\"");
            break;
        }
        catch (zmq::error_t err)
        {
            LOG4CPLUS_ERROR(*gpLogger, "Failed to bind to \"" << RUM_DUMPER_READ_SESSION_TIMEBRK << "\" with reason: " << err.what());
            sleep(2);
            continue;
        }
    }

    //zmq::message_t recvMsg;
    //zmq::message_t sendMsg(sizeof(rum_session_timebrk_t));        

    while(1)
    {
        mySubType = RUM_DUMPER_MSG_UNKNOWN;

        rum_session_timebrk_t mySessionTimebrk;
        memset(&mySessionTimebrk, 0 ,sizeof(rum_session_timebrk_t));

        rum_session_event_t mySessionEvent;
        memset(&mySessionEvent, 0 ,sizeof(rum_session_event_t));

        //myRet = RedisClient::getSessionTimebrk(pRedisCtx, mySessionTimebrk, DumperParamers.timebrkReadTimeout);

        try{
            zmq::message_t recvMsg;
            recvRet = recvSock.recv(&recvMsg, ZMQ_NOBLOCK);
            //recvRet = recvSock.recv(&recvMsg);
            if (recvRet == false)
            {
                mySubType = RUM_DUMPER_TIMEOUT;
                sessionMsgProcess(mySubType, NULL, rDispatchSendSock, 0);
                usleep(10000);
                continue;
            }

            std::string content((char*)recvMsg.data());
            std::stringstream ss(content);
            boost::archive::text_iarchive ia(ss);
            ia >> mySessionTimebrk;

            isEvent = validateAndCheckEvent(mySessionTimebrk, mySessionEvent);

            zmq::message_t sendMsg(sizeof(rum_session_timebrk_t));
            rum_session_timebrk_t* pSessionTimebrk = (rum_session_timebrk_t*)sendMsg.data();
            memcpy(pSessionTimebrk, &mySessionTimebrk, sizeof(rum_session_timebrk_t));
            sendRet = rRealtimeSendSock.send(sendMsg, ZMQ_NOBLOCK);
            //sendMsg.rebuild(sizeof(rum_session_timebrk_t));
            if (sendRet == false)
            {
                LOG4CPLUS_ERROR(*gpLogger, "Failed to send message to realtime_computing");
            }
            else
            {
                LOG4CPLUS_DEBUG(*gpLogger, "Success to send message to realtime_computing");
            }

            mySubType = RUM_DUMPER_MSG_SESSION_TIMEBRK;
            sessionMsgProcess(mySubType, (char*)&mySessionTimebrk, rDispatchSendSock, DumperParamers.produceTimerTimeout);

            if (isEvent == true)
            {
                sendMsg.rebuild(sizeof(dumperMsgInfo));
                LOG4CPLUS_DEBUG(*gpLogger, "Process SessionEvent");

                dumperMsgInfoPtr pMsgInfo = (dumperMsgInfoPtr)sendMsg.data();
                pMsgInfo->msgType = RUM_DUMPER_MSG_SESSION_EVENT;
                memcpy(pMsgInfo->msgBody, &mySessionEvent, sizeof(rum_session_event_t));

                sendRet = rDispatchSendSock.send(sendMsg);
                if( sendRet == true )
                {
                    LOG4CPLUS_DEBUG(*gpLogger, "Send event Message to Scheduler");
                }
                else
                {
                    LOG4CPLUS_ERROR(*gpLogger, "Failed to Send event Message to Scheduler");
                }
                LOG4CPLUS_DEBUG(*gpLogger, "event_time in session_event(Dispatcher): " << mySessionEvent.event_time);
            }
            
        }
        catch (zmq::error_t err)
        {
            LOG4CPLUS_ERROR(*gpLogger, "Failed to receive data from " << RUM_DUMPER_READ_SESSION_TIMEBRK << " with reason: " << err.what());
            continue;
        }
    }
}

static void* realtimeComputing( void* args )
{
    zmq::socket_t* pRealtimeRecvSock = (zmq::socket_t*)args;
    taskRealtimeComputing(pRealtimeRecvSock);

    return NULL;
}

static void* Scheduler( void *args )
{
    schedulerCtx* ctxArgs = (schedulerCtx*)args;
    zmq::socket_t* pPairRecvSock = (zmq::socket_t*)ctxArgs->sock;
    zmq::context_t* pZmqContext = (zmq::context_t*)ctxArgs->context;
    taskSchedule(pPairRecvSock, pZmqContext);

    return NULL;
}

static void* Worker( void* args )
{
    LOG4CPLUS_DEBUG(*gpLogger, "Worker start to work");
    zmq::context_t* pZmqContext = (zmq::context_t*)args;
    taskWork(pZmqContext);

    return NULL;
}

static void* sinkEventHandle( void* args )
{
    LOG4CPLUS_DEBUG(*gpLogger, "sinkEventHandle start to work");
    zmq::context_t* pZmqContext = (zmq::context_t*)args;
    taskSinkEventHandle(pZmqContext);

    return NULL;
}

static void* sinkClean( void* args )
{
    LOG4CPLUS_DEBUG(*gpLogger, "sinkClean start to work");
    zmq::context_t* pZmqContext = (zmq::context_t*)args;
    taskSinkClean(pZmqContext);

    return NULL;
}

static void* sinkAggregate( void* args )
{
    LOG4CPLUS_DEBUG(*gpLogger, "sinkAggregate start to work");
    zmq::context_t* pZmqContext = (zmq::context_t*)args;
    taskSinkAggregate(pZmqContext);

    return NULL;
}

int main (int argc, char *argv [])
{
    int myRet = -1;

    printf("address of gpLogger in dumper_main: %p\n", gpLogger);
    PropertyConfigurator::doConfigure(LOG4CPLUS_TEXT("/etc/rum/log.properties"));

    const char* pConfigFile = RUM_DUMPER_CONFIG_FILE;
    if( argc > 1 )
    {
        pConfigFile = argv[1];
    }
    
    /* Parse configuration file */
    parseConf( pConfigFile );

    if (DumperParamers.logLevel == "DEBUG")
    {
        cout << "configured log_level: " << DumperParamers.logLevel << endl;
        logger_root.setLogLevel(DEBUG_LOG_LEVEL);
    }
    else if (DumperParamers.logLevel == "WARNING")
    {
        logger_root.setLogLevel(WARN_LOG_LEVEL);
    }
    else if (DumperParamers.logLevel == "ERROR")
    {
        logger_root.setLogLevel(ERROR_LOG_LEVEL);
    }
    else if (DumperParamers.logLevel == "FATAL")
    {
        logger_root.setLogLevel(FATAL_LOG_LEVEL);
    }
    else
    {
        logger_root.setLogLevel(INFO_LOG_LEVEL);
    }

    LogLevelManager& llm = getLogLevelManager();
    /*cout << llm.toString(logger_root.getChainedLogLevel()) << endl;
    printf("LogLevel(string): %s\n", DumperParamers.logLevel.c_str());
    */
    LOG4CPLUS_INFO(logger_root, "Start Dispatcher LOG4CPLUS" << endl);

    LOG4CPLUS_INFO(logger_root, "LogLevel(string): " << llm.toString(logger_root.getChainedLogLevel()));
    cout << "LogLevel(string): " << llm.toString(logger_root.getChainedLogLevel()) << endl;

    gListMutex = PTHREAD_MUTEX_INITIALIZER;

    zmq::context_t dispatchPairContext (0);
    zmq::socket_t dispatchRecvSock (dispatchPairContext, ZMQ_PAIR);
    zmq::socket_t dispatchSendSock (dispatchPairContext, ZMQ_PAIR);

    while(1)
    {
        try{
            dispatchRecvSock.bind(RUM_DUMPER_DISPATCH_PAIR_CONNECTION);
            LOG4CPLUS_INFO(logger_root, "Success bind to \"" << RUM_DUMPER_DISPATCH_PAIR_CONNECTION << "\"");
            break;
        }
        catch (zmq::error_t err)
        {
            LOG4CPLUS_ERROR(logger_root, "Failed to bind to \"" << RUM_DUMPER_DISPATCH_PAIR_CONNECTION << "\" with reason: " << err.what());
            sleep(2);
            continue;
        }
    }

    while(1)
    {
        try{
            dispatchSendSock.connect(RUM_DUMPER_DISPATCH_PAIR_CONNECTION);
            LOG4CPLUS_INFO(logger_root, "Success connect to \"" << RUM_DUMPER_DISPATCH_PAIR_CONNECTION << "\"");
            break;
        }
        catch (zmq::error_t err)
        {
            LOG4CPLUS_ERROR(logger_root, "Failed to connect to \"" << RUM_DUMPER_DISPATCH_PAIR_CONNECTION << "\" with reason: " << err.what());
            sleep(2);
            continue;
        }
    }

    int zmqSndHWM = 100000;
    int zmqRcvHWM = 100000;

    zmq::context_t realtimePairContext (0);
    zmq::socket_t realtimeRecvSock (realtimePairContext, ZMQ_PAIR);
    zmq::socket_t realtimeSendSock (realtimePairContext, ZMQ_PAIR);

    realtimeSendSock.setsockopt(ZMQ_SNDHWM, &zmqSndHWM, sizeof(int));
    realtimeRecvSock.setsockopt(ZMQ_RCVHWM, &zmqRcvHWM, sizeof(int));
    //realtimeRecvSock.setsockopt(ZMQ_LINGER, &DumperParamers.dashboardTimerTimeout, sizeof(uint32_t));
    
    while(1)
    {
        try{
            realtimeRecvSock.bind(RUM_DUMPER_REALTIME_PAIR_CONNECTION);
            LOG4CPLUS_INFO(logger_root, "Success bind to \"" << RUM_DUMPER_REALTIME_PAIR_CONNECTION << "\"");
            break;
        }
        catch (zmq::error_t err)
        {
            LOG4CPLUS_ERROR(logger_root, "Failed to bind to \"" << RUM_DUMPER_REALTIME_PAIR_CONNECTION << "\" with reason: " << err.what());
            sleep(2);
            continue;
        }
    }

    while(1)
    {
        try{
            realtimeSendSock.connect(RUM_DUMPER_REALTIME_PAIR_CONNECTION);
            LOG4CPLUS_INFO(logger_root, "Success connect to \"" << RUM_DUMPER_REALTIME_PAIR_CONNECTION << "\"");
            break;
        }
        catch (zmq::error_t err)
        {
            LOG4CPLUS_ERROR(logger_root, "Failed to connect to \"" << RUM_DUMPER_REALTIME_PAIR_CONNECTION << "\" with reason: " << err.what());
            sleep(2);
            continue;
        }
    }
    myRet = zthread_new (realtimeComputing, &realtimeRecvSock);
    if (myRet < 0)
    {
        LOG4CPLUS_ERROR(logger_root, "Create realtimeComputing thread error: " << strerror (errno));
        return -1;
    }

    /* create sink threads */
    zmq::context_t pushPullContext (1);

    myRet = zthread_new (sinkEventHandle, &pushPullContext);
    if (myRet < 0)
    {
        LOG4CPLUS_ERROR(logger_root, "Create event_handler sink thread error: " << strerror (errno));
        return -1;
    }

    myRet = zthread_new (sinkClean, &pushPullContext);
    if (myRet < 0)
    {
        LOG4CPLUS_ERROR(logger_root, "Create cleaner sink thread error: " << strerror (errno));
        return -1;
    }

    myRet = zthread_new (sinkAggregate, &pushPullContext);
    if (myRet < 0)
    {
        LOG4CPLUS_ERROR(logger_root, "Create aggregation sink thread error: " << strerror (errno));
        return -1;
    }
    LOG4CPLUS_INFO(logger_root, "Success to create Sink threads");

    /* create Scheduler thread */
    schedulerCtx args;
    args.sock = &dispatchRecvSock;
    args.context = &pushPullContext;

    myRet = zthread_new (Scheduler, &args);
    if (myRet < 0)
    {
        LOG4CPLUS_ERROR(logger_root, "Create Scheduler thread error: " << strerror (errno));
        return -1;
    }
    LOG4CPLUS_INFO(logger_root, "Success to create Scheduler thread");

    /* create workers thread */
    int myThreadCounts = 0;
    for( int ix = 0; ix < DumperParamers.workerCopies; ++ix )
    {
        myRet = zthread_new (Worker, &pushPullContext);
        if (myRet < 0)
        {
            LOG4CPLUS_ERROR(logger_root, "Create worker thread error: " << strerror (errno));
        }
        else
        {
            myThreadCounts  += 1;
        }
    }

    if( myThreadCounts < (DumperParamers.workerCopies / 2) )
    {
        LOG4CPLUS_ERROR(logger_root, "Created worker threads(" << myThreadCounts << ") less than the half of configed workerCopies(" << DumperParamers.workerCopies << ")");
        return -1;
    }
    LOG4CPLUS_INFO(logger_root, "Success to create Worker threads");

    taskDispatcher(dispatchSendSock, realtimeSendSock);
    return 0;
}
