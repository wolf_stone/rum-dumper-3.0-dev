#include "threads_api.h"
#include "session_computing.h"
#include "dumper_common.h"
#include "debug.h"
#include "redisclient.hpp"

static Logger logger = Logger::getInstance(LOG4CPLUS_TEXT("WorkerFileLogger.WORKLOG"));

bool timebrkToSlaEvent(redisContext*& pRedisCtx, const rum_session_timebrk_t& timebrkItem, rum_session_event_t& slaEvent)
{
    int           myRet = -1;
    uint64_t      response_time = 0;
    uint64_t      tcp_retry_time = 0;                         
    uint64_t      tcp_connection_time = 0;                
    uint64_t      ssl_shake_time = 0;                
    uint64_t      tcp_server_time = 0;                     
    uint64_t      http_network_time = 0;        
    uint64_t      http_server_time = 0;          
    uint64_t      http_download_time = 0;                      

    thresholdsInfo myThresholdsInfo = {0, 0};
    if( timebrkItem.tcp_connection_requests <= 0 )
    {
        LOG4CPLUS_ERROR(logger, "Impossible: connection_requests(" << timebrkItem.tcp_connection_requests << ") less or equal than 0");
        return false;
    }
    if( timebrkItem.tcp_success_connections <= 0 )
    {
        return false;
    }
    if ( timebrkItem.tcp_connection_requests > 1 )
    {
        LOG4CPLUS_ERROR(logger, "Impossible: connection_requests(" << timebrkItem.tcp_connection_requests << ") great than 1");
        return false;
    }
    if ( timebrkItem.tcp_success_connections > 1)
    {
        LOG4CPLUS_ERROR(logger, "Impossible: success_connections(" << timebrkItem.tcp_success_connections << ") great than 1");
        return false;
    }

    if (timebrkItem.service_type == RUM_SERVICE_TYPE_TCP)
    {
        if (timebrkItem.tcp_retry_num > 0)
        {
            //tcp_retry_time = timebrkItem.tcp_retry_time / timebrkItem.tcp_retry_num;
            //tcp_retry_time /= 1000;
            
            tcp_retry_time = timebrkItem.tcp_retry_time;
            response_time += tcp_retry_time ;
        }

        tcp_connection_time = timebrkItem.tcp_connection_time;
        response_time += tcp_connection_time;

        tcp_server_time = timebrkItem.tcp_server_time;
        response_time += tcp_server_time;

    }
    else if (timebrkItem.service_type == RUM_SERVICE_TYPE_TCPSSL)
    {
        if (timebrkItem.ssl_requests <= 0)
        {
            LOG4CPLUS_ERROR(logger, "Impossible: ssl_requests less or equal than 0");
            return false;
        }
        if (timebrkItem.ssl_success_connections <= 0)
        {
            return false;
        }
        if ( timebrkItem.ssl_requests > 1 )
        {
            LOG4CPLUS_ERROR(logger, "Impossible: ssl_requests(" << timebrkItem.ssl_requests << ") great than 1");
            return false;
        }
        if ( timebrkItem.ssl_success_connections > 1)
        {
            LOG4CPLUS_ERROR(logger, "Impossible: ssl_success_connections(" << timebrkItem.ssl_success_connections << ") great than 1");
            return false;
        }
        if (timebrkItem.tcp_retry_num > 0)
        {
            //tcp_retry_time = timebrkItem.tcp_retry_time / timebrkItem.tcp_retry_num;
            //tcp_retry_time /= 1000;

            tcp_retry_time = timebrkItem.tcp_retry_time;
            response_time += tcp_retry_time; 

        }
        tcp_connection_time = timebrkItem.tcp_connection_time;
        ssl_shake_time = timebrkItem.ssl_shake_time;
        tcp_server_time = timebrkItem.tcp_server_time;

        response_time += tcp_connection_time;
        response_time += ssl_shake_time;
        response_time += tcp_server_time;
    }
    else if (timebrkItem.service_type == RUM_SERVICE_TYPE_HTTP)
    {
        if (timebrkItem.http_request_num <= 0)
        {
            LOG4CPLUS_ERROR(logger, "Impossible: HTTP http_request_num less or equal than 0");
            return false;
        }
        if (timebrkItem.http_success_responses <= 0)
        {
            LOG4CPLUS_ERROR(logger, "Impossible: HTTP http_success_responses less or equal than 0");
            return false;
        }
        if ( timebrkItem.http_request_num > 1 )
        {
            LOG4CPLUS_ERROR(logger, "Impossible: HTTP request_num(" << timebrkItem.http_request_num << ") great than 1");
            return false;
        }
        if ( timebrkItem.http_success_responses > 1)
        {
            LOG4CPLUS_ERROR(logger, "Impossible: success_responses(" << timebrkItem.http_success_responses << ") great than 1");
            return false;
        }

        if (timebrkItem.tcp_retry_num > 0)
        {
            //tcp_retry_time = timebrkItem.tcp_retry_time / timebrkItem.tcp_retry_num;
            //tcp_retry_time /= 1000;

            tcp_retry_time = timebrkItem.tcp_retry_time;
            response_time += tcp_retry_time; 

        }
        tcp_connection_time = timebrkItem.tcp_connection_time;
        http_network_time = timebrkItem.http_network_time;
        http_server_time = timebrkItem.http_server_time;
        http_download_time = timebrkItem.http_download_time;

        response_time += tcp_connection_time;
        response_time += http_network_time;
        response_time += http_server_time;
        response_time += http_download_time;

    }
    else if (timebrkItem.service_type == RUM_SERVICE_TYPE_HTTPS)
    {
        if (timebrkItem.ssl_requests <= 0)
        {
            LOG4CPLUS_ERROR(logger, "Impossible: ssl_requests less or equal than 0");
            return false;
        }
        if (timebrkItem.ssl_success_connections <= 0)
        {
            return false;
        }
        if ( timebrkItem.ssl_requests > 1 )
        {
            LOG4CPLUS_ERROR(logger, "Impossible: ssl_requests(" << timebrkItem.ssl_requests << ") great than 1");
            return false;
        }
        if ( timebrkItem.ssl_success_connections > 1)
        {
            LOG4CPLUS_ERROR(logger, "Impossible: ssl_success_connections(" << timebrkItem.ssl_success_connections << ") great than 1");
            return false;
        }
        if (timebrkItem.http_request_num <= 0)
        {
            LOG4CPLUS_ERROR(logger, "Impossible: HTTP http_request_num less or equal than 0");
            return false;
        }
        if (timebrkItem.http_success_responses <= 0)
        {
            //LOG4CPLUS_ERROR(logger, "Impossible: HTTP http_success_responses less or equal than 0");
            return false;
        }
        if ( timebrkItem.http_request_num > 1 )
        {
            LOG4CPLUS_ERROR(logger, "Impossible: HTTP request_num(" << timebrkItem.http_request_num << ") great than 1");
            return false;
        }
        if ( timebrkItem.http_success_responses > 1)
        {
            LOG4CPLUS_ERROR(logger, "Impossible: success_responses(" << timebrkItem.http_success_responses << ") great than 1");
            return false;
        }
        if (timebrkItem.tcp_retry_num > 0)
        {
            //tcp_retry_time = timebrkItem.tcp_retry_time / timebrkItem.tcp_retry_num;
            //tcp_retry_time /= 1000;

            tcp_retry_time = timebrkItem.tcp_retry_time;
            response_time += tcp_retry_time; 

        }

        tcp_connection_time = timebrkItem.tcp_connection_time;
        ssl_shake_time = timebrkItem.ssl_shake_time;
        http_network_time = timebrkItem.http_network_time;
        http_server_time = timebrkItem.http_server_time;
        http_download_time = timebrkItem.http_download_time;

        response_time += tcp_connection_time;
        response_time += ssl_shake_time;
        response_time += http_network_time;
        response_time += http_server_time;
        response_time += http_download_time;

    }

    //Get SLA thresholds from Redis
    myRet = RedisClient::getThresholdsInfo(pRedisCtx, myThresholdsInfo, timebrkItem.collector_id, timebrkItem.service_id);
    if (myRet == 0 )
    {
        LOG4CPLUS_DEBUG(logger, "performanceCritical: " << myThresholdsInfo.performanceCritical);
        if (response_time > myThresholdsInfo.performanceCritical)
        {
            slaEvent.collector_id = timebrkItem.collector_id;
            slaEvent.service_id = timebrkItem.service_id;
            slaEvent.event_time = timebrkItem.end_time;
            slaEvent.event_type = RUM_EVENT_TYPE_SLA;
            strcpy(slaEvent.source_ip, timebrkItem.source_ip);
            slaEvent.source_port = timebrkItem.source_port;
            //strcpy(slaEvent.source_port, timebrkItem.source_port);
            strcpy(slaEvent.event_name, RUM_PERFORMANCE_SLA_RED);
            strcpy(slaEvent.http_request_url, timebrkItem.http_request_url);
            strcpy(slaEvent.http_refer_url, timebrkItem.http_refer_url);

            slaEvent.response_time = response_time;
            slaEvent.tcp_retry_time = tcp_retry_time;
            slaEvent.tcp_connection_time = tcp_connection_time;
            slaEvent.tcp_server_time = tcp_server_time;
            slaEvent.http_network_time = http_network_time;
            slaEvent.http_server_time = http_server_time;
            slaEvent.http_download_time = http_download_time;

            slaEvent.performance_critical = myThresholdsInfo.performanceCritical;
        }
        else
        {
            return false;
        }
    }
    else if (myRet == -1)
    {
        LOG4CPLUS_ERROR(logger, "Failed to get thresholds data from Redis Server");
        return false;
    }
    else if (myRet == -2)
    {
        RedisClient::redisReConnect ( &pRedisCtx, DumperParamers.redisSrvIp, DumperParamers.redisSrvPort );    
        return false;
    }

    return true;

}

void taskWork(zmq::context_t* pZmqContext)
{
    int zmqSndHWM = 100000;
    int zmqRcvHWM = 100000;

    gpLogger = &logger;
    printf("address of gpLogger in worker: %p\n", gpLogger);

    if (DumperParamers.logLevel == "DEBUG")
    {
        logger.setLogLevel(DEBUG_LOG_LEVEL);
    }
    else if (DumperParamers.logLevel == "WARNING")
    {
        logger.setLogLevel(WARN_LOG_LEVEL);
    }
    else if (DumperParamers.logLevel == "ERROR")
    {
        logger.setLogLevel(ERROR_LOG_LEVEL);
    }
    else if (DumperParamers.logLevel == "FATAL")
    {
        logger.setLogLevel(FATAL_LOG_LEVEL);
    }
    else
    {
        logger.setLogLevel(INFO_LOG_LEVEL);
    }
    LOG4CPLUS_INFO(logger, "Start Worker LOG4CPLUS");
    LogLevelManager& llm = getLogLevelManager();
    LOG4CPLUS_INFO(logger, "LogLevel(string): " << llm.toString(logger.getChainedLogLevel()));

    try
    {
        zmq::socket_t taskSendSock (*pZmqContext, ZMQ_PUSH);
        taskSendSock.setsockopt(ZMQ_RCVHWM, &zmqRcvHWM, sizeof(int));

        redisContext* pRedisCtx = NULL;
        //redisAsyncContext* pRedisCtx = NULL;

        pRedisCtx = RedisClient::initRedisContext( DumperParamers.redisSrvIp, DumperParamers.redisSrvPort );    
        if( pRedisCtx == NULL )
        {
            LOG4CPLUS_ERROR(logger, "Failed to connect to Redis Server");
        }
        else
        {
            LOG4CPLUS_INFO(logger, "Success to connect to Redis Server");
        }

        while(1)
        {
            try{
                taskSendSock.connect(RUM_DUMPER_TASK_FINISH_CONNECTION);
                LOG4CPLUS_INFO(logger, "Success connect to \"" << RUM_DUMPER_TASK_FINISH_CONNECTION << "\"");
                break;
            }
            catch (zmq::error_t err)
            {
                LOG4CPLUS_ERROR(logger, "Failed to connect to \"" << RUM_DUMPER_TASK_FINISH_CONNECTION << "\" with reason: " << err.what());
                sleep(2);
                continue;
            }
        }

        zmq::socket_t eventSock (*pZmqContext, ZMQ_PUSH);
        eventSock.setsockopt(ZMQ_SNDHWM, &zmqSndHWM, sizeof(int));
        while(1)
        {
            try{
                eventSock.connect(RUM_DUMPER_EVENT_CONNECTION);
                LOG4CPLUS_INFO(logger, "Success connect to \"" << RUM_DUMPER_EVENT_CONNECTION << "\"");
                break;
            }
            catch (zmq::error_t err)
            {
                LOG4CPLUS_ERROR(logger, "Failed to connect to \"" << RUM_DUMPER_EVENT_CONNECTION << "\" with reason: " << err.what());
                sleep(2);
                continue;
            }
        }

        zmq::socket_t timebrkSock (*pZmqContext, ZMQ_PUSH);
        timebrkSock.setsockopt(ZMQ_SNDHWM, &zmqSndHWM, sizeof(int));
        while(1)
        {
            try{
                timebrkSock.connect(RUM_DUMPER_TIMEBRK_CONNECTION);
                LOG4CPLUS_INFO(logger, "Success connect to \"" << RUM_DUMPER_TIMEBRK_CONNECTION << "\"");
                break;
            }
            catch (zmq::error_t err)
            {
                LOG4CPLUS_ERROR(logger, "Failed to connect to \"" << RUM_DUMPER_TIMEBRK_CONNECTION << "\" with reason: " << err.what());
                sleep(2);
                continue;
            }
        }

        zmq::socket_t taskRecvSock (*pZmqContext, ZMQ_PULL);
        taskRecvSock.setsockopt(ZMQ_RCVHWM, &zmqRcvHWM, sizeof(int));
        while(1)
        {
            try{
                taskRecvSock.connect(RUM_DUMPER_TASK_READY_CONNECTION);
                LOG4CPLUS_INFO(logger, "Success connect to \"" << RUM_DUMPER_TASK_READY_CONNECTION << "\"");
                break;
            }
            catch (zmq::error_t err)
            {
                LOG4CPLUS_ERROR(logger, "Failed to connect to \"" << RUM_DUMPER_TASK_READY_CONNECTION << "\" with reason: " << err.what());
                sleep(2);
                continue;
            }
        }

        zmq::message_t recvMsg(RUM_DUMPER_MSG_DATA_LEN);
        bool myRet = false;
        uint32_t* pServiceId = NULL;

        string prefix = "Worker";
        std::stringstream strstream;
        strstream << pthread_self();
        prefix += strstream.str();
        string dotprefix = strstream.str() + "Dot";

        string prefixResult = prefix + "Result";

        while(1)
        {
            bool isInitialized = false;

            try{
                myRet = taskRecvSock.recv(&recvMsg);
                if( myRet == false )
                {
                    LOG4CPLUS_ERROR(logger, "Failed to get message from \"" <<  RUM_DUMPER_TASK_READY_CONNECTION << "\"");
                    continue;
                }
                else
                {
                    LOG4CPLUS_INFO(logger, "Success to get task ready");
                }
            }
            catch (zmq::error_t err)
            {
                LOG4CPLUS_ERROR(logger, "Failed to receive data in worker with reason: " << err.what());
                continue;
            }
            pServiceId = (uint32_t*) recvMsg.data();
            LOG4CPLUS_DEBUG(logger, "Service_id(Worker-" << pthread_self() << "):" << *pServiceId);

            rum_session_timebrk_t timebrkResult;
            memset(&timebrkResult, 0, sizeof(rum_session_timebrk_t));

            list<rum_session_timebrk_t>& rTimebrkList = gpHashListConsumer->GetTimebrkListById(*pServiceId);
            list<rum_session_timebrk_t>::iterator l_iter;
            for( l_iter=rTimebrkList.begin(); l_iter != rTimebrkList.end(); l_iter++)
            {
                bool isSlaEvent = false;
                rum_session_event_t slaEvent;
                memset(&slaEvent, 0, sizeof(rum_session_event_t));

                LOG4CPLUS_DEBUG(logger, "end_time of session_timebrk: " << l_iter->end_time);
                LOG4CPLUS_DEBUG(logger, "http_success_responses of session_timebrk: " << l_iter->http_success_responses );
                LOG4CPLUS_DEBUG(logger, "http_success_responses of timebrkResult Before Add: " << timebrkResult.http_success_responses );

                sessionTimebrkSummation(*l_iter, timebrkResult);
                LOG4CPLUS_DEBUG(logger, "http_success_responses of http_request_num : " << timebrkResult.http_success_responses );
                sessionDisplay(prefixResult , timebrkResult);
                if (isInitialized == false)
                {
                    isInitialized = true;
                    time_t t_start = (*l_iter).end_time - (*l_iter).end_time % RUM_FIVE_MINUTES_SECS;
                    time_t t_end = t_start + RUM_FIVE_MINUTES_SECS;
                    timebrkResult.collector_id = (*l_iter).collector_id;                       
                    timebrkResult.start_time = t_start;                         
                    timebrkResult.end_time = t_end;
                    timebrkResult.service_id = l_iter->service_id;
                    timebrkResult.service_type = l_iter->service_type;
                }

                isSlaEvent = timebrkToSlaEvent(pRedisCtx, *l_iter, slaEvent);
                if (isSlaEvent == true)
                {
                    zmq::message_t eventSendMsg(RUM_DUMPER_MSG_DATA_LEN);        
                    memcpy(eventSendMsg.data(), &slaEvent, RUM_DUMPER_MSG_DATA_LEN);
                    eventSock.send(eventSendMsg);
                }
            }
            //sessionDisplay(dotprefix , timebrkResult);

            zmq::message_t serviceIdSendMsg(RUM_ID_LEN);        
            memcpy(serviceIdSendMsg.data(), pServiceId, RUM_ID_LEN);
            taskSendSock.send(serviceIdSendMsg);

            zmq::message_t resultSendMsg(RUM_DUMPER_MSG_DATA_LEN);        
            memcpy(resultSendMsg.data(), &timebrkResult, RUM_DUMPER_MSG_DATA_LEN);
            timebrkSock.send(resultSendMsg);
        }
    }

    catch (zmq::error_t err)
    {
        LOG4CPLUS_ERROR(logger, "Failed to initilize worker with reason: " << err.what());
    }
}
