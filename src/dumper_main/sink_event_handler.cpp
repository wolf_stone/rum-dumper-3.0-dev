#include "threads_api.h"
#include "dumper_common.h"
#include "database_api.h"

static Logger logger = Logger::getInstance(LOG4CPLUS_TEXT("EventFileLogger.EVENTLOG"));

void taskSinkEventHandle(zmq::context_t* pZmqContext)
{
    int zmqRcvHWM = 100000;
    rum_session_event_t* pSessionEvent = NULL;

    gpLogger = &logger;
    printf("address of gpLogger in sink_event: %p\n", gpLogger);

    if (DumperParamers.logLevel == "DEBUG")
    {
        logger.setLogLevel(DEBUG_LOG_LEVEL);
    }
    else if (DumperParamers.logLevel == "WARNING")
    {
        logger.setLogLevel(WARN_LOG_LEVEL);
    }
    else if (DumperParamers.logLevel == "ERROR")
    {
        logger.setLogLevel(ERROR_LOG_LEVEL);
    }
    else if (DumperParamers.logLevel == "FATAL")
    {
        logger.setLogLevel(FATAL_LOG_LEVEL);
    }
    else
    {
        logger.setLogLevel(INFO_LOG_LEVEL);
    }

    LOG4CPLUS_INFO(logger, "Start sink_event LOG4CPLUS");

    char  conninfo[DB_CONFIG_LEN];
    PGconn      *conn=NULL;

    snprintf(conninfo,DB_CONFIG_LEN,"dbname=%s host=%s port=%d user=%s password=%s", (DumperParamers.dbName).c_str(), (DumperParamers.dbSrvIp).c_str(),DumperParamers.dbSrvPort,(DumperParamers.dbSrvUser).c_str(),(DumperParamers.dbSrvPasswd).c_str());

    while(1)
    {
        conn = PQconnectdb(conninfo);
        if (false == checkConnection(conn, conninfo)) 
        {
            PQfinish(conn);
            sleep(2);
            continue;
        }
        else
        {
            PQfinish(conn);
            break;
        }
    }

    try
    {
        zmq::socket_t eventSock (*pZmqContext, ZMQ_PULL);
        eventSock.setsockopt(ZMQ_RCVHWM, &zmqRcvHWM, sizeof(int));

        while(1)
        {
            try{
                eventSock.bind(RUM_DUMPER_EVENT_CONNECTION);
                LOG4CPLUS_INFO(logger, "Success bind to \"" << RUM_DUMPER_EVENT_CONNECTION << "\"");
                break;
            }
            catch (zmq::error_t err)
            {
                LOG4CPLUS_ERROR(logger, "Failed to bind to \"" << RUM_DUMPER_EVENT_CONNECTION << "\" with reason: " << err.what());
                sleep(2);
                continue;
            }
        }
        
        zmq::message_t recvMsg(RUM_DUMPER_MSG_DATA_LEN);
        bool myRet = false;
        
        while(1)
        {
            try{
                myRet = eventSock.recv(&recvMsg);
                if (myRet == false)
                {
                    LOG4CPLUS_ERROR(logger, "sinkEventHandle failed to get message");
                    continue;
                }
            }
            catch (zmq::error_t err)
            {
                LOG4CPLUS_ERROR(logger, "Failed to receive data in event_handler with reason: " << err.what());
                continue;
            }

            pSessionEvent = (rum_session_event_t*)recvMsg.data();

            myRet = dbAddSessionEvent( conninfo, pSessionEvent );
            if (myRet == false)
            {
                LOG4CPLUS_ERROR(logger, "Failed to add session event into database");
            }
            
        }
    }
    catch (std::exception e)
    {
        LOG4CPLUS_ERROR(logger, "Failed to initilize sinkEventHandle with reason: " << e.what());
    }
    PQfinish(conn);
}
