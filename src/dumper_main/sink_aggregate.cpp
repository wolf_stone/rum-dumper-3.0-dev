#include "threads_api.h"
#include "dumper_common.h"
#include "debug.h"
#include "database_api.h"
#include "session_computing.h"

//static time_t gsLastHour = 0;
//static time_t gsLastDay = 0;
char  conninfo[DB_CONFIG_LEN];

HistoryMap hourDayhistory;

static Logger logger = Logger::getInstance(LOG4CPLUS_TEXT("AggregateFileLogger.AGGRLOG"));

void initializeTimeOptions()
{
    //time_t dbMaxEndMinute = 0;

    bool dbRet = false;

    dbRet = dbSetHourDayHistory(conninfo, hourDayhistory);
    HistoryMap::iterator mapIter;
    for (mapIter = hourDayhistory.begin(); mapIter != hourDayhistory.end(); mapIter++)
    {
        LOG4CPLUS_DEBUG(logger, "ServiceId: " << mapIter->first);
        LOG4CPLUS_DEBUG(logger, "LastHour: " << mapIter->second.lastHour);
        LOG4CPLUS_DEBUG(logger, "LastDay: " << mapIter->second.lastDay);
    }
    //dbRet = dbGetMaxEndHour(conninfo, gsLastHour);
    //dbRet = dbGetMaxEndDay(conninfo, gsLastDay);
}

void addAllSessionTimebrk(const vector<rum_session_timebrk_t>& sessonTimebrkVec, rum_session_timebrk_t& timebrkResult)
{
    memset(&timebrkResult, 0, sizeof(rum_session_timebrk_t));
    timebrkResult.service_id = sessonTimebrkVec[0].service_id;
    timebrkResult.collector_id = sessonTimebrkVec[0].collector_id;
    timebrkResult.service_type = sessonTimebrkVec[0].service_type;

    for (size_t ix = 0; ix < sessonTimebrkVec.size(); ++ix)
    {
        sessionTimebrkSummation(sessonTimebrkVec[ix], timebrkResult);

        //sessionDisplay("addAllSessionTimebrk Item ", sessonTimebrkVec[ix]);
        /*timebrkResult.retry_num += sessonTimebrkVec[ix].retry_num; 
        timebrkResult.retry_time += sessonTimebrkVec[ix].retry_time;

        timebrkResult.connection_requests += sessonTimebrkVec[ix].connection_requests;
        timebrkResult.success_connections += sessonTimebrkVec[ix].success_connections;
        timebrkResult.connection_time += sessonTimebrkVec[ix].connection_time;

        timebrkResult.server_time += sessonTimebrkVec[ix].server_time;
        timebrkResult.throughput += sessonTimebrkVec[ix].throughput;

        timebrkResult.ssl_requests += sessonTimebrkVec[ix].ssl_requests;
        timebrkResult.ssl_success_connections += sessonTimebrkVec[ix].ssl_success_connections;
        timebrkResult.ssl_shake_time += sessonTimebrkVec[ix].ssl_shake_time;

        timebrkResult.authentication_requests += sessonTimebrkVec[ix].authentication_requests;
        timebrkResult.success_authentications += sessonTimebrkVec[ix].success_authentications;
        timebrkResult.authentication_time += sessonTimebrkVec[ix].authentication_time;

        timebrkResult.request_num += sessonTimebrkVec[ix].request_num;
        timebrkResult.success_responses += sessonTimebrkVec[ix].success_responses;
        timebrkResult.network_time2first_buffer += sessonTimebrkVec[ix].network_time2first_buffer;
        timebrkResult.server_time2first_buffer += sessonTimebrkVec[ix].server_time2first_buffer;
        timebrkResult.download_time += sessonTimebrkVec[ix].download_time;
        timebrkResult.download_size += sessonTimebrkVec[ix].download_size;
        */
    }
}

void doHourAggregate(time_t lastHour, time_t thisHour, uint32_t service_id)
{
    time_t tmpHour = lastHour + RUM_ONE_HOUR_SECS;
    bool myRet = false;
    rum_session_timebrk_t timebrkResult;

    while (tmpHour <= thisHour)
    {
        vector<rum_session_timebrk_t> sessonTimebrkVec;
        getMinuteRecords(conninfo, lastHour, tmpHour, service_id, sessonTimebrkVec);
        if (sessonTimebrkVec.size() > 0)
        {
            addAllSessionTimebrk(sessonTimebrkVec, timebrkResult);
            timebrkResult.start_time = lastHour;
            timebrkResult.end_time = tmpHour;

            sessionDisplay("addAllSessionTimebrk Result(Hour) ", timebrkResult);

            myRet = dbAddSessionTimebrkHour(conninfo, &timebrkResult);
            if (myRet == false)
            {
                LOG4CPLUS_ERROR(logger, "Failed to add hour record into session hour db");
            }
        }
        sessonTimebrkVec.clear();
        lastHour += RUM_ONE_HOUR_SECS;
        tmpHour += RUM_ONE_HOUR_SECS;
    }
}

void doDayAggregate(time_t lastDay, time_t thisDay, uint32_t service_id)
{
    time_t tmpDay = lastDay + RUM_ONE_DAY_SECS;
    bool myRet = false;
    rum_session_timebrk_t timebrkResult;

    while (tmpDay <= thisDay)
    {
        vector<rum_session_timebrk_t> sessonTimebrkVec;
        getHourRecords(conninfo, lastDay, tmpDay, service_id, sessonTimebrkVec);
        if (sessonTimebrkVec.size() > 0)
        {
            addAllSessionTimebrk(sessonTimebrkVec, timebrkResult);
            timebrkResult.start_time = lastDay;
            timebrkResult.end_time = tmpDay;

            sessionDisplay("addAllSessionTimebrk Result(Day)", timebrkResult);

            myRet = dbAddSessionTimebrkDay(conninfo, &timebrkResult);
            if (myRet == false)
            {
                LOG4CPLUS_ERROR(logger, "Failed to add day record into session day db");
            }
        }
        sessonTimebrkVec.clear();
        lastDay += RUM_ONE_DAY_SECS;
        tmpDay += RUM_ONE_DAY_SECS;
    }
}

void sessionTimebrkAggregate(uint32_t service_id, time_t end_time)
{
    time_t lastHour = hourDayhistory[service_id].lastHour;
    time_t lastDay = hourDayhistory[service_id].lastDay;

    time_t thisHour = end_time;
    thisHour = thisHour - thisHour % RUM_ONE_HOUR_SECS; 

    LOG4CPLUS_INFO(logger, "thisHour : " << string(ctime(&thisHour))); 
    LOG4CPLUS_INFO(logger, "lastHour : " << string(ctime(&lastHour))); 

    if (lastHour == 0)
    {
        if (thisHour % RUM_ONE_HOUR_SECS == 0)
        {
            lastHour = thisHour - RUM_ONE_HOUR_SECS;
        }
        else
        {
            lastHour = thisHour; 

            LOG4CPLUS_INFO(logger, "Update lastHour from " << string(ctime(&(hourDayhistory[service_id].lastHour)))); 

            hourDayhistory[service_id].lastHour = thisHour;

            LOG4CPLUS_INFO(logger, "To " << string(ctime(&thisHour))); 
        }
    }

    if ( thisHour - lastHour >= RUM_ONE_HOUR_SECS)
    {
        doHourAggregate(lastHour, thisHour, service_id);

        LOG4CPLUS_INFO(logger, "Update lastHour from " << string(ctime(&(hourDayhistory[service_id].lastHour)))); 

        hourDayhistory[service_id].lastHour = thisHour;

        LOG4CPLUS_INFO(logger, "To " << string(ctime(&thisHour))); 
    }


    time_t thisDay = getDayLeftBound(end_time);

    LOG4CPLUS_INFO(logger, "thisDay : " << string(ctime(&thisDay))); 
    LOG4CPLUS_INFO(logger, "lastDay : " << string(ctime(&lastDay))); 

    if (lastDay == 0)
    {
        if (thisDay == end_time)                    //end_time is the right end of one day
        {
            lastDay = thisDay - RUM_ONE_DAY_SECS;
        }
        else
        {
            lastDay = thisDay;

            LOG4CPLUS_INFO(logger, "Update gsLastDay from " << string(ctime(&(hourDayhistory[service_id].lastDay)))); 

            hourDayhistory[service_id].lastDay = thisDay;

            LOG4CPLUS_INFO(logger, "To " << string(ctime(&thisDay))); 
        }
    }

    if (thisDay - lastDay >= RUM_ONE_DAY_SECS)
    {
        doDayAggregate(lastDay, thisDay, service_id);

        LOG4CPLUS_INFO(logger, "Update gsLastDay from " << string(ctime(&(hourDayhistory[service_id].lastDay)))); 

        hourDayhistory[service_id].lastDay = thisDay;

        LOG4CPLUS_INFO(logger, "To " << string(ctime(&thisDay))); 

    }

    /*if (thisDay > sThisDay)
    {
        LOG(INFO) << "Update sThisDay from " << string(ctime(&sThisDay)); 

        sThisDay = thisDay;

        LOG(INFO) << "To " << string(ctime(&sThisDay)); 
        LOG(INFO) << "gsLastDay : " << string(ctime(&gsLastDay)); 

        if (sThisDay - gsLastDay >= RUM_ONE_DAY_SECS)
        {
            doDayAggregate(gsLastDay, sThisDay, service_id);

            LOG(INFO) << "Update gsLastDay from " << string(ctime(&gsLastDay)); 

            gsLastDay = sThisDay;

            LOG(INFO) << "To " << string(ctime(&gsLastDay)); 

        }
    }*/
}

void allSessionTimebrkAggregate(time_t end_time)
{
    time_t lastHour = 0;
    time_t lastDay = 0;
    uint32_t serviceId = 0;

    HistoryMap::iterator mapIter = hourDayhistory.begin();
    while (mapIter != hourDayhistory.end())
    {
        serviceId = mapIter->first;
        lastHour = mapIter->second.lastHour;
        lastDay = mapIter->second.lastDay;

        sessionTimebrkAggregate(serviceId, end_time);
        mapIter++; 
    }
}

void taskSinkAggregate(zmq::context_t* pZmqContext)
{
    gpLogger = &logger;
    printf("address of gpLogger in sink_aggregate: %p\n", gpLogger);

    if (DumperParamers.logLevel == "DEBUG")
    {
        logger.setLogLevel(DEBUG_LOG_LEVEL);
    }
    else if (DumperParamers.logLevel == "WARNING")
    {
        logger.setLogLevel(WARN_LOG_LEVEL);
    }
    else if (DumperParamers.logLevel == "ERROR")
    {
        logger.setLogLevel(ERROR_LOG_LEVEL);
    }
    else if (DumperParamers.logLevel == "FATAL")
    {
        logger.setLogLevel(FATAL_LOG_LEVEL);
    }
    else
    {
        logger.setLogLevel(INFO_LOG_LEVEL);
    }
    LOG4CPLUS_INFO(logger, "Start sink_aggregate LOG4CPLUS");

    PGconn      *conn=NULL;

    snprintf(conninfo,DB_CONFIG_LEN,"dbname=%s host=%s port=%d user=%s password=%s", (DumperParamers.dbName).c_str(), (DumperParamers.dbSrvIp).c_str(),DumperParamers.dbSrvPort,(DumperParamers.dbSrvUser).c_str(),(DumperParamers.dbSrvPasswd).c_str());

    while(1)
    {
        conn = PQconnectdb(conninfo);
        if (false == checkConnection(conn, conninfo)) 
        {
            PQfinish(conn);
            sleep(2);
            continue;
        }
        else
        {
            break;
        }
    }

    initializeTimeOptions();

    int zmqRcvHWM = 100000;
    rum_session_timebrk_t* pSessionTimebrk = NULL;
    try
    {
        zmq::socket_t timebrkSock (*pZmqContext, ZMQ_PULL);
        timebrkSock.setsockopt (ZMQ_RCVHWM, &zmqRcvHWM, sizeof(int));

        while(1)
        {
            try{
                timebrkSock.bind (RUM_DUMPER_TIMEBRK_CONNECTION);
                LOG4CPLUS_INFO(logger, "Success bind to \"" << RUM_DUMPER_TIMEBRK_CONNECTION << "\"");
                break;
            }
            catch (zmq::error_t err)
            {
                LOG4CPLUS_ERROR(logger, "Failed to bind to \"" << RUM_DUMPER_TIMEBRK_CONNECTION << "\" with reason: " << err.what());
                sleep(2);
                continue;
            }
        }

        zmq::message_t recvMsg (RUM_DUMPER_MSG_DATA_LEN);
        bool myRet = false;
        
        while(1)
        {
            try{
                myRet = timebrkSock.recv(&recvMsg);
                if( myRet == false )
                {
                    LOG4CPLUS_ERROR(logger, "Failed to get message from \"" <<  RUM_DUMPER_TIMEBRK_CONNECTION << "\"");
                    continue;
                }
            }
            catch (zmq::error_t err)
            {
                LOG4CPLUS_ERROR(logger, "Failed to receive data in sink_aggregate with reason: " << err.what());
                continue;
            }

            pSessionTimebrk = (rum_session_timebrk_t*)recvMsg.data();
            sessionDisplay("sinkAggregate ", *pSessionTimebrk);
            if (pSessionTimebrk->flag == FLAG_OUTDATED_TIMEBRK)
            {
                LOG4CPLUS_INFO(logger, "This is outdated SessionTimebrk");
                myRet = dbAddSessionTimebrkSingle(conninfo, pSessionTimebrk);
                if (myRet == false)
                {
                    LOG4CPLUS_ERROR(logger, "Failed to add outdated session timebrk into session timebrk single db");
                }
                sessionDisplay("sinkAggregate Outdated ", *pSessionTimebrk);
                dbAddOutdatedSessionTimebrk(conninfo, pSessionTimebrk);
                continue;
            }
            if (pSessionTimebrk->flag == FLAG_TIMER_NOTIFICATION)
            {
                LOG4CPLUS_INFO(logger, "This is timeout notification");
                allSessionTimebrkAggregate(pSessionTimebrk->end_time);
                continue;
            }

            myRet = dbAddSessionTimebrkMinute(conninfo, pSessionTimebrk);
            if (myRet == false)
            {
                LOG4CPLUS_ERROR(logger, "Failed to add Five minute result into Database");
            }
            LOG4CPLUS_INFO(logger, "Received aggregate Task");
            sessionTimebrkAggregate(pSessionTimebrk->service_id, pSessionTimebrk->end_time);
        }
    }
    catch (std::exception e)
    {
        LOG4CPLUS_ERROR(logger, "Failed to initilize sinkAggregate with reason: " << e.what());
    }
}
