#include "threads_api.h"
#include "dumper_common.h"
#include "database_api.h"
#include "debug.h"

static Logger logger = Logger::getInstance(LOG4CPLUS_TEXT("ClearnerFileLogger.CLEANLOG"));

void  taskSinkClean(zmq::context_t* pZmqContext)
{
    int zmqRcvHWM = 100000;

    gpLogger = &logger;
    printf("address of gpLogger in sink_clean: %p\n", gpLogger);

    if (DumperParamers.logLevel == "DEBUG")
    {
        logger.setLogLevel(DEBUG_LOG_LEVEL);
    }
    else if (DumperParamers.logLevel == "WARNING")
    {
        logger.setLogLevel(WARN_LOG_LEVEL);
    }
    else if (DumperParamers.logLevel == "ERROR")
    {
        logger.setLogLevel(ERROR_LOG_LEVEL);
    }
    else if (DumperParamers.logLevel == "FATAL")
    {
        logger.setLogLevel(FATAL_LOG_LEVEL);
    }
    else
    {
        logger.setLogLevel(INFO_LOG_LEVEL);
    }
    LOG4CPLUS_INFO(logger, "Start sink_clean LOG4CPLUS");

    char  conninfo[DB_CONFIG_LEN];
    PGconn      *conn=NULL;

    snprintf(conninfo,DB_CONFIG_LEN,"dbname=%s host=%s port=%d user=%s password=%s", (DumperParamers.dbName).c_str(), (DumperParamers.dbSrvIp).c_str(),DumperParamers.dbSrvPort,(DumperParamers.dbSrvUser).c_str(),(DumperParamers.dbSrvPasswd).c_str());

    while(1)
    {
        conn = PQconnectdb(conninfo);
        if (false == checkConnection(conn, conninfo)) 
        {
            PQfinish(conn);
            sleep(2);
            continue;
        }
        else
        {
            break;
        }
    }
    
    try
    {
        zmq::socket_t taskSock (*pZmqContext, ZMQ_PULL);
        taskSock.setsockopt(ZMQ_RCVHWM, &zmqRcvHWM, sizeof(int));

        while(1)
        {
            try{
                taskSock.bind(RUM_DUMPER_TASK_FINISH_CONNECTION);
                LOG4CPLUS_INFO(logger, "Success bind to \"" << RUM_DUMPER_TASK_FINISH_CONNECTION << "\"");
                break;
            }
            catch (zmq::error_t err)
            {
                LOG4CPLUS_ERROR(logger, "Failed to bind to \"" << RUM_DUMPER_TASK_FINISH_CONNECTION << "\" with reason: " << err.what());
                sleep(2);
                continue;
            }
        }

        zmq::message_t recvMsg(RUM_ID_LEN);
        bool myRet = false;
        uint32_t* pServiceId = NULL;
        
        while(1)
        {
            try{
                myRet = taskSock.recv(&recvMsg);
                if( myRet == false )
                {
                    LOG4CPLUS_ERROR(logger, "Failed to get message from \"" << RUM_DUMPER_TASK_FINISH_CONNECTION << "\"");
                    continue;
                }
                else
                {
                    LOG4CPLUS_INFO(logger, "Received clean task");
                }
            }
            catch (zmq::error_t err)
            {
                LOG4CPLUS_ERROR(logger, "Failed to receive data in sink_cleaner with reason: " << err.what());
                continue;
            }

            pServiceId = (uint32_t*) recvMsg.data();
            list<rum_session_timebrk_t>& rTimebrkList = gpHashListConsumer->GetTimebrkListById(*pServiceId);
            list<rum_session_timebrk_t>::iterator l_iter;
            for( l_iter=rTimebrkList.begin(); l_iter != rTimebrkList.end(); l_iter++)
            {
                sessionDisplay("sinkClean", *l_iter);
                /*myRet = dbAddSessionTimebrkSingle(conninfo, &(*l_iter));
                //TODO: Large time overhead, need to be re-design
                if (myRet == false)
                {
                    LOG4CPLUS_ERROR(logger, "Failed to add sessoin timebrk into database");
                }
                else
                {
                    LOG4CPLUS_DEBUG(logger, "Successed to add sessoin timebrk into database");
                }*/
            }
            LOG4CPLUS_DEBUG(logger, "Service_id(sinkClean): " << *pServiceId);
            gpHashListConsumer->DecCleanTaskFinished();
            if (gpHashListConsumer->IsCleanTaskAllFinished() == true)
            {
                delete gpHashListConsumer;
                __sync_lock_release(&gpHashListConsumer);
                if(__sync_lock_test_and_set(&gpHashListConsumer,NULL) == NULL)
                {
                    LOG4CPLUS_INFO(logger, "Success to free gpHashListConsumer");
                }
            }
        }
    }
    catch (std::exception e)
    {
        LOG4CPLUS_ERROR(logger, "Failed to initilize sinkClean with reason: " << e.what());
    }
}
