#include <time.h>
#include <string.h>
#include "SessionHashList.h"
#include "session_computing.h"
#include "debug.h"

/*static void timebrkSummation(const rum_session_timebrk_t& timebrkItem,rum_session_timebrk_t& timebrkResult)
{
    timebrkResult.retry_num += timebrkItem.retry_num; 
    timebrkResult.retry_time += timebrkItem.retry_time;

    timebrkResult.connection_requests += timebrkItem.connection_requests;
    timebrkResult.success_connections += timebrkItem.success_connections;
    timebrkResult.connection_time += timebrkItem.connection_time;

    timebrkResult.server_time += timebrkItem.server_time;
    timebrkResult.throughput += timebrkItem.throughput;

    timebrkResult.ssl_requests += timebrkItem.ssl_requests;
    timebrkResult.ssl_success_connections += timebrkItem.ssl_success_connections;
    timebrkResult.ssl_shake_time += timebrkItem.ssl_shake_time;

    timebrkResult.authentication_requests += timebrkItem.authentication_requests;
    timebrkResult.success_authentications += timebrkItem.success_authentications;
    timebrkResult.authentication_time += timebrkItem.authentication_time;

    timebrkResult.request_num += timebrkItem.request_num;
    timebrkResult.success_responses += timebrkItem.success_responses;
    timebrkResult.network_time2first_buffer += timebrkItem.network_time2first_buffer;
    timebrkResult.server_time2first_buffer += timebrkItem.server_time2first_buffer;
    timebrkResult.download_time += timebrkItem.download_time;
    timebrkResult.download_size += timebrkItem.download_size;
}*/

RumSessionHashlist::RumSessionHashlist():workerFinishedIdSize(0), cleanFinishedIdSize(0)
{
    gpHashListPtr = new(nothrow) SessionHashlist;
    if (gpHashListPtr == NULL)
    {
        LOG4CPLUS_FATAL(*gpLogger, "Failed to alloc memory for gpHashListPtr");
    }
    workerIdMutex = PTHREAD_MUTEX_INITIALIZER;
    cleanIdMutex = PTHREAD_MUTEX_INITIALIZER;
}

RumSessionHashlist::~RumSessionHashlist()
{
    ClearSessionHashList();
}

bool RumSessionHashlist::InsertTimebrkByService(rum_session_timebrk_t* pSessionTimebrk)
{
    map<uint32_t, list<rum_session_timebrk_t> >::iterator mapIter;
    if (pSessionTimebrk == NULL)
    {
        LOG4CPLUS_ERROR(*gpLogger, "SessionTimebrk to be inserted is NULL");
        return false;
    }
    mapIter = gpHashListPtr->find(pSessionTimebrk->service_id);
    if (mapIter != gpHashListPtr->end())
    {   
        //It is impossible that key exsist and the related list size is 0
        
        //LOG4CPLUS_DEBUG(*gpLogger, "service_id: " << mapIter->first);
        list<rum_session_timebrk_t>::reverse_iterator thisIter = mapIter->second.rbegin();
        //list<rum_session_timebrk_t>::reverse_iterator lastIter = thisIter;
        LOG4CPLUS_DEBUG(*gpLogger, "Size of list: " << mapIter->second.size());

        if (pSessionTimebrk->end_time >= thisIter->end_time)
        {
            //LOG4CPLUS_DEBUG(*gpLogger, " end_time of pSessionTimebrk(The max end_time): " << pSessionTimebrk->end_time);
            //LOG4CPLUS_DEBUG(*gpLogger, " end_time of thisIter(The max end_time): " << thisIter->end_time);
            mapIter->second.push_back(*pSessionTimebrk);
            return true;
        }

        while (thisIter != mapIter->second.rend())
        {
            //LOG4CPLUS_DEBUG(*gpLogger, " end_time of pSessionTimebrk: " << pSessionTimebrk->end_time);
            //LOG4CPLUS_DEBUG(*gpLogger, " end_time of thisIter: " << thisIter->end_time);
            if (pSessionTimebrk->end_time >= thisIter->end_time)
            {
                mapIter->second.insert(thisIter.base(), *pSessionTimebrk);
                break;
            }
            thisIter++;
        }
        if (thisIter == mapIter->second.rend())
        {
            mapIter->second.push_front(*pSessionTimebrk);
        }

    }
    else
    {
        list<rum_session_timebrk_t> list_rum_session_timebrk;
        list_rum_session_timebrk.push_back(*pSessionTimebrk);
        gpHashListPtr->insert(map<uint32_t, list<rum_session_timebrk_t> >::value_type(pSessionTimebrk->service_id,list_rum_session_timebrk));
        ++workerFinishedIdSize;                
        ++cleanFinishedIdSize;               
    }
    return true;

}

size_t RumSessionHashlist::GetSize()
{
    size_t timebrkSize = 0;
    map<uint32_t, list<rum_session_timebrk_t> >::iterator mapIter = gpHashListPtr->begin();
    for (; mapIter != gpHashListPtr->end(); mapIter++)
    {
        timebrkSize += mapIter->second.size();
    }
    return timebrkSize; 
}

bool RumSessionHashlist::InsertTimeBrkByCollector(rum_session_timebrk_t* pSessionTimebrk)
{
    map<uint32_t, list<rum_session_timebrk_t> >::iterator mapIter;
    if (pSessionTimebrk == NULL)
    {
        LOG4CPLUS_ERROR(*gpLogger, "SessionTimebrk to be inserted is NULL");
        return false;
    }
    mapIter = gpHashListPtr->find(pSessionTimebrk->collector_id);
    if (mapIter != gpHashListPtr->end())
    {
        mapIter->second.push_back(*pSessionTimebrk);
    }
    else
    {
        list<rum_session_timebrk_t> list_rum_session_timebrk;
        list_rum_session_timebrk.push_back(*pSessionTimebrk);
        gpHashListPtr->insert(map<uint32_t, list<rum_session_timebrk_t> >::value_type(pSessionTimebrk->collector_id,list_rum_session_timebrk));
    }
    return true;

}

SessionHashlistPtr RumSessionHashlist::GetSessionHashlistPtr()
{
    return gpHashListPtr;
}

void RumSessionHashlist::RemoveOutdatedTimebrk(time_t startSeconds)
{
    map<uint32_t, list<rum_session_timebrk_t> >::iterator mapIter = gpHashListPtr->begin();
    for (; mapIter != gpHashListPtr->end(); mapIter++)
    {
        //LOG4CPLUS_DEBUG(*gpLogger, "service_id in realtime_computing: " << mapIter->first);

        list<rum_session_timebrk_t>::iterator listIter = mapIter->second.begin();
        while (listIter != mapIter->second.end())
        {
            //LOG4CPLUS_DEBUG(*gpLogger, "end_time in realtime_computing: " << listIter->end_time);
            if (listIter->end_time <= startSeconds)
            {
                rum_session_timebrk_t mySessionTimebrk;
                //LOG4CPLUS_DEBUG(*gpLogger, "end_time to be removed in realtime_computing: " << listIter->end_time);
                if (mapIter->second.size() == 1)
                {
                    mySessionTimebrk = mapIter->second.front();
                    uint32_t serviceId = mySessionTimebrk.service_id;
                    uint32_t collectorId = mySessionTimebrk.collector_id;

                    memset(&mySessionTimebrk, 0, sizeof(rum_session_timebrk_t));
                    mySessionTimebrk.service_id = serviceId; 
                    mySessionTimebrk.collector_id = collectorId;
                    mapIter->second.pop_front();
                    mapIter->second.push_front(mySessionTimebrk);
                    break;
                }
                else{
                    mapIter->second.erase(listIter++);
                }
            }
            else
            {
                break;
            }
        }
    }
    LOG4CPLUS_DEBUG(*gpLogger, "Size of HashList: " << gpHashListPtr->size());
}

void RumSessionHashlist::Summation(RumSessionHashlist& resultHashList, time_t startTime, time_t endTime)
{
    LOG4CPLUS_DEBUG(*gpLogger, "DoSummation");
    //cout <<  "DoSummation" << endl;
    map<uint32_t, list<rum_session_timebrk_t> >::iterator mapIter = gpHashListPtr->begin();
    for (; mapIter != gpHashListPtr->end(); mapIter++)
    {
        //cout <<  "DoSummation collector_id:" << mapIter->first << endl;
        LOG4CPLUS_DEBUG(*gpLogger, "DoSummation collector_id:" << mapIter->first);
        bool isInitilized = false;
        rum_session_timebrk_t timebrkResult;
        memset(&timebrkResult, 0, sizeof(rum_session_timebrk_t));
        timebrkResult.start_time = startTime;
        timebrkResult.end_time = endTime;
        LOG4CPLUS_DEBUG(*gpLogger, "DoSummation service_id: " << mapIter->first);
        LOG4CPLUS_DEBUG(*gpLogger, "DoSummation start_time: " << startTime);
        LOG4CPLUS_DEBUG(*gpLogger, "DoSummation end_time: " << endTime);
        if (mapIter->second.size() > 0)
        {
            LOG4CPLUS_DEBUG(*gpLogger, "DoSummation list size: " << mapIter->second.size());
            list<rum_session_timebrk_t>::iterator listIter = mapIter->second.begin();
            while (listIter != mapIter->second.end())
            {   
                if (isInitilized == false)
                {
                    isInitilized = true;
                    timebrkResult.service_id = listIter->service_id;
                    timebrkResult.collector_id = listIter->collector_id;
                    LOG4CPLUS_DEBUG(*gpLogger, "DoSummation collector_id: " << timebrkResult.collector_id);
                }
                LOG4CPLUS_DEBUG(*gpLogger, "timebrk end_time: " << listIter->end_time);
                //cout << "timebrk end_time: " << listIter->end_time << endl;
                LOG4CPLUS_DEBUG(*gpLogger, "request_num before Add: " << timebrkResult.http_request_num);
                //cout << "request_num before Add: " << timebrkResult.request_num << endl;
                sessionTimebrkSummation(*listIter, timebrkResult);
                LOG4CPLUS_DEBUG(*gpLogger, "request_num after Add: " << timebrkResult.http_request_num);
                //cout << "request_num after Add: " << timebrkResult.request_num << endl;
                listIter++;
            }
            resultHashList.InsertTimeBrkByCollector(&timebrkResult);
        }
    }
}

list<rum_session_timebrk_t>& RumSessionHashlist::GetTimebrkListById(uint32_t id)
{
    map<uint32_t, list<rum_session_timebrk_t> >::iterator mapIter;
    mapIter = gpHashListPtr->find(id);
    return mapIter->second;
}

void RumSessionHashlist::GetIdArray (vector<uint32_t>& idVec)
{
    map<uint32_t, list<rum_session_timebrk_t> >::iterator mapIter = gpHashListPtr->begin();
    for (; mapIter != gpHashListPtr->end(); mapIter++)
    {
        idVec.push_back(mapIter->first);
    }
}

void RumSessionHashlist::DecWorkerTaskFinished ()
{
    pthread_mutex_lock ( &workerIdMutex );
    --workerFinishedIdSize;
    pthread_mutex_unlock ( &workerIdMutex );

    if ( workerFinishedIdSize < 0 )
    {
        LOG4CPLUS_ERROR(*gpLogger, "Bad value workerFinishedIdSize: " << workerFinishedIdSize); 
    }
}

void RumSessionHashlist::DecCleanTaskFinished ()
{
    pthread_mutex_lock ( &cleanIdMutex );
    --cleanFinishedIdSize;
    pthread_mutex_unlock ( &cleanIdMutex );

    if ( cleanFinishedIdSize < 0 )
    {
        LOG4CPLUS_ERROR(*gpLogger, "Bad value cleanFinishedIdSize: " << cleanFinishedIdSize);
    }
    if (cleanFinishedIdSize==0)
    {
        ClearSessionHashList();
    }
}

bool RumSessionHashlist::IsWorkerTaskAllFinished ()
{
    bool myRet = false;
    pthread_mutex_lock ( &workerIdMutex );
    if (workerFinishedIdSize==0)
    {
        myRet = true;
    }
    pthread_mutex_unlock ( &workerIdMutex );
    return myRet;
}

bool RumSessionHashlist::IsCleanTaskAllFinished ()
{
    bool myRet = false;
    pthread_mutex_lock ( &cleanIdMutex );
    if (cleanFinishedIdSize==0)
    {
        myRet = true;
    }
    pthread_mutex_unlock ( &cleanIdMutex );
    return myRet;
}

bool RumSessionHashlist::ClearSessionHashList()
{
    if (gpHashListPtr != NULL)
    {   
        map< uint32_t, list<rum_session_timebrk_t> >::iterator mapIter = gpHashListPtr->begin();
        for (; mapIter != gpHashListPtr->end(); mapIter++)
        {
            mapIter->second.clear();
        }
        gpHashListPtr->clear();
        delete gpHashListPtr;
        gpHashListPtr = NULL;
    }
    return true;
}

void RumSessionHashlist::SessionDisplay(string prefix, rum_session_timebrk_t& rSessionTimebrk)
{
    LOG4CPLUS_DEBUG(*gpLogger, endl);
    LOG4CPLUS_DEBUG(*gpLogger, prefix << "--service_id: " << rSessionTimebrk.service_id);
    LOG4CPLUS_DEBUG(*gpLogger, prefix << "--source_ip: " << rSessionTimebrk.source_ip);
    LOG4CPLUS_DEBUG(*gpLogger, prefix << "--end_time: " << ctime(&rSessionTimebrk.end_time));
}

void RumSessionHashlist::DetailsDisplay( string prefix )
{
    map<uint32_t, list<rum_session_timebrk_t> >::iterator mapIter;
    list<rum_session_timebrk_t>::iterator l_iter;
    for( mapIter = gpHashListPtr->begin(); mapIter != gpHashListPtr->end(); mapIter++)
    {
        for(l_iter = mapIter->second.begin(); l_iter != mapIter->second.end(); l_iter++)
        {
            SessionDisplay(prefix, *l_iter);
        }
    }
}
