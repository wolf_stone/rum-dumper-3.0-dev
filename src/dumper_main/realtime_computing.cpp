//#include <signal.h>
#include "threads_api.h"
#include "dumper_common.h"
#include "debug.h"
#include "redisclient.hpp"
#include "SessionHashList.h"
#include "czmq.h"

static Logger logger = Logger::getInstance(LOG4CPLUS_TEXT("RealTimeFileLogger.REALTLOG"));

//static uint16_t isTicketed = 0;         //1 means timer was triggered
static bool hasVisits = false;
static uint8_t firstVisit = 0;

static time_t endSeconds = 0;
static time_t startSeconds = 0;

static RumSessionHashlist realtimeHashList;
static pthread_mutex_t gRealListMutex;

static redisContext* gpRedisCtx = NULL;

static void* timeOutHandler (void* args)
{
    //__sync_lock_test_and_set (&isTicketed,1);
    int myRet = -1;
    gpLogger = &logger;

    LOG4CPLUS_DEBUG(logger, "realtime_computing timer thread_id: " << pthread_self());

    while(1)
    {
        sleep(DumperParamers.dashboardTimerTimeout);
        if( __sync_lock_test_and_set (&firstVisit, firstVisit) == 0)
        {
            LOG4CPLUS_INFO(logger, "No visits start from realtime_computing thread start");
            continue;
        }

        RumSessionHashlist resultHashList;

        pthread_mutex_lock ( &gRealListMutex );
        if (hasVisits == false)                 //no vistis in a certain 2 seconds interval
        {
            LOG4CPLUS_DEBUG(logger, "No vistis in a certain " << DumperParamers.dashboardTimerTimeout << " seconds interval");

            endSeconds += DumperParamers.dashboardTimerTimeout;
            startSeconds = endSeconds - DumperParamers.dashboardDuration;

            LOG4CPLUS_DEBUG(logger, "startSeconds in realtime timer: " << startSeconds);
            LOG4CPLUS_DEBUG(logger, "endSeconds in realtime timer: " << endSeconds);

            realtimeHashList.RemoveOutdatedTimebrk(startSeconds);
            LOG4CPLUS_DEBUG(logger, "realtimeHashList will do Summation");
            realtimeHashList.Summation(resultHashList, startSeconds, endSeconds);
            LOG4CPLUS_DEBUG(logger, "Size of resultHashList(timeout triggered): " << resultHashList.GetSize());

        }
        else
        {
            hasVisits = false;
            LOG4CPLUS_DEBUG(logger, "realtimeHashList will do Summation");
            realtimeHashList.Summation(resultHashList, startSeconds, endSeconds);
            LOG4CPLUS_DEBUG(logger, "Size of resultHashList(visits triggered): " << resultHashList.GetSize());
        }
        pthread_mutex_unlock ( &gRealListMutex );

        //LOG4CPLUS_DEBUG(logger, "Size of resultHashList: " << resultHashList.GetSize());

        myRet = RedisClient::writeRealtimePerformance(gpRedisCtx, resultHashList);
        if (myRet == -2)
        {
            RedisClient::redisReConnect ( &gpRedisCtx, DumperParamers.redisSrvIp, DumperParamers.redisSrvPort );    
        }
        else if (myRet == -1 )
        {
            LOG4CPLUS_ERROR(logger, "Failed to send realtime performance data into Redis Server");
        }
    }
    return NULL;
}

void taskRealtimeComputing(zmq::socket_t* pRealtimeRecvSock)
{
    bool myRet = false;
    static int timebrkCounts = 0;

    gpLogger = &logger;
    printf("address of gpLogger in realtime_computing: %p\n", gpLogger);

    if (DumperParamers.logLevel == "DEBUG")
    {
        logger.setLogLevel(DEBUG_LOG_LEVEL);
    }
    else if (DumperParamers.logLevel == "WARNING")
    {
        logger.setLogLevel(WARN_LOG_LEVEL);
    }
    else if (DumperParamers.logLevel == "ERROR")
    {
        logger.setLogLevel(ERROR_LOG_LEVEL);
    }
    else if (DumperParamers.logLevel == "FATAL")
    {
        logger.setLogLevel(FATAL_LOG_LEVEL);
    }
    else
    {
        logger.setLogLevel(INFO_LOG_LEVEL);
    }
    LOG4CPLUS_INFO(logger, "Start realtime_computing LOG4CPLUS");

    //bool isFirstTicked = false;

    rum_session_timebrk_t* pSessionTimebrk = NULL;
    zmq::message_t recvMsg;// (sizeof(rum_session_timebrk_t));

    gListMutex = PTHREAD_MUTEX_INITIALIZER;

    gpRedisCtx = RedisClient::initRedisContext( DumperParamers.redisSrvIp, DumperParamers.redisSrvPort );    

    LOG4CPLUS_DEBUG(logger, "realtime_computing thread_id: " << pthread_self());

    int threadRet = zthread_new (timeOutHandler, NULL);
    if (threadRet < 0)
    {
        LOG4CPLUS_ERROR(logger, "realtimeComputing create timeout thread error: " << strerror (errno));
        return;
    }

    while(1)
    {
        try{
            myRet = pRealtimeRecvSock->recv(&recvMsg);
            if( myRet == false )
            {
                LOG4CPLUS_ERROR(logger, "Failed to get message from \"" <<  RUM_DUMPER_REALTIME_PAIR_CONNECTION << "\"");
            }
            else            //received session_timebrk
            {
                __sync_lock_test_and_set (&firstVisit, 1);
                pSessionTimebrk = (rum_session_timebrk_t*)recvMsg.data();

                sessionDisplay("realtime_computing", *pSessionTimebrk);

                LOG4CPLUS_DEBUG(logger, "Received SessionTimebrk with end_time: " << pSessionTimebrk->end_time);

                pthread_mutex_lock ( &gRealListMutex );

                hasVisits = true;

                if (pSessionTimebrk->end_time > endSeconds)
                {
                    endSeconds = pSessionTimebrk->end_time;
                    startSeconds = endSeconds - DumperParamers.dashboardDuration;
                    realtimeHashList.RemoveOutdatedTimebrk(startSeconds);
                    LOG4CPLUS_DEBUG(logger, pSessionTimebrk->end_time << " trigger dashboardDuration update");
                }
                else if (pSessionTimebrk->end_time <= startSeconds)
                {
                    LOG4CPLUS_WARN(logger, "Outdated SessionTimebrk in realtime_computing: " << pSessionTimebrk->end_time);
                }

                if (pSessionTimebrk->end_time > startSeconds)
                {
                    realtimeHashList.InsertTimebrkByService(pSessionTimebrk);
                    LOG4CPLUS_DEBUG(logger, pSessionTimebrk->end_time << " was inserted");
                }

                pthread_mutex_unlock ( &gRealListMutex );

                ++timebrkCounts;
                LOG4CPLUS_DEBUG(logger, "startSeconds: " << startSeconds);
                LOG4CPLUS_DEBUG(logger, "endSeconds: " << endSeconds);
                LOG4CPLUS_DEBUG(logger, "timebrkCounts: " << timebrkCounts);

            }
        }
        catch (zmq::error_t err)
        {
            LOG4CPLUS_ERROR(logger, "Failed to receive data in realtimeComputing with reason: " << err.what());
        }
    }
}
