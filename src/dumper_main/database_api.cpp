#include <stdlib.h>
#include <string.h>
#include "database_api.h"
#include "session_computing.h"
#include "debug.h"

bool checkConnection(PGconn * conn, const char * message)
{
    if (PQstatus(conn) != CONNECTION_OK) {
        LOG4CPLUS_ERROR(*gpLogger, " Connecte to database(" << message << ") failed: " << PQerrorMessage(conn));
        return false;
    }

	return true;
}

/* check the result status of a sql-command. */
bool checkResult(PGresult *result, PGconn *conn, const char *message)
{
    if (PQresultStatus(result) != PGRES_COMMAND_OK)
    {
        LOG4CPLUS_ERROR(*gpLogger, "Command(" << message << ") failed: " << PQerrorMessage(conn));
        return false;
    }

    return true;
}

/* check the status of a new declared cursor. */
bool checkCursor(PGresult *result, PGconn *conn, const char *message)
{
    if (PQresultStatus(result) != PGRES_COMMAND_OK)
    {
        LOG4CPLUS_ERROR(*gpLogger, "DECLARE CURSOR(" << message << ") failed: " << PQerrorMessage(conn));
        return false;
    }

	return true;
}

/* check the status after fetch a cursor. */
bool checkFetch(PGresult *result, PGconn *conn, const char *message)
{
    if (PQresultStatus(result) != PGRES_TUPLES_OK)
    {
        LOG4CPLUS_ERROR(*gpLogger, "FETCH ALL(" << message << ") failed: " << PQerrorMessage(conn));
        return false;
    }

	return true;
}

time_t getDayLeftBound( time_t tSeconds )      //result less or equal than tSeconds
{
    if (tSeconds % RUM_ONE_DAY_SECS == 0)
    {
        return (tSeconds - RUM_EIGHT_HOUR_SECS);
    }
    time_t myTmpSeconds = RUM_ONE_DAY_SECS - tSeconds % RUM_ONE_DAY_SECS;
    if( myTmpSeconds < RUM_EIGHT_HOUR_SECS )
    {
        return (tSeconds + myTmpSeconds - RUM_EIGHT_HOUR_SECS);
    }
    else if( myTmpSeconds == RUM_EIGHT_HOUR_SECS )
    {
        return tSeconds;
    }
    else
    {
        return (tSeconds - (tSeconds % RUM_ONE_DAY_SECS) - RUM_EIGHT_HOUR_SECS);
    }
}

bool dbAddSessionTimebrkByOption( const char* conninfo, const char* tableName, rum_session_timebrk_t* pSessionTimebrk )
{
    PGconn      *conn=NULL;
    PGresult    *result=NULL;

    char sql_cmd[DB_COMMAND_LEN] = {0}; 

    conn = PQconnectdb(conninfo);
    if (false == checkConnection(conn, conninfo)) 
    {
        PQfinish(conn);
        return false;
    }

    result = PQexec(conn, "BEGIN");
    if (0 == checkResult(result, conn, "dbAddSessionTimebrkByOption():: BEGIN")) {
        PQclear(result);
        PQfinish(conn);

        return false;
    }
    PQclear(result);

    sprintf(sql_cmd, "INSERT INTO rd_session_time_breakdown_%s (collector_id, service_id, start_time, end_time, service_type, tcp_retry_num, tcp_retry_time, tcp_connection_requests, tcp_success_connections, tcp_connection_time, tcp_server_time, tcp_exchange_size, ssl_requests, ssl_success_connections, ssl_shake_time, http_request_num, http_success_responses, http_network_time, http_server_time, http_download_time, http_download_size) VALUES(%u, %u, %lu, %lu, '%u', %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu)", tableName, pSessionTimebrk->collector_id, pSessionTimebrk->service_id, pSessionTimebrk->start_time, pSessionTimebrk->end_time,pSessionTimebrk->service_type, pSessionTimebrk->tcp_retry_num, pSessionTimebrk->tcp_retry_time, pSessionTimebrk->tcp_connection_requests, pSessionTimebrk->tcp_success_connections, pSessionTimebrk->tcp_connection_time, pSessionTimebrk->tcp_server_time, pSessionTimebrk->tcp_exchange_size, pSessionTimebrk->ssl_requests, pSessionTimebrk->ssl_success_connections, pSessionTimebrk->ssl_shake_time, pSessionTimebrk->http_request_num, pSessionTimebrk->http_success_responses, pSessionTimebrk->http_network_time, pSessionTimebrk->http_server_time, pSessionTimebrk->http_download_time, pSessionTimebrk->http_download_size);
    LOG4CPLUS_DEBUG(*gpLogger, "dbAddSessionTimebrkByOption: sql_cmd= " << sql_cmd);

    result = PQexec(conn, sql_cmd);
    if (0 == checkResult(result, conn, "dbAddSessionTimebrkByOption():: INSERT")) {
        PQclear(result);
        result = PQexec(conn, "END");
        PQclear(result);

        PQfinish(conn);
        return false;
    }

    PQclear(result);
    result = PQexec(conn, "END");
    PQclear(result);
    PQfinish(conn);
    return true;
}

bool dbGetSessionTimebrkByOption (const char* conninfo, const char* tableName, 
                                rum_session_timebrk_t* pSessionTimebrk)
{
    PGconn      *conn=NULL;
    PGresult    *result=NULL;
    char        sql_cmd[DB_COMMAND_LEN] = {0}; 

    enum        record_index {SERVICE_ID=0, START_TIME, END_TIME, SERVICE_TYPE, TCP_RETRY_NUM, TCP_RETRY_TIME,
        TCP_CONNECTION_REQUESTS, TCP_SUCCESS_CONNECTIONS, TCP_CONNECTION_TIME, TCP_SERVER_TIME, 
        TCP_EXCHANGE_SIZE, SSL_REQUESTS, SSL_SUCCESS_CONNECTIONS, SSL_SHAKE_TIME, 
        HTTP_REQUEST_NUM, HTTP_SUCCESS_RESPONSES, HTTP_NETWORK_TIME, 
        HTTP_SERVER_TIME, HTTP_DOWNLOAD_TIME, HTTP_DOWNLOAD_SIZE, COLLECTOR_ID};

    conn = PQconnectdb(conninfo);
    if(PQstatus(conn) != CONNECTION_OK){
        LOG4CPLUS_ERROR(*gpLogger, "dbGetSessionTimebrkByOption()::failed to connect the database with error: " << PQerrorMessage(conn));
        PQfinish(conn);
        return false;
    }
    
    sprintf(sql_cmd,"SELECT * FROM rd_session_time_breakdown_%s where start_time=%lu and end_time=%lu and service_id=%u", tableName, pSessionTimebrk->start_time, pSessionTimebrk->end_time, pSessionTimebrk->service_id);
    result = PQexec(conn, sql_cmd);

    if(PQresultStatus(result) != PGRES_TUPLES_OK){
        LOG4CPLUS_ERROR(*gpLogger, "dbGetSessionTimebrkByOption()::failed to execute the SELECT command with error: " << PQerrorMessage(conn)); 
        PQclear(result);
        PQfinish(conn);
        return false;
    }

    if(PQntuples(result)>0)
    {

        pSessionTimebrk->tcp_retry_num = (uint64_t)(atoll(PQgetvalue(result, 0, TCP_RETRY_NUM)));
        pSessionTimebrk->tcp_retry_time = (uint64_t)(atoll(PQgetvalue(result, 0, TCP_RETRY_TIME)));

        pSessionTimebrk->tcp_connection_requests = (uint64_t)(atoll(PQgetvalue(result, 0, TCP_CONNECTION_REQUESTS)));
        pSessionTimebrk->tcp_success_connections = (uint64_t)(atoll(PQgetvalue(result, 0, TCP_SUCCESS_CONNECTIONS)));
        pSessionTimebrk->tcp_connection_time = (uint64_t)(atoll(PQgetvalue(result, 0, TCP_CONNECTION_TIME)));

        pSessionTimebrk->tcp_server_time = (uint64_t)(atoll(PQgetvalue(result, 0, TCP_SERVER_TIME)));
        pSessionTimebrk->tcp_exchange_size = (uint64_t)(atoll(PQgetvalue(result, 0, TCP_EXCHANGE_SIZE)));

        pSessionTimebrk->ssl_requests = (uint64_t)(atoll(PQgetvalue(result, 0, SSL_REQUESTS)));
        pSessionTimebrk->ssl_success_connections = (uint64_t)(atoll(PQgetvalue(result, 0, SSL_SUCCESS_CONNECTIONS)));
        pSessionTimebrk->ssl_shake_time = (uint64_t)(atoll(PQgetvalue(result, 0, SSL_SHAKE_TIME)));

        //pSessionTimebrk->authentication_requests = (uint64_t)(atoll(PQgetvalue(result, 0, AUTHENTICATION_REQUESTS)));
        //pSessionTimebrk->success_authentications = (uint64_t)(atoll(PQgetvalue(result, 0, SUCCESS_AUTHENTICATIONS)));
        //pSessionTimebrk->authentication_time = (uint64_t)(atoll(PQgetvalue(result, 0, AUTHENTICATION_TIME)));

        pSessionTimebrk->http_request_num = (uint64_t)(atoll(PQgetvalue(result, 0, HTTP_REQUEST_NUM)));
        pSessionTimebrk->http_success_responses = (uint64_t)(atoll(PQgetvalue(result, 0, HTTP_SUCCESS_RESPONSES)));
        pSessionTimebrk->http_network_time = (uint64_t)(atoll(PQgetvalue(result, 0, HTTP_NETWORK_TIME)));
        pSessionTimebrk->http_server_time = (uint64_t)(atoll(PQgetvalue(result, 0, HTTP_SERVER_TIME)));
        pSessionTimebrk->http_download_time = (uint64_t)(atoll(PQgetvalue(result, 0, HTTP_DOWNLOAD_TIME)));

        pSessionTimebrk->http_download_size = (uint64_t)(atoll(PQgetvalue(result, 0, HTTP_DOWNLOAD_SIZE)));
        //pSessionTimebrk->collector_id = (uint32_t)(atoi(PQgetvalue(result, index, COLLECTOR_ID)));

        PQclear(result);
        PQfinish(conn);
        return true;
    }
    else
    {
        PQclear(result);
        PQfinish(conn);
        return false;
    }
}

bool dbSetHourDayHistory(const char* conninfo, HistoryMap& hourDayhistory)
{
    PGconn      *conn=NULL;
    PGresult    *result=NULL;
    char        sql_cmd[DB_COMMAND_LEN] = {0}; 

    conn = PQconnectdb(conninfo);
    if(PQstatus(conn) != CONNECTION_OK){
        LOG4CPLUS_ERROR(*gpLogger, "dbSetHourDayHistory()::failed to connect the database with error: " << PQerrorMessage(conn));
        PQfinish(conn);
        return false;
    }

    result = PQexec(conn, "BEGIN");
    if (false == checkResult(result, conn, "dbSetHourDayHistory():: BEGIN")) {
        PQclear(result);
        PQfinish(conn);

        return false;
    }
    PQclear(result);

    sprintf(sql_cmd, "DECLARE setHourHistory CURSOR FOR SELECT service_id, max(end_time) FROM rd_session_time_breakdown_hour group by service_id");

    result = PQexec(conn, sql_cmd);
    if (false == checkCursor(result, conn, "dbSetHourDayHistory():: setHourHistory")) {
        PQclear(result);
        result = PQexec(conn, "END");
        PQclear(result);

        PQfinish(conn);
        return false;
    }
    PQclear(result);

    result = PQexec(conn, "FETCH ALL IN setHourHistory");
    if (false == checkFetch(result, conn, "dbSetHourDayHistory():: setHourHistory")) {
        PQclear(result);
        result = PQexec(conn, "CLOSE setHourHistory");
        PQclear(result);
        result = PQexec(conn, "END");
        PQclear(result);

        PQfinish(conn);
        return false;
    }

    int count = PQntuples(result);

    for (int index = 0; index < count; index++) 
    {
        HistoryTime tmpTime = {0, 0};
        uint32_t serviceId = (uint32_t)(atoi(PQgetvalue(result, index, 0)));

        tmpTime.lastHour = (time_t)(atoll(PQgetvalue(result, index, 1)));
        hourDayhistory.insert(pair<uint32_t, HistoryTime>(serviceId, tmpTime));
    }

    PQclear(result);
    result = PQexec(conn, "CLOSE setHourHistory");
    PQclear(result);
    result = PQexec(conn, "END");
    PQclear(result);


    result = PQexec(conn, "BEGIN");
    if (false == checkResult(result, conn, "dbSetHourDayHistory():: BEGIN")) {
        PQclear(result);
        PQfinish(conn);

        return false;
    }
    PQclear(result);

    sprintf(sql_cmd, "DECLARE setDayHistory CURSOR FOR SELECT service_id, max(end_time) FROM rd_session_time_breakdown_day group by service_id");

    result = PQexec(conn, sql_cmd);
    if (false == checkCursor(result, conn, "dbSetHourDayHistory():: setDayHistory")) {
        PQclear(result);
        result = PQexec(conn, "END");
        PQclear(result);

        PQfinish(conn);
        return false;
    }
    PQclear(result);

    result = PQexec(conn, "FETCH ALL IN setDayHistory");
    if (false == checkFetch(result, conn, "dbSetHourDayHistory():: setDayHistory")) {
        PQclear(result);
        result = PQexec(conn, "CLOSE setDayHistory");
        PQclear(result);
        result = PQexec(conn, "END");
        PQclear(result);

        PQfinish(conn);
        return false;
    }

    count = PQntuples(result);

    for (int index = 0; index < count; index++) 
    {
        HistoryTime tmpTime = {0, 0};
        uint32_t serviceId = (uint32_t)(atoi(PQgetvalue(result, index, 0)));

        tmpTime.lastDay = (time_t)(atoll(PQgetvalue(result, index, 1)));
        HistoryMap::iterator mapIter = hourDayhistory.find(serviceId);
        if (mapIter != hourDayhistory.end())
        {
            mapIter->second.lastDay = tmpTime.lastDay;
        }
        else
        {
            hourDayhistory.insert(pair<uint32_t, HistoryTime>(serviceId, tmpTime));
        }
    }

    PQclear(result);
    result = PQexec(conn, "CLOSE setDayHistory");
    PQclear(result);
    result = PQexec(conn, "END");
    PQclear(result);
    PQfinish(conn);
    return true;
}
bool dbGetMaxEndHour (const char* conninfo, time_t& dbMaxEndHour)
{
    PGconn      *conn=NULL;
    PGresult    *result=NULL;
    char        sql_cmd[DB_COMMAND_LEN] = {0}; 

    dbMaxEndHour = 0;

    conn = PQconnectdb(conninfo);
    if(PQstatus(conn) != CONNECTION_OK){
        LOG4CPLUS_ERROR(*gpLogger, "dbGetMaxEndHour()::failed to connect the database with error: " << PQerrorMessage(conn));
        PQfinish(conn);
        return false;
    }

    sprintf(sql_cmd,"SELECT max(end_time) FROM rd_session_time_breakdown_hour");
    result = PQexec(conn, sql_cmd);

    if(PQresultStatus(result) != PGRES_TUPLES_OK){
        LOG4CPLUS_ERROR(*gpLogger, "dbGetMaxEndHour()::failed to execute the SELECT command with error: " << PQerrorMessage(conn)); 
        PQclear(result);
        PQfinish(conn);
        return false;
    }

    if(PQntuples(result)>0)
    {
        dbMaxEndHour = atol(PQgetvalue(result, 0, 0));
        PQclear(result);
        PQfinish(conn);
        return true;                                                                      
    }
    else
    {
        PQclear(result);
        PQfinish(conn);
        return false;
    }

}

bool dbGetMaxEndDay (const char* conninfo, time_t& dbMaxEndDay)
{
    PGconn      *conn=NULL;
    PGresult    *result=NULL;
    char        sql_cmd[DB_COMMAND_LEN] = {0}; 

    dbMaxEndDay = 0;

    conn = PQconnectdb(conninfo);
    if(PQstatus(conn) != CONNECTION_OK){
        LOG4CPLUS_ERROR(*gpLogger, "dbGetMaxEndDay()::failed to connect the database with error: " << PQerrorMessage(conn));
        PQfinish(conn);
        return false;
    }

    sprintf(sql_cmd,"SELECT max(end_time) FROM rd_session_time_breakdown_day");
    result = PQexec(conn, sql_cmd);

    if(PQresultStatus(result) != PGRES_TUPLES_OK){
        LOG4CPLUS_ERROR(*gpLogger, "dbGetMaxEndDay()::failed to execute the SELECT command with error: " << PQerrorMessage(conn)); 
        PQclear(result);
        PQfinish(conn);
        return false;
    }

    if(PQntuples(result)>0)
    {
        dbMaxEndDay = atol(PQgetvalue(result, 0, 0));

        PQclear(result);
        PQfinish(conn);
        return true;                                                                      
    }
    else
    {
        PQclear(result);
        PQfinish(conn);
        return false;
    }
}

bool dbAddSessionTimebrkMinute( const char* conninfo, rum_session_timebrk_t* pSessionTimebrk )
{
    bool myRet = false;
    rum_session_timebrk_t mySessionTimebrk;

    memset( &mySessionTimebrk, 0, sizeof(rum_session_timebrk_t));
    mySessionTimebrk.service_id = pSessionTimebrk->service_id;
    mySessionTimebrk.start_time = pSessionTimebrk->start_time;
    mySessionTimebrk.end_time = pSessionTimebrk->end_time;
    mySessionTimebrk.collector_id = pSessionTimebrk->collector_id;

    myRet = dbGetSessionTimebrkByOption(conninfo, "minute", &mySessionTimebrk);
    if (myRet == true)
    {
        sessionDisplay("Origin minute record ", mySessionTimebrk);

        sessionTimebrkSummation(*pSessionTimebrk, mySessionTimebrk);
        myRet = dbAddSessionTimebrkByOption(conninfo, "minute", &mySessionTimebrk);

        sessionDisplay("Origin minute record ", mySessionTimebrk);
    }
    else
    {
        myRet = dbAddSessionTimebrkByOption(conninfo, "minute", pSessionTimebrk);
        LOG4CPLUS_INFO(*gpLogger, "Insert record into session minute db");
    }

    return myRet;
}

bool dbAddSessionTimebrkHour( const char* conninfo, rum_session_timebrk_t* pSessionTimebrk )
{
    bool myRet = false;
    rum_session_timebrk_t mySessionTimebrk;

    memset( &mySessionTimebrk, 0, sizeof(rum_session_timebrk_t));
    mySessionTimebrk.service_id = pSessionTimebrk->service_id;
    mySessionTimebrk.start_time = pSessionTimebrk->start_time;
    mySessionTimebrk.end_time = pSessionTimebrk->end_time;
    mySessionTimebrk.collector_id = pSessionTimebrk->collector_id;

    myRet = dbGetSessionTimebrkByOption(conninfo, "hour", &mySessionTimebrk);
    if (myRet == true)
    {
        sessionDisplay("Origin hour record ", mySessionTimebrk);

        sessionTimebrkSummation(*pSessionTimebrk, mySessionTimebrk);
        myRet = dbAddSessionTimebrkByOption(conninfo, "hour", &mySessionTimebrk);

        sessionDisplay("Updated hour record ", mySessionTimebrk);
    }
    else
    {
        myRet = dbAddSessionTimebrkByOption(conninfo, "hour", pSessionTimebrk);
        LOG4CPLUS_INFO(*gpLogger, "Insert record into session hour db");
    }

    return myRet;
}

bool dbAddSessionTimebrkDay( const char* conninfo, rum_session_timebrk_t* pSessionTimebrk )
{
    bool myRet = false;
    rum_session_timebrk_t mySessionTimebrk;

    memset( &mySessionTimebrk, 0, sizeof(rum_session_timebrk_t));
    mySessionTimebrk.service_id = pSessionTimebrk->service_id;
    mySessionTimebrk.start_time = pSessionTimebrk->start_time;
    mySessionTimebrk.end_time = pSessionTimebrk->end_time;
    mySessionTimebrk.collector_id = pSessionTimebrk->collector_id;

    myRet = dbGetSessionTimebrkByOption(conninfo, "day", &mySessionTimebrk);
    if (myRet == true)
    {
        sessionDisplay("Origin day record ", mySessionTimebrk);

        sessionTimebrkSummation(*pSessionTimebrk, mySessionTimebrk);
        myRet = dbAddSessionTimebrkByOption(conninfo, "day", &mySessionTimebrk);

        sessionDisplay("Updated day record ", mySessionTimebrk);
    }
    else
    {
        myRet = dbAddSessionTimebrkByOption(conninfo, "day", pSessionTimebrk);
        LOG4CPLUS_INFO(*gpLogger, "Insert record into session day db");
    }

    return myRet;
}

bool dbAddSessionTimebrkSingle( const char* conninfo, rum_session_timebrk_t* pSessionTimebrk )
{
    PGconn      *conn=NULL;
    PGresult    *result=NULL;

    char sql_cmd[DB_COMMAND_LEN] = {0}; 

    conn = PQconnectdb(conninfo);
    if (false == checkConnection(conn, conninfo)) 
    {
        PQfinish(conn);
        return false;
    }

    result = PQexec(conn, "BEGIN");
    if (0 == checkResult(result, conn, "dbAddSessionTimebrkSingle():: BEGIN")) {
        PQclear(result);
        PQfinish(conn);

        return false;
    }
    PQclear(result);

    sprintf(sql_cmd, "INSERT INTO rd_session_time_breakdown_single (collector_id, service_id, start_time, end_time, service_type, source_ip, source_port, tcp_retry_num, tcp_retry_time, tcp_connection_requests, tcp_success_connections, tcp_connection_time, tcp_server_time, tcp_exchange_size, ssl_requests, ssl_success_connections, ssl_shake_time, http_request_url, http_refer_url, http_request_num, http_success_responses, http_network_time, http_server_time, http_download_time, http_download_size) VALUES(%u, %u, %lu, %lu, '%u', '%s', '%hu', %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, '%s', '%s', %lu, %lu, %lu, %lu, %lu, %lu)", pSessionTimebrk->collector_id, pSessionTimebrk->service_id, pSessionTimebrk->start_time, pSessionTimebrk->end_time,pSessionTimebrk->service_type, pSessionTimebrk->source_ip, pSessionTimebrk->source_port, pSessionTimebrk->tcp_retry_num, pSessionTimebrk->tcp_retry_time, pSessionTimebrk->tcp_connection_requests, pSessionTimebrk->tcp_success_connections, pSessionTimebrk->tcp_connection_time, pSessionTimebrk->tcp_server_time, pSessionTimebrk->tcp_exchange_size, pSessionTimebrk->ssl_requests, pSessionTimebrk->ssl_success_connections, pSessionTimebrk->ssl_shake_time, pSessionTimebrk->http_request_url, pSessionTimebrk->http_refer_url, pSessionTimebrk->http_request_num, pSessionTimebrk->http_success_responses, pSessionTimebrk->http_network_time, pSessionTimebrk->http_server_time, pSessionTimebrk->http_download_time, pSessionTimebrk->http_download_size);
    LOG4CPLUS_DEBUG(*gpLogger, "dbAddSessionTimebrkSingle sql_cmd: " << sql_cmd);

    result = PQexec(conn, sql_cmd);
    if (0 == checkResult(result, conn, "dbAddSessionTimebrkSingle():: INSERT")) {
        PQclear(result);
        result = PQexec(conn, "END");
        PQclear(result);

        PQfinish(conn);
        return false;
    }

    PQclear(result);
    result = PQexec(conn, "END");
    PQclear(result);
    PQfinish(conn);
    return true;
}

bool dbAddSessionEvent( const char* conninfo, rum_session_event_t* pSessionEvent )
{
    PGconn      *conn=NULL;
    PGresult    *result=NULL;

    char sql_cmd[DB_COMMAND_LEN] = {0}; 

    conn = PQconnectdb(conninfo);
    if (false == checkConnection(conn, conninfo)) 
    {
        PQfinish(conn);
        return false;
    }

    result = PQexec(conn, "BEGIN");
    if (0 == checkResult(result, conn, "dbAddSessionEvent():: BEGIN")) {
        PQclear(result);
        PQfinish(conn);

        return false;
    }
    PQclear(result);

    sprintf(sql_cmd, "INSERT INTO rd_session_event (collector_id, service_id, event_time, event_type, source_ip, source_port, event_name, http_request_url, http_refer_url, response_time, tcp_retry_time, tcp_connection_time, tcp_server_time, http_network_time, http_server_time, http_download_time, performance_critical) VALUES(%u, %u, %lu, '%u', '%s', '%hu', '%s', '%s', '%s', %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu)", pSessionEvent->collector_id, pSessionEvent->service_id, pSessionEvent->event_time, pSessionEvent->event_type, pSessionEvent->source_ip, pSessionEvent->source_port, pSessionEvent->event_name, pSessionEvent->http_request_url, pSessionEvent->http_refer_url, pSessionEvent->response_time, pSessionEvent->tcp_retry_time, pSessionEvent->tcp_connection_time, pSessionEvent->tcp_server_time, pSessionEvent->http_network_time, pSessionEvent->http_server_time, pSessionEvent->http_download_time, pSessionEvent->performance_critical);
    LOG4CPLUS_DEBUG(*gpLogger, "dbAddSessionEvent sql_cmd: " << sql_cmd);

    result = PQexec(conn, sql_cmd);
    if (0 == checkResult(result, conn, "dbAddSessionEvent():: INSERT")) {
        PQclear(result);
        result = PQexec(conn, "END");
        PQclear(result);

        PQfinish(conn);
        return false;
    }

    PQclear(result);
    result = PQexec(conn, "END");
    PQclear(result);
    PQfinish(conn);
    return true;
}

bool getMinuteRecords(const char* conninfo, time_t startTime, 
                    time_t endTime, uint32_t serviceId,
                    vector<rum_session_timebrk_t>& sessonTimebrkVec)
{
    PGconn      *conn=NULL;
    PGresult    *result=NULL;
    char        sql_cmd[DB_COMMAND_LEN] = {0}; 
    int         count=0;
    int         index=0;
    enum        record_index {SERVICE_ID=0, START_TIME, END_TIME, SERVICE_TYPE, TCP_RETRY_NUM, TCP_RETRY_TIME,
        TCP_CONNECTION_REQUESTS, TCP_SUCCESS_CONNECTIONS, TCP_CONNECTION_TIME, TCP_SERVER_TIME, 
        TCP_EXCHANGE_SIZE, SSL_REQUESTS, SSL_SUCCESS_CONNECTIONS, SSL_SHAKE_TIME, 
        HTTP_REQUEST_NUM, HTTP_SUCCESS_RESPONSES, HTTP_NETWORK_TIME, 
        HTTP_SERVER_TIME, HTTP_DOWNLOAD_TIME, HTTP_DOWNLOAD_SIZE, COLLECTOR_ID};

    conn = PQconnectdb(conninfo);
    if (false == checkConnection(conn, "getMinuteRecords():: ")) {
        PQfinish(conn);

        return false;
    }

    result = PQexec(conn, "BEGIN");
    if (false == checkResult(result, conn, "getMinuteRecords():: BEGIN")) {
        PQclear(result);
        PQfinish(conn);

        return false;
    }
    PQclear(result);

    sprintf(sql_cmd, "DECLARE read_minute CURSOR FOR SELECT * FROM rd_session_time_breakdown_minute WHERE start_time>=%lu AND end_time<=%lu AND service_id='%d' ORDER BY start_time ASC", startTime, endTime, serviceId);

    result = PQexec(conn, sql_cmd);
    if (false == checkCursor(result, conn, "getMinuteRecords():: read_minute")) {
        PQclear(result);
        result = PQexec(conn, "END");
        PQclear(result);

        PQfinish(conn);
        return false;
    }
    PQclear(result);

    result = PQexec(conn, "FETCH ALL IN read_minute");
    if (false == checkFetch(result, conn, "getMinuteRecords():: read_minute")) {
        PQclear(result);
        result = PQexec(conn, "CLOSE read_minute");
        PQclear(result);
        result = PQexec(conn, "END");
        PQclear(result);

        PQfinish(conn);
        return false;
    }

    count = PQntuples(result);

    rum_session_timebrk_t mySessionTimebrk;
    memset( &mySessionTimebrk, 0, sizeof(rum_session_timebrk_t));
    for (index = 0; index < count; index ++) {
        mySessionTimebrk.service_id = (uint32_t)(atoi(PQgetvalue(result, index, SERVICE_ID)));

        mySessionTimebrk.start_time = (time_t)(atoll(PQgetvalue(result, index, START_TIME)));
        mySessionTimebrk.end_time = (time_t)(atoll(PQgetvalue(result, index, END_TIME)));

        mySessionTimebrk.tcp_retry_num = (uint64_t)(atoll(PQgetvalue(result, index, TCP_RETRY_NUM)));
        mySessionTimebrk.tcp_retry_time = (uint64_t)(atoll(PQgetvalue(result, index, TCP_RETRY_TIME)));

        mySessionTimebrk.tcp_connection_requests = (uint64_t)(atoll(PQgetvalue(result, index, TCP_CONNECTION_REQUESTS)));
        mySessionTimebrk.tcp_success_connections = (uint64_t)(atoll(PQgetvalue(result, index, TCP_SUCCESS_CONNECTIONS)));
        mySessionTimebrk.tcp_connection_time = (uint64_t)(atoll(PQgetvalue(result, index, TCP_CONNECTION_TIME)));

        mySessionTimebrk.tcp_server_time = (uint64_t)(atoll(PQgetvalue(result, index, TCP_SERVER_TIME)));
        mySessionTimebrk.tcp_exchange_size = (uint64_t)(atoll(PQgetvalue(result, index, TCP_EXCHANGE_SIZE)));

        mySessionTimebrk.ssl_requests = (uint64_t)(atoll(PQgetvalue(result, index, SSL_REQUESTS)));
        mySessionTimebrk.ssl_success_connections = (uint64_t)(atoll(PQgetvalue(result, index, SSL_SUCCESS_CONNECTIONS)));
        mySessionTimebrk.ssl_shake_time = (uint64_t)(atoll(PQgetvalue(result, index, SSL_SHAKE_TIME)));

        //mySessionTimebrk.authentication_requests = (uint64_t)(atoll(PQgetvalue(result, index, AUTHENTICATION_REQUESTS)));
        //mySessionTimebrk.success_authentications = (uint64_t)(atoll(PQgetvalue(result, index, SUCCESS_AUTHENTICATIONS)));
        //mySessionTimebrk.authentication_time = (uint64_t)(atoll(PQgetvalue(result, index, AUTHENTICATION_TIME)));

        mySessionTimebrk.http_request_num = (uint64_t)(atoll(PQgetvalue(result, index, HTTP_REQUEST_NUM)));
        mySessionTimebrk.http_success_responses = (uint64_t)(atoll(PQgetvalue(result, index, HTTP_SUCCESS_RESPONSES)));
        mySessionTimebrk.http_network_time = (uint64_t)(atoll(PQgetvalue(result, index, HTTP_NETWORK_TIME)));
        mySessionTimebrk.http_server_time = (uint64_t)(atoll(PQgetvalue(result, index, HTTP_SERVER_TIME)));
        mySessionTimebrk.http_download_time = (uint64_t)(atoll(PQgetvalue(result, index, HTTP_DOWNLOAD_TIME)));
        mySessionTimebrk.http_download_size = (uint64_t)(atoll(PQgetvalue(result, index, HTTP_DOWNLOAD_SIZE)));

        mySessionTimebrk.collector_id = (uint32_t)(atoi(PQgetvalue(result, index, COLLECTOR_ID)));
        sessonTimebrkVec.push_back( mySessionTimebrk );
    }

    PQclear(result);
    result = PQexec(conn, "CLOSE read_minute");
    PQclear(result);
    result = PQexec(conn, "END");
    PQclear(result);

    PQfinish(conn);
    return count;
}

bool getHourRecords(const char* conninfo, time_t startTime, 
                    time_t endTime, uint32_t serviceId,
                    vector<rum_session_timebrk_t>& sessonTimebrkVec)
{
    PGconn      *conn=NULL;
    PGresult    *result=NULL;
    char        sql_cmd[DB_COMMAND_LEN] = {0}; 
    int         count=0;
    int         index=0;
    enum        record_index {SERVICE_ID=0, START_TIME, END_TIME, SERVICE_TYPE, TCP_RETRY_NUM, TCP_RETRY_TIME,
        TCP_CONNECTION_REQUESTS, TCP_SUCCESS_CONNECTIONS, TCP_CONNECTION_TIME, TCP_SERVER_TIME, 
        TCP_EXCHANGE_SIZE, SSL_REQUESTS, SSL_SUCCESS_CONNECTIONS, SSL_SHAKE_TIME, 
        HTTP_REQUEST_NUM, HTTP_SUCCESS_RESPONSES, HTTP_NETWORK_TIME, 
        HTTP_SERVER_TIME, HTTP_DOWNLOAD_TIME, HTTP_DOWNLOAD_SIZE, COLLECTOR_ID};

    conn = PQconnectdb(conninfo);
    if (false == checkConnection(conn, "getHourRecords():: ")) {
        PQfinish(conn);

        return false;
    }

    result = PQexec(conn, "BEGIN");
    if (false == checkResult(result, conn, "getHourRecords():: BEGIN")) {
        PQclear(result);
        PQfinish(conn);

        return false;
    }
    PQclear(result);

    sprintf(sql_cmd, "DECLARE read_hour CURSOR FOR SELECT * FROM rd_session_time_breakdown_hour WHERE start_time>=%lu AND end_time<=%lu AND service_id='%d' ORDER BY start_time ASC", startTime, endTime, serviceId);

    result = PQexec(conn, sql_cmd);
    if (false == checkCursor(result, conn, "getHourRecords():: read_hour")) {
        PQclear(result);
        result = PQexec(conn, "END");
        PQclear(result);

        PQfinish(conn);
        return false;
    }
    PQclear(result);

    result = PQexec(conn, "FETCH ALL IN read_hour");
    if (false == checkFetch(result, conn, "getHourRecords():: read_hour")) {
        PQclear(result);
        result = PQexec(conn, "CLOSE read_hour");
        PQclear(result);
        result = PQexec(conn, "END");
        PQclear(result);

        PQfinish(conn);
        return false;
    }

    count = PQntuples(result);

    rum_session_timebrk_t mySessionTimebrk;
    memset( &mySessionTimebrk, 0, sizeof(rum_session_timebrk_t));
    for (index = 0; index < count; index ++) {
        mySessionTimebrk.service_id = (uint32_t)(atoi(PQgetvalue(result, index, SERVICE_ID)));

        mySessionTimebrk.start_time = (time_t)(atoll(PQgetvalue(result, index, START_TIME)));
        mySessionTimebrk.end_time = (time_t)(atoll(PQgetvalue(result, index, END_TIME)));

        mySessionTimebrk.tcp_retry_num = (uint64_t)(atoll(PQgetvalue(result, index, TCP_RETRY_NUM)));
        mySessionTimebrk.tcp_retry_time = (uint64_t)(atoll(PQgetvalue(result, index, TCP_RETRY_TIME)));

        mySessionTimebrk.tcp_connection_requests = (uint64_t)(atoll(PQgetvalue(result, index, TCP_CONNECTION_REQUESTS)));
        mySessionTimebrk.tcp_success_connections = (uint64_t)(atoll(PQgetvalue(result, index, TCP_SUCCESS_CONNECTIONS)));
        mySessionTimebrk.tcp_connection_time = (uint64_t)(atoll(PQgetvalue(result, index, TCP_CONNECTION_TIME)));

        mySessionTimebrk.tcp_server_time = (uint64_t)(atoll(PQgetvalue(result, index, TCP_SERVER_TIME)));
        mySessionTimebrk.tcp_exchange_size = (uint64_t)(atoll(PQgetvalue(result, index, TCP_EXCHANGE_SIZE)));

        mySessionTimebrk.ssl_requests = (uint64_t)(atoll(PQgetvalue(result, index, SSL_REQUESTS)));
        mySessionTimebrk.ssl_success_connections = (uint64_t)(atoll(PQgetvalue(result, index, SSL_SUCCESS_CONNECTIONS)));
        mySessionTimebrk.ssl_shake_time = (uint64_t)(atoll(PQgetvalue(result, index, SSL_SHAKE_TIME)));


        mySessionTimebrk.http_request_num = (uint64_t)(atoll(PQgetvalue(result, index, HTTP_REQUEST_NUM)));
        mySessionTimebrk.http_success_responses = (uint64_t)(atoll(PQgetvalue(result, index, HTTP_SUCCESS_RESPONSES)));
        mySessionTimebrk.http_network_time = (uint64_t)(atoll(PQgetvalue(result, index, HTTP_NETWORK_TIME)));
        mySessionTimebrk.http_server_time = (uint64_t)(atoll(PQgetvalue(result, index, HTTP_SERVER_TIME)));
        mySessionTimebrk.http_download_time = (uint64_t)(atoll(PQgetvalue(result, index, HTTP_DOWNLOAD_TIME)));
        mySessionTimebrk.http_download_size = (uint64_t)(atoll(PQgetvalue(result, index, HTTP_DOWNLOAD_SIZE)));

        mySessionTimebrk.collector_id = (uint32_t)(atoi(PQgetvalue(result, index, COLLECTOR_ID)));
        sessonTimebrkVec.push_back( mySessionTimebrk );
    }

    PQclear(result);
    result = PQexec(conn, "CLOSE read_minute");
    PQclear(result);
    result = PQexec(conn, "END");
    PQclear(result);

    PQfinish(conn);
    return count;
}

bool dbAddOutdatedSessionTimebrk(const char* conninfo, rum_session_timebrk_t* pSessionTimebrk)
{
    bool myRet = false;
    time_t startTime =  0;
    time_t endTime = 0;
    rum_session_timebrk_t mySessionTimebrk;

    //Five_Minute 
    startTime = pSessionTimebrk->end_time;
    if (startTime % RUM_FIVE_MINUTES_SECS == 0)
    {
        endTime = startTime;
        startTime = endTime - RUM_FIVE_MINUTES_SECS;
    }
    else
    {
        startTime = startTime - startTime % RUM_FIVE_MINUTES_SECS;
        endTime = startTime + RUM_FIVE_MINUTES_SECS;
    }

    memset( &mySessionTimebrk, 0, sizeof(rum_session_timebrk_t));
    mySessionTimebrk.start_time = startTime;
    mySessionTimebrk.end_time = endTime;
    mySessionTimebrk.service_id = pSessionTimebrk->service_id;
    mySessionTimebrk.collector_id = pSessionTimebrk->collector_id;;

    myRet = dbGetSessionTimebrkByOption(conninfo, "minute",  &mySessionTimebrk);
    if (myRet == true)
    {
        sessionDisplay("Origin minute record ", mySessionTimebrk);

        sessionTimebrkSummation(*pSessionTimebrk, mySessionTimebrk);
        dbAddSessionTimebrkByOption(conninfo, "minute", &mySessionTimebrk);

        sessionDisplay("Updated minute record ", mySessionTimebrk);
    }
    else
    {
        pSessionTimebrk->start_time = startTime;
        pSessionTimebrk->end_time = endTime;

        dbAddSessionTimebrkByOption(conninfo, "minute", pSessionTimebrk);
        LOG4CPLUS_INFO(*gpLogger, "Insert record into session minute db");
    }


    //HOUR 
    startTime = pSessionTimebrk->end_time;
    if (startTime % RUM_ONE_HOUR_SECS == 0)
    {
        endTime = startTime;
        startTime = endTime - RUM_ONE_HOUR_SECS;
    }
    else
    {
        startTime = startTime - startTime % RUM_ONE_HOUR_SECS;
        endTime = startTime + RUM_ONE_HOUR_SECS;
    }
    
    memset( &mySessionTimebrk, 0, sizeof(rum_session_timebrk_t));
    mySessionTimebrk.service_id = pSessionTimebrk->service_id;
    mySessionTimebrk.start_time = startTime;
    mySessionTimebrk.end_time = endTime;
    mySessionTimebrk.collector_id = pSessionTimebrk->collector_id;;

    myRet = dbGetSessionTimebrkByOption(conninfo, "hour", &mySessionTimebrk);
    if (myRet == true)
    {
        sessionDisplay("Origin hour record ", mySessionTimebrk);

        sessionTimebrkSummation(*pSessionTimebrk, mySessionTimebrk);
        dbAddSessionTimebrkByOption(conninfo, "hour", &mySessionTimebrk);

        sessionDisplay("Updated hour record ", mySessionTimebrk);
    }
    else
    {
        pSessionTimebrk->start_time = startTime;
        pSessionTimebrk->end_time = endTime;
        dbAddSessionTimebrkByOption(conninfo, "hour", pSessionTimebrk);
        LOG4CPLUS_INFO(*gpLogger, "Insert record into session hour db");
    }

    //Day 
    startTime = getDayLeftBound(pSessionTimebrk->end_time);
    if (startTime == pSessionTimebrk->end_time)
    {
        endTime = startTime;
        startTime = endTime - RUM_ONE_DAY_SECS;
    }
    else
    {
        endTime = startTime + RUM_ONE_DAY_SECS;
    }

    memset( &mySessionTimebrk, 0, sizeof(rum_session_timebrk_t));
    mySessionTimebrk.service_id = pSessionTimebrk->service_id;
    mySessionTimebrk.start_time = startTime;
    mySessionTimebrk.end_time = endTime;
    mySessionTimebrk.collector_id = pSessionTimebrk->collector_id;;
    
    myRet = dbGetSessionTimebrkByOption(conninfo, "day", &mySessionTimebrk);
    if (myRet == true)
    {
        sessionDisplay("Origin day record ", mySessionTimebrk);

        sessionTimebrkSummation(*pSessionTimebrk, mySessionTimebrk);
        dbAddSessionTimebrkByOption(conninfo, "day", &mySessionTimebrk);

        sessionDisplay("Updated day record ", mySessionTimebrk);
    }
    else
    {
        pSessionTimebrk->start_time = startTime;
        pSessionTimebrk->end_time = endTime;
        dbAddSessionTimebrkByOption(conninfo, "day", pSessionTimebrk);
        LOG4CPLUS_INFO(*gpLogger, "Insert record into session day db");
    }

    return true;                                                                      
}
