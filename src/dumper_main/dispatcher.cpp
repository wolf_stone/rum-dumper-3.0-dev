#include <stdio.h>
#include <signal.h>
#include <time.h> 
#include <sys/time.h>
#include <list>

#include "dumper_common.h"
#include "threads_api.h"
#include "debug.h"

static uint16_t isTicketed = 0;         //1 means timer was triggered
static timer_t timerid;


void timeOutHandler (sigval_t v)
{
    __sync_lock_test_and_set (&isTicketed,1);
    //LOG4CPLUS_DEBUG(*gpLogger, "Timeout isTicketed: " << isTicketed);
}


void initTimer(time_t tickInterval, bool &isFirstTicked)
{
    struct sigevent sev;
    struct itimerspec its;
    int myRet = -1;

    if( isFirstTicked == false )                        //The first time to set timer
    {
        //sev.sigev_notify = SIGEV_SIGNAL;
        //sev.sigev_signo = SIGRTMIN;
        
        sev.sigev_value.sival_ptr = &timerid;

        sev.sigev_notify = SIGEV_THREAD;
        sev.sigev_notify_function = timeOutHandler;

        myRet = timer_create(CLOCK_REALTIME, &sev, &timerid);
        if ( myRet == -1)
        {
            //LOG4CPLUS_ERROR(*gpLogger, "timer_create failed");
        }
        else
        {
            //LOG4CPLUS_INFO(*gpLogger, "timer_create successed");
        }
        isFirstTicked = true;

        its.it_value.tv_sec = tickInterval;
        its.it_value.tv_nsec = 0;
        its.it_interval.tv_sec = its.it_value.tv_sec;
        its.it_interval.tv_nsec = its.it_value.tv_nsec;
    }

    LOG4CPLUS_DEBUG(*gpLogger, "timer ID is 0x%lx" << (long) timerid);

    /* Start the timer */
    its.it_value.tv_sec = tickInterval;
    its.it_value.tv_nsec = 0;
    its.it_interval.tv_sec = its.it_value.tv_sec;
    its.it_interval.tv_nsec = its.it_value.tv_nsec;

    if (timer_settime(timerid, 0, &its, NULL) == -1)
    {
        //LOG4CPLUS_ERROR(*gpLogger, "timer_settime failed");
    }
    LOG4CPLUS_DEBUG(*gpLogger, "Set timer interval to " << tickInterval << "(s)");
}

bool updateHashListRouter()
{
    static uint32_t taskCounts = 0;
    if (gpHashListProducer != NULL )
    {
        pthread_mutex_lock ( &gListMutex );
        gListTasks.push_back(gpHashListProducer);
        pthread_mutex_unlock ( &gListMutex);
        gpHashListProducer = NULL;
        ++taskCounts;
        LOG4CPLUS_INFO(*gpLogger, "Produced taskCounts: " << taskCounts);
        return true;
    }

    return false;
}

void sessionMsgProcess(uint16_t msgType, char* data, zmq::socket_t& rMsgSendSock, uint32_t timeout)
{
    static struct five_minute_interval_t sFiveInterval = {0,0};
    static bool isFirstTicked = false;
    static bool isFirstTriggered = false; 
    static time_t tickSeconds = 0;
    time_t nowSecs = 0;
    time_t fiveSecsBegin;
    time_t fiveSecsEnd;
    static time_t timerInterval;
    int myRet = -1;
    dumperMsgInfoPtr pMsgInfo = NULL;

    if( msgType == RUM_DUMPER_MSG_SESSION_TIMEBRK )
    {
        if( gpHashListProducer == NULL )   //alloc when insert
        {
            gpHashListProducer = new(std::nothrow) RumSessionHashlist();
            if( gpHashListProducer == NULL )
            {
                LOG4CPLUS_ERROR(*gpLogger, "Failed to alloc memory for gpHashListProducer");
                return;
            }
            LOG4CPLUS_INFO(*gpLogger, "Success to realloc memory for gpHashListProducer");
        }

        zmq::message_t sendMsg(RUM_DUMPER_MSG_LEN);     //will be destructed after send

        rum_session_timebrk_t* pSessionTimeBrk = (rum_session_timebrk_t*)data;
        nowSecs = pSessionTimeBrk->end_time;

        sessionDisplay("dispatcher", *pSessionTimeBrk );

        tickSeconds = nowSecs;
        if( nowSecs % RUM_FIVE_MINUTES_SECS == 0)
        {
            timerInterval = timeout;

            fiveSecsEnd = nowSecs; 
            fiveSecsBegin =  nowSecs - RUM_FIVE_MINUTES_SECS; 
        }
        else
        {
            timerInterval = RUM_FIVE_MINUTES_SECS - (nowSecs % RUM_FIVE_MINUTES_SECS) + timeout;

            fiveSecsBegin =  nowSecs - nowSecs % RUM_FIVE_MINUTES_SECS; 
            fiveSecsEnd = fiveSecsBegin + RUM_FIVE_MINUTES_SECS;
        }

        initTimer(timerInterval, isFirstTicked);                       //reset timer when new session_timebrk coming

        if( isFirstTriggered == false )  //It is the first session_timebrk received by rum-dumper
        {
            LOG4CPLUS_INFO(*gpLogger, "It is the First session_timebrk");
            isFirstTriggered = true;
            sFiveInterval.secs_begin = fiveSecsBegin;
            sFiveInterval.secs_end = fiveSecsEnd;
            LOG4CPLUS_INFO(*gpLogger, "Update the five interval from " << sFiveInterval.secs_begin << " to " << sFiveInterval.secs_end);

        }
        if( nowSecs <= sFiveInterval.secs_begin )
        {
            LOG4CPLUS_DEBUG(*gpLogger, "nowSecs( " << nowSecs << " ) less or equal than secs_begin( " << sFiveInterval.secs_begin << " )");

            pMsgInfo = (dumperMsgInfoPtr)sendMsg.data();
            pMsgInfo->msgType = RUM_DUMPER_MSG_SESSION_TIMEBRK;
            memcpy(pMsgInfo->msgBody, pSessionTimeBrk, RUM_DUMPER_MSG_DATA_LEN);

            pSessionTimeBrk->flag = FLAG_OUTDATED_TIMEBRK;

            myRet = rMsgSendSock.send(sendMsg);
            if( myRet == true )
            {
                LOG4CPLUS_INFO(*gpLogger, "Send outdated Message to Scheduler");
            }
            else
            {
                LOG4CPLUS_ERROR(*gpLogger, "Failed to Send outdated Message to Scheduler");
            }
        }
        else if( nowSecs < sFiveInterval.secs_end )
        {
            LOG4CPLUS_DEBUG(*gpLogger, "nowSecs( " << nowSecs << " ) less than secs_end( " << sFiveInterval.secs_end << " )");
            gpHashListProducer->InsertTimebrkByService(pSessionTimeBrk);
        }
        else if( nowSecs == sFiveInterval.secs_end )
        {
            LOG4CPLUS_DEBUG(*gpLogger, "nowSecs( " << nowSecs << " ) equal than secs_end( " << sFiveInterval.secs_end << " )");
            gpHashListProducer->InsertTimebrkByService(pSessionTimeBrk);
        }
        else if( nowSecs > sFiveInterval.secs_end )
        {
            LOG4CPLUS_DEBUG(*gpLogger, "nowSecs( " << nowSecs << " ) great than secs_end( " << sFiveInterval.secs_end << " )");
            myRet = updateHashListRouter();
            if( myRet == true )
            {

                dumperMsgInfoPtr pMsgInfo = (dumperMsgInfoPtr)sendMsg.data();
                pMsgInfo->msgType = RUM_DUMPER_MSG_TASK_READY;
                myRet = rMsgSendSock.send(sendMsg);
                if( myRet == true )
                {
                    LOG4CPLUS_INFO(*gpLogger, "Send task ready Message to Scheduler");
                }
                else
                {
                    LOG4CPLUS_ERROR(*gpLogger, "Failed to Send task ready Message to Scheduler");
                }
            }

            if( gpHashListProducer == NULL )   //alloc when insert
            {
                gpHashListProducer = new(std::nothrow) RumSessionHashlist();
                if( gpHashListProducer == NULL )
                {
                    LOG4CPLUS_ERROR(*gpLogger, "Failed to alloc memory for gpHashListProducer");
                    return;
                }
                LOG4CPLUS_INFO(*gpLogger, "Success to realloc memory for gpHashListProducer");
            }
            gpHashListProducer->InsertTimebrkByService(pSessionTimeBrk);

            sFiveInterval.secs_begin = fiveSecsBegin;
            sFiveInterval.secs_end = fiveSecsEnd;
            LOG4CPLUS_INFO(*gpLogger, "Update the five interval from " << sFiveInterval.secs_begin << " to " << sFiveInterval.secs_end);
        }

    }
    /*else if( msgType == RUM_DUMPER_MSG_SESSION_EVENT )
    {
        zmq::message_t sendMsg(RUM_DUMPER_MSG_LEN);
        LOG4CPLUS_DEBUG(*gpLogger, "Process SessionEvent");
        rum_session_event_t* pSessionEvent = (rum_session_event_t*)data;

        dumperMsgInfoPtr pMsgInfo = (dumperMsgInfoPtr)sendMsg.data();
        pMsgInfo->msgType = RUM_DUMPER_MSG_SESSION_EVENT;
        memcpy(pMsgInfo->msgBody, pSessionEvent, RUM_DUMPER_MSG_DATA_LEN);

        myRet = rMsgSendSock.send(sendMsg);
        if( myRet == true )
        {
            LOG4CPLUS_DEBUG(*gpLogger, "Send event Message to Scheduler");
        }
        else
        {
            LOG4CPLUS_ERROR(*gpLogger, "Failed to Send event Message to Scheduler");
        }
        LOG4CPLUS_DEBUG(*gpLogger, "event_time in session_event(Dispatcher): " << pSessionEvent->event_time);
    }*/

    if( __sync_lock_test_and_set (&isTicketed, 0) == 1)
    {

        LOG4CPLUS_DEBUG(*gpLogger, "tickSeconds: " << tickSeconds);
        tickSeconds += timerInterval; 
        LOG4CPLUS_DEBUG(*gpLogger, "tickSeconds + interval: " << tickSeconds);
        /*if( tickSeconds > sFiveInterval.secs_begin && tickSeconds < sFiveInterval.secs_end )
        {
            //Do Nothing
            LOG(INFO) << "tickSeconds( " << tickSeconds << " ) less than secs_end( " << sFiveInterval.secs_end << " )";
        }*/
        if( tickSeconds > sFiveInterval.secs_end )
        {
            fiveSecsBegin =  tickSeconds - tickSeconds % RUM_FIVE_MINUTES_SECS; 
            fiveSecsEnd = fiveSecsBegin + RUM_FIVE_MINUTES_SECS;

            LOG4CPLUS_DEBUG(*gpLogger, "tickSeconds( " << tickSeconds << " ) great than secs_end( " << sFiveInterval.secs_end << " )");
            if( gpHashListProducer != NULL )
            {
                myRet = updateHashListRouter();
                if( myRet == true )
                {
                    zmq::message_t sendMsg(RUM_DUMPER_MSG_LEN);

                    dumperMsgInfoPtr pMsgInfo = (dumperMsgInfoPtr)sendMsg.data();
                    pMsgInfo->msgType = RUM_DUMPER_MSG_TASK_READY;

                    myRet = rMsgSendSock.send(sendMsg);
                    if( myRet == true )
                    {
                        LOG4CPLUS_INFO(*gpLogger, "Send task ready Message to Scheduler triggered by timeout");
                    }
                    else
                    {
                        LOG4CPLUS_ERROR(*gpLogger, "Failed to Send task ready Message to Scheduler");
                    }
                }
            }
            else
            {
                rum_session_timebrk_t* pSessionTimeBrk; 

                zmq::message_t sendMsg(RUM_DUMPER_MSG_LEN);
                pMsgInfo = (dumperMsgInfoPtr)sendMsg.data();
                pMsgInfo->msgType = RUM_DUMPER_MSG_SESSION_TIMEBRK;
                pSessionTimeBrk = (rum_session_timebrk_t*)pMsgInfo->msgBody;

                memset(pSessionTimeBrk, 0, sizeof(rum_session_timebrk_t));
                pSessionTimeBrk->start_time = fiveSecsBegin;
                pSessionTimeBrk->end_time = fiveSecsEnd;
                pSessionTimeBrk->flag = FLAG_TIMER_NOTIFICATION;

                myRet = rMsgSendSock.send(sendMsg);
                if( myRet == true )
                {
                    LOG4CPLUS_INFO(*gpLogger, "Send timer outdated Message to Scheduler");
                }
                else
                {
                    LOG4CPLUS_ERROR(*gpLogger, "Failed to Send timer outdated Message to Scheduler");
                }
            }

            sFiveInterval.secs_begin = fiveSecsBegin;
            sFiveInterval.secs_end = fiveSecsEnd;
            LOG4CPLUS_INFO(*gpLogger, "Update the five interval from " << sFiveInterval.secs_begin << " to " << sFiveInterval.secs_end);
            timerInterval = RUM_FIVE_MINUTES_SECS;
            initTimer(timerInterval, isFirstTicked);

        }
        else
        {
            LOG4CPLUS_ERROR(*gpLogger, "Impossible: tickSeconds = " << tickSeconds << "(secs_begin=" << sFiveInterval.secs_begin << ", secs_end=" << sFiveInterval.secs_end << ")"); 
        }
        LOG4CPLUS_DEBUG(*gpLogger, "Reset isTicketed: " << isTicketed);
    }
}
