#ifndef __DATABASE_API_H__
#define __DATABASE_API_H__
#include "libpq-fe.h"
#include "common.h"
#include <vector>

#define DB_CONFIG_LEN       1024
#define DB_COMMAND_LEN      1024

using namespace std;

struct _historyTime{
    time_t lastHour;
    time_t lastDay;
};

typedef struct _historyTime HistoryTime;
typedef HistoryTime* HistoryTimePtr;
typedef map<uint32_t, HistoryTime> HistoryMap;

bool checkConnection(PGconn * conn, const char * message);

bool checkResult(PGresult *result, PGconn *conn, const char *message);

bool checkCursor(PGresult *result, PGconn *conn, const char *message);

bool checkFetch(PGresult *result, PGconn *conn, const char *message);

time_t getDayLeftBound( time_t tSeconds );

bool dbSetHourDayHistory(const char* conninfo, map<uint32_t, HistoryTime>& hourDayhistory);

bool dbGetMaxEndHour (const char* conninfo, time_t& dbMaxEndHour);

bool dbGetMaxEndDay (const char* conninfo, time_t& dbMaxEndDay);

bool dbAddSessionTimebrkMinute (const char* conninfo, rum_session_timebrk_t* pSessionTimebrk);

bool dbAddSessionTimebrkHour (const char* conninfo, rum_session_timebrk_t* pSessionTimebrk);

bool dbAddSessionTimebrkDay (const char* conninfo, rum_session_timebrk_t* pSessionTimebrk);

bool dbAddSessionEvent (const char* conninfo, rum_session_event_t* pSessionEvent);

bool dbAddSessionTimebrkSingle (const char* conninfo, rum_session_timebrk_t* pSessionTimebrk);

bool getMinuteRecords(const char* conninfo, time_t start, 
                    time_t end, uint32_t serviceId, 
                    vector<rum_session_timebrk_t>& sessonTimebrkVec);

bool getHourRecords(const char* conninfo, time_t startTime, 
                    time_t endTime, uint32_t serviceId,
                    vector<rum_session_timebrk_t>& sessonTimebrkVec);

bool dbAddOutdatedSessionTimebrk(const char* conninfo, rum_session_timebrk_t* pSessionTimebrk);
#endif
