#ifndef __DISPATCHER_H__
#define __DISPATCHER_H__
#include "common.h"
#include "zmq.hpp"
#include "debug.h"

void taskRealtimeComputing(zmq::socket_t* pRealtimeRecvSock);
void sessionMsgProcess(uint16_t msgType, char* data, zmq::socket_t& rMsgSendSock, uint32_t timeout = 30);
void taskSchedule(zmq::socket_t* pPairRecvSock, zmq::context_t* pZmqContext);
void taskWork(zmq::context_t* pZmqContext);
void taskSinkEventHandle(zmq::context_t* pZmqContext);
void taskSinkClean(zmq::context_t* pZmqContext);
void taskSinkAggregate(zmq::context_t* pZmqContext);


#endif
