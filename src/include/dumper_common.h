#ifndef __DUMPER_COMMON_H__
#define __DUMPER_COMMON_H__

#include "common.h"
#include "SessionHashList.h"

typedef struct _dumperParams DumperParams;
typedef DumperParams* DumperParamsPtr;
typedef struct _dumperMsgInfo dumperMsgInfo;
typedef dumperMsgInfo* dumperMsgInfoPtr;

extern RumSessionHashlist* gpHashListProducer;      //produced by Dispatcher
//extern RumSessionHashlist* gpHashListRouter;        //share between Dispatcher and Scheduler
extern RumSessionHashlist* gpHashListConsumer;      //Used by Scheduler and Workers
extern pthread_mutex_t gListMutex;

extern list<RumSessionHashlist*> gListTasks;       //Used by Scheduler and Workers
//extern RumSessionHashlist* gpHashListScavenger;     //Used by SinkClean

extern DumperParams DumperParamers;

struct five_minute_interval_t
{
    time_t secs_begin;
    time_t secs_end;
    time_t end_time;
};

/*
 * Structure used to describes global parameters of rum dumper,
 * these fields will be set with default value or program parameters.
 */
struct _dumperParams {
    std::string dbSrvIp;                        /**< database server ip */
    uint16_t dbSrvPort;                         /**< database server port */
    std::string dbSrvUser; 
    std::string dbSrvPasswd;
    std::string dbName;                         /**< database name will be used*/

    std::string redisSrvIp;                     /**< redis server ip */
    uint16_t redisSrvPort;                      /**< redis server port */
    std::string redisSrvUser; 
    std::string redisSrvPasswd;

    uint16_t workerCopies;                      /**< number of workers */

    uint32_t timebrkReadTimeout;                /**< timeout used to read from redis session_timebrk_list */

    uint32_t produceTimerTimeout;               /**< timer timeout used in dispatcher */
    uint32_t dashboardTimerTimeout;             /**< dashboard flush interval */
    uint32_t dashboardDuration;                 /**< dashboard duration */

    std::string logLevel;
    std::string logPath;
};

struct _dumperMsgInfo {
    uint16_t msgType;
    char msgBody[RUM_DUMPER_MSG_DATA_LEN];
};


typedef struct{
    void* sock;
    void* context;
}schedulerCtx;

typedef struct{
    uint64_t performanceWarning;
    uint64_t performanceCritical;
}thresholdsInfo;
#endif
