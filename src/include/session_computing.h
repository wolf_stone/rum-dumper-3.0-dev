#ifndef __SESSION_COMPUTING_H__
#define __SESSION_COMPUTING_H__
#include "common.h"

void sessionTimebrkSummation(const rum_session_timebrk_t& timebrkItem, rum_session_timebrk_t& timebrkResult);

#endif
