#ifndef __DEBUG_H__
#define __DEBUG_H__
#include <string>
#include "common.h"
#include <log4cplus/logger.h>
#include <log4cplus/configurator.h>
#include <log4cplus/loggingmacros.h>

using namespace log4cplus;

extern __thread Logger* gpLogger;
//__thread Logger* gpLogger;

void sessionDisplay(const std::string prefix, const rum_session_timebrk_t& rSessionTimebrk);

#endif
