#ifndef __SESSION_HASH_HPP__
#define __SESSION_HASH_HPP__

#include <list>
#include <map>
#include <string>
#include <iostream>
#include <vector>
#include "common.h"
#include <pthread.h>

using namespace std;

class RumSessionHashlist
{
public:
    RumSessionHashlist();

    ~RumSessionHashlist();

    bool InsertTimebrkByService(rum_session_timebrk_t* t_timebrk);

    bool InsertTimeBrkByCollector(rum_session_timebrk_t* t_timebrk);

    void GetIdArray (vector<uint32_t>& idVec);

    list<rum_session_timebrk_t>& GetTimebrkListById(uint32_t id);

    SessionHashlistPtr GetSessionHashlistPtr();

    void Summation(RumSessionHashlist& resultHashList, time_t startTime, time_t endTime);
    size_t GetSize();

    void DecWorkerTaskFinished ();
    void DecCleanTaskFinished ();

    void RemoveOutdatedTimebrk(time_t startSeconds);

    bool IsWorkerTaskAllFinished ();
    bool IsCleanTaskAllFinished ();

    bool ClearSessionHashList();

    void SessionDisplay(string prefix, rum_session_timebrk_t& rSessionTimebrk);
    void DetailsDisplay(string prefix);

private:
    SessionHashlistPtr gpHashListPtr;
    int workerFinishedIdSize;               //worker finished the computing task for a certain service_id
    int cleanFinishedIdSize;                //sinkClean finished the clean task for a certain service_id
    pthread_mutex_t workerIdMutex;
    pthread_mutex_t cleanIdMutex;
};


#endif
