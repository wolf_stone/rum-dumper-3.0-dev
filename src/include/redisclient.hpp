#ifndef __REDIS_CLIENT_HPP__
#define __REDIS_CLIENT_HPP__

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>

#include "hiredis.h"
#include "async.h"
#include "json/json.h"

#include "dumper_common.h"
#include "debug.h"

class RedisClient {
	private:

        //static std::string sRedisSrvIp;
        //static uint16_t sRedisSrvPort;

		/*
		 * static redisContext* context()
		 *
		 * return a unique redisContext object
		 *
		 */

        static redisContext* myRedisConnect( const std::string& redisSrvIp, uint16_t redisSrvPort)
        {
            redisContext *lpRedisCtx = NULL;
            struct timeval myTimeout = { 2, 0 }; 
            while(1)
            {
                lpRedisCtx = redisConnectWithTimeout(redisSrvIp.c_str(), redisSrvPort, myTimeout);
                if ((lpRedisCtx)->err) {
                    LOG4CPLUS_ERROR(*gpLogger, "Connection error: " << lpRedisCtx->errstr);
                    redisFree(lpRedisCtx);

                    sleep(1);
                    continue;
                }
                else
                {
                    printf("address of gpLogger in redis: %p\n", gpLogger);
                    LOG4CPLUS_INFO(*gpLogger, "redis server connected");
                    break;
                }
            }
            return lpRedisCtx;
        }

        static redisAsyncContext* myRedisAsyncConnect( const std::string& redisSrvIp, uint16_t redisSrvPort)
        {
            redisAsyncContext* lpRedisCtx = NULL;
            while(1)
            {
                lpRedisCtx = redisAsyncConnect(redisSrvIp.c_str(), redisSrvPort);
                if (lpRedisCtx->err) {
                    LOG4CPLUS_ERROR(*gpLogger, "AsyncConnection error: " << lpRedisCtx->errstr);
                    redisAsyncFree(lpRedisCtx);
                    lpRedisCtx = NULL;


                    sleep(2);
                    continue;
                }
                else
                {
                    LOG4CPLUS_INFO(*gpLogger, "redis server async connected");
                    break;
                }
            }

            return lpRedisCtx;
        }

        static void messageCounter( redisContext* pRedisCtx )
        {
            redisReply* reply = NULL;
            reply = (redisReply*)redisCommand(pRedisCtx, "INCR rum:session_recv_counter");
            if( reply->type == REDIS_REPLY_INTEGER )
            {
                LOG4CPLUS_DEBUG(*gpLogger, "session_recv_counter: " << reply->integer);
            }
            else if( reply->type == REDIS_REPLY_STRING )
            {
                LOG4CPLUS_DEBUG(*gpLogger, "session_recv_counter: " << reply->str);
            }
            if( reply != NULL )
            {
                freeReplyObject(reply);
                reply = NULL;
            }
        }

        static int json2SessionTimebrk( const char* pJsonData, rum_session_timebrk_t& rSessionTimebrk )
        {
            Json::Reader myJsonReader;
            Json::Value myJsonValue;
            if (myJsonReader.parse(pJsonData, myJsonValue))
            {
                rSessionTimebrk.collector_id = myJsonValue["collector_id"].asUInt();
                rSessionTimebrk.service_id = myJsonValue["service_id"].asUInt();
                rSessionTimebrk.service_type = myJsonValue["service_type"].asUInt();

                rSessionTimebrk.start_time = myJsonValue["start_time"].asUInt64();
                rSessionTimebrk.end_time = myJsonValue["end_time"].asUInt64();

                rSessionTimebrk.tcp_retry_num = myJsonValue["retry_num"].asUInt64();
                rSessionTimebrk.tcp_retry_time = myJsonValue["retry_time"].asUInt64();
                rSessionTimebrk.tcp_connection_requests = myJsonValue["connection_requests"].asUInt64();
                rSessionTimebrk.tcp_success_connections = myJsonValue["success_connections"].asUInt64();
                rSessionTimebrk.tcp_connection_time = myJsonValue["connection_time"].asUInt64();
                rSessionTimebrk.tcp_server_time = myJsonValue["server_time"].asUInt64();
                rSessionTimebrk.tcp_exchange_size = myJsonValue["exchange_size"].asUInt64();

                rSessionTimebrk.ssl_requests = myJsonValue["ssl_requests"].asUInt64();
                rSessionTimebrk.ssl_success_connections = myJsonValue["ssl_success_connections"].asUInt64();
                rSessionTimebrk.ssl_shake_time = myJsonValue["ssl_shake_time"].asUInt64();

                /*rSessionTimebrk.authentication_requests = myJsonValue["authentication_requests"].asUInt64();
                rSessionTimebrk.success_authentications = myJsonValue["success_authentications"].asUInt64();
                rSessionTimebrk.authentication_time = myJsonValue["authentication_time"].asUInt64();
                */

                rSessionTimebrk.http_request_num = myJsonValue["request_num"].asUInt64();
                rSessionTimebrk.http_success_responses = myJsonValue["success_responses"].asUInt64();
                rSessionTimebrk.http_network_time = myJsonValue["network_time2first_buffer"].asUInt64();
                rSessionTimebrk.http_server_time = myJsonValue["server_time2first_buffer"].asUInt64();
                rSessionTimebrk.http_download_time = myJsonValue["download_time"].asUInt64();
                rSessionTimebrk.http_download_size = myJsonValue["download_size"].asUInt64();

                rSessionTimebrk.source_port = myJsonValue["source_port"].asUInt();

                snprintf(rSessionTimebrk.source_ip, RUM_IP_NAME_LEN, "%s", myJsonValue["source_ip"].asCString());
                //snprintf(rSessionTimebrk.source_port, RUM_TYPE_PORT_LEN, "%s", myJsonValue["source_port"].asCString());
                snprintf(rSessionTimebrk.http_refer_url, RUM_DATA_LEN, "%s", myJsonValue["refer_url"].asCString());
                snprintf(rSessionTimebrk.http_request_url, RUM_DATA_LEN, "%s", myJsonValue["service_url"].asCString());

                //sessionDisplay("JSON ", rSessionTimebrk);

                return 0;
            }
            else
            {
                return -1;
            }
        }

        static int json2SessionEvent( const char* pJsonData, rum_session_event_t& rSessionEvent )
        {
            Json::Reader myJsonReader;
            Json::Value myJsonValue;
            if (myJsonReader.parse(pJsonData, myJsonValue))
            {
                rSessionEvent.collector_id = myJsonValue["collector_id"].asUInt();
                rSessionEvent.service_id = myJsonValue["service_id"].asUInt();
                rSessionEvent.event_time = myJsonValue["event_time"].asUInt64();
                rSessionEvent.event_type = myJsonValue["event_type"].asUInt();
                snprintf(rSessionEvent.event_name, RUM_IP_NAME_LEN, "%s", myJsonValue["event_name"].asCString());
                snprintf(rSessionEvent.source_ip, RUM_IP_NAME_LEN, "%s", myJsonValue["source_ip"].asCString());
                //snprintf(rSessionEvent.source_port, RUM_TYPE_PORT_LEN, "%s", myJsonValue["source_port"].asCString());
                snprintf(rSessionEvent.http_refer_url, RUM_DATA_LEN, "%s", myJsonValue["refer_url"].asCString());
                snprintf(rSessionEvent.http_request_url, RUM_DATA_LEN, "%s", myJsonValue["service_url"].asCString());
                return 0;
            }
            else
            {
                return -1;
            }
        }

        static void sessionTimebrk2Json ( Json::Value &jsonValue, const rum_session_timebrk_t& rSessionTimebrk )
        {

            jsonValue["collector_id"] = rSessionTimebrk.collector_id;
            jsonValue["service_id"] = rSessionTimebrk.service_id;
            jsonValue["start_time"] = (Json::UInt64)rSessionTimebrk.start_time;
            jsonValue["end_time"] = (Json::UInt64)rSessionTimebrk.end_time;
            jsonValue["tcp_retry_num"] = (Json::UInt64)rSessionTimebrk.tcp_retry_num;
            jsonValue["tcp_retry_time"] = (Json::UInt64)rSessionTimebrk.tcp_retry_time;
            jsonValue["tcp_connection_requests"] = (Json::UInt64)rSessionTimebrk.tcp_connection_requests;
            jsonValue["tcp_success_connections"] = (Json::UInt64)rSessionTimebrk.tcp_success_connections;
            jsonValue["tcp_connection_time"] = (Json::UInt64)rSessionTimebrk.tcp_connection_time;
            jsonValue["tcp_server_time"] = (Json::UInt64)rSessionTimebrk.tcp_server_time;
            jsonValue["tcp_exchange_size"] = (Json::UInt64)rSessionTimebrk.tcp_exchange_size;

            jsonValue["ssl_requests"] = (Json::UInt64)rSessionTimebrk.ssl_requests;
            jsonValue["ssl_success_connections"] = (Json::UInt64)rSessionTimebrk.ssl_success_connections;
            jsonValue["ssl_shake_time"] = (Json::UInt64)rSessionTimebrk.ssl_shake_time;

            jsonValue["http_request_num"] = (Json::UInt64)rSessionTimebrk.http_request_num;
            jsonValue["http_success_responses"] = (Json::UInt64)rSessionTimebrk.http_success_responses;
            jsonValue["http_network_time"] = (Json::UInt64)rSessionTimebrk.http_network_time;
            jsonValue["http_server_time"] = (Json::UInt64)rSessionTimebrk.http_server_time;
            jsonValue["http_download_time"] = (Json::UInt64)rSessionTimebrk.http_download_time;
            jsonValue["http_download_size"] = (Json::UInt64)rSessionTimebrk.http_download_size;

        }


    public:

        static redisContext* initRedisContext( const std::string& redisSrvIp, uint16_t redisSrvPort )
        {
            return RedisClient::myRedisConnect(redisSrvIp, redisSrvPort);
        }

        static redisAsyncContext* initAsyncRedisContext( const std::string& redisSrvIp, uint16_t redisSrvPort )
        {
            return RedisClient::myRedisAsyncConnect(redisSrvIp, redisSrvPort);
        }

		static void redisReConnect (redisContext** ppRedisCtx, const std::string& redisSrvIp, uint16_t redisSrvPort) 
        {
            redisFree(*ppRedisCtx);
            *ppRedisCtx = RedisClient::myRedisConnect(redisSrvIp, redisSrvPort);
            LOG4CPLUS_INFO(*gpLogger, "RedisCtx re-connected");
        }


		/*
		 * int getThresholdsInfo(thresholdsInfo& rThresholdsInfo, int collector_id, int serviceId)
		 *
		 * use by worker
		 *
		 * redis keys: rum:service_thres_map:collector_id
		 * 
		 */
		static int getThresholdsInfo(redisContext* context, thresholdsInfo& rThresholdsInfo, int collectorId, int serviceId)
        {
            redisReply* reply = NULL;

            reply = (redisReply*)redisCommand(context,"PING");
            if (reply != NULL)
            {
                freeReplyObject(reply);
                reply = NULL;
            }
            else
            {
                LOG4CPLUS_ERROR(*gpLogger, "Failed to ping Redis Server, Redis Server will be reconnected!");
                return -2;
            }

            Json::Reader reader;
            Json::Value root;
            
            reply = (redisReply*)redisCommand(context, "HGET rum:service_thres_map:collector_%d %d", collectorId, serviceId);

            if(reply->type==REDIS_REPLY_STRING){
                LOG4CPLUS_DEBUG(*gpLogger, "reply->str =  " << reply->str);
                if (reader.parse(reply->str, root))
                {
                    rThresholdsInfo.performanceWarning = root["performance_warning"].asUInt64();
                    rThresholdsInfo.performanceCritical = root["performance_critical"].asUInt64();
                }
                else
                {
                    LOG4CPLUS_ERROR(*gpLogger, "Failed to parse reply object returns from 'rum:service_thres_map:collector_" << collectorId << "' with service_id=" <<  serviceId);
                    return -1;
                }

            }
            else
            {
                LOG4CPLUS_ERROR(*gpLogger, "Failed to get reply object from 'rum:service_thres_map:collector_x'");
                freeReplyObject(reply);
                return -1;
            }
            freeReplyObject(reply);
            return 0;
        }


		/*
		 * int getSessionTimebrk(redisContext* context, rum_session_timebrk_t& rSessionTimebrk, uint32_t timeout);
		 *
		 * use by dumper_main
		 *
		 * redis keys: rum:list_session_timebrk
		 * 
		 */

        static int getSessionTimebrk(redisContext* context, rum_session_timebrk_t& rSessionTimebrk, uint32_t timeout)
        {
            int myRet = 0;
            redisReply* reply = NULL;

            reply = (redisReply*)redisCommand(context,"PING");
            if (reply != NULL)
            {
                freeReplyObject(reply);
                reply = NULL;
            }
            else
            {
                LOG4CPLUS_ERROR(*gpLogger, "Failed to ping Redis Server, Redis Server will be reconnected!");
                return -2;
            }

            reply = (redisReply*)redisCommand(context,"BLPOP rum:list_session_timebrk %u", timeout );
            if (reply == NULL) {
                return -1;
            }

            if(reply->type == REDIS_REPLY_ARRAY)
            {
                LOG4CPLUS_DEBUG(*gpLogger, "reply->element[0]: " << reply->element[0]->str);
                LOG4CPLUS_DEBUG(*gpLogger, "reply->element[1]: " << reply->element[1]->str);
                myRet = RedisClient::json2SessionTimebrk(reply->element[1]->str, rSessionTimebrk);
                if( myRet == 0 )            //success to json value
                {
                    myRet = 1;
                }
                else
                {
                    myRet = -1;
                    LOG4CPLUS_ERROR(*gpLogger, "Failed to json parse SessionTimebrk");
                }
            }
            else if( reply->type == REDIS_REPLY_ERROR )
            {
                //LOG(ERROR) << "Invalid reply from rum:list_session_timebrk: " << reply->str;
                myRet = 0;
            }

			freeReplyObject(reply);
            return myRet;
        }

        static int getSessionEvent(redisContext* context, rum_session_event_t& rSessionEvent)//, uint32_t timeout)
        {
            int myRet = 0;
            redisReply* reply = NULL;

            reply = (redisReply*)redisCommand(context,"LPOP rum:list_session_event");
            if (reply == NULL) {
                //LOG(INFO) << "failed to get session_event from rum:list_session_event!";
                return -1;
            }

            if(reply->type == REDIS_REPLY_ARRAY)
            {
                LOG4CPLUS_DEBUG(*gpLogger, "reply->element[0]: " << reply->element[0]->str);
                LOG4CPLUS_DEBUG(*gpLogger, "reply->element[1]: " << reply->element[1]->str);
                myRet = RedisClient::json2SessionEvent(reply->element[1]->str, rSessionEvent);
                if( myRet == 0 )            //success to json value
                {
                    myRet = 1;
                }
                else
                {
                    myRet = -1;
                    LOG4CPLUS_ERROR(*gpLogger, "Failed to json SessionEvent");
                }
            }
            else if( reply->type == REDIS_REPLY_ERROR )
            {
                //LOG(INFO) << "failed to get session_event from rum:list_session_event: " << reply->str;
                myRet = -1;
            }

			freeReplyObject(reply);
            return myRet;
        }

		/*
		 * writeRealtimePerformance(redisContext* context, const RumSessionHashlist& resultHashList)
		 *
		 *
		 * redis keys: rum:service_realtime_performance
		 * 
		 * 
		 */
        static int writeRealtimePerformance(redisContext* context, RumSessionHashlist& resultHashList)
        {

            //int myRet = 0;
            redisReply* reply = NULL;

            reply = (redisReply*)redisCommand(context,"PING");
            if (reply != NULL)
            {
                freeReplyObject(reply);
                reply = NULL;
            }
            else
            {
                LOG4CPLUS_ERROR(*gpLogger, "Failed to ping Redis Server, Redis Server will be reconnected!");
                return -2;
            }

            vector<uint32_t> idArray;

            resultHashList.GetIdArray(idArray);
            vector<uint32_t>::iterator idIter;
            LOG4CPLUS_DEBUG(*gpLogger, "idvec size: " << idArray.size());
            for ( idIter = idArray.begin() ; idIter != idArray.end() ; idIter++ )
            {
                Json::Value root;
                Json::FastWriter writer;
                uint32_t collectorId = *idIter;

                LOG4CPLUS_DEBUG(*gpLogger, "collectorId: " << *idIter);
                list<rum_session_timebrk_t>& rTimebrkList = resultHashList.GetTimebrkListById(collectorId);
                //TODO: if size of list is 0
                
                list<rum_session_timebrk_t>::iterator listIter;
                for( listIter=rTimebrkList.begin(); listIter != rTimebrkList.end(); listIter++)
                {
                    Json::Value jsonValue;
                    sessionTimebrk2Json(jsonValue, *listIter);
                    char serviceId[100] = {0};

                    snprintf(serviceId, sizeof(uint32_t), "%d", listIter->service_id);

                    LOG4CPLUS_DEBUG(*gpLogger, "Service_id: " << serviceId);
                    root[serviceId] = jsonValue;
                    redisAppendCommand(context, "HSET rum:service_realtime_performance:collector_%d %d %s", collectorId, listIter->service_id, (writer.write(jsonValue)).c_str());
                    LOG4CPLUS_DEBUG(*gpLogger, "Set " << writer.write(jsonValue) << " to service_realtime_performance");
                }
                for (size_t ix = 0; ix < rTimebrkList.size(); ++ix)
                {
                    redisGetReply(context,(void**)&reply); // reply for HSET

                    if(reply->type==REDIS_REPLY_ERROR){
                        LOG4CPLUS_ERROR(*gpLogger, "Failed to set result to rum:service_realtime_performance:collector_" << collectorId);
                        LOG4CPLUS_ERROR(*gpLogger, "with reason: " << reply->str);
                        freeReplyObject(reply);
                        return -1;
                    }
                    freeReplyObject(reply);
                }

                /*reply = (redisReply*)redisCommand(context, "HGET rum:service_realtime_performance:collector_%d 1", collectorId);
                if(reply->type==REDIS_REPLY_STRING)
                {
                    LOG4CPLUS_DEBUG(*gpLogger, "reply->str: " << reply->str);
                }
                freeReplyObject(reply);
                */
            }

            return 0;
        }

};

#endif
