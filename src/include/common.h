#ifndef __COMMON_H__
#define __COMMON_H__

#include <map>
#include <list>
#include <stdint.h>
#include <time.h>

#define RUM_DUMPER_CONFIG_FILE                  "/etc/rum/rum-server.conf"
#define RUM_IP_NAME_LEN                         128
#define RUM_TYPE_PORT_LEN                       32
#define RUM_DATA_LEN                            2048 

#define RUM_DUMPER_MSG_UNKNOWN                  0
#define RUM_DUMPER_MSG_SESSION_EVENT            1
#define RUM_DUMPER_MSG_SESSION_TIMEBRK          2
#define RUM_DUMPER_MSG_TASK_READY               3
#define RUM_DUMPER_TIMEOUT                      4

#define RUM_FIVE_MINUTES_SECS                   300
#define RUM_ONE_HOUR_SECS                       3600
#define RUM_EIGHT_HOUR_SECS                     28800
#define RUM_ONE_DAY_SECS                        86400

#define RUM_DUMPER_MAX(a, b)                    ((a) > (b) ? (a) : (b))
#define RUM_SESSION_TIMEBRK_LEN                 sizeof(rum_session_timebrk_t)
#define RUM_SESSION_EVENT_LEN                   sizeof(rum_session_event_t)
#define RUM_DUMPER_MSG_DATA_LEN                 RUM_DUMPER_MAX(RUM_SESSION_TIMEBRK_LEN, RUM_SESSION_EVENT_LEN)
#define RUM_DUMPER_MSG_LEN                      sizeof(dumperMsgInfo)
#define RUM_ID_LEN                              sizeof(uint32_t)

#define RUM_DUMPER_DISPATCH_PAIR_CONNECTION     "inproc://dumperDispatcherSchedulerPAIR"
#define RUM_DUMPER_REALTIME_PAIR_CONNECTION     "inproc://dumperRealTimeComputingPAIR"
#define RUM_DUMPER_EVENT_CONNECTION             "inproc://dumperEventPushPull"
#define RUM_DUMPER_TIMEBRK_CONNECTION           "inproc://dumperTimeBrkPushPull"
#define RUM_DUMPER_TASK_READY_CONNECTION        "inproc://dumperTaskReadyPushPull"
#define RUM_DUMPER_TASK_FINISH_CONNECTION       "inproc://dumperTaskFinishPushPull"
#define RUM_DUMPER_READ_SESSION_TIMEBRK         "tcp://0.0.0.0:6556"

#define RUM_PERFORMANCE_SLA_RED                 "SLA:100"
#define RUM_AVAILABILITY_SLA_RED                "SLA:101"
/*#define RUM_EVENT_TYPE_SLA                      "SLA"
#define RUM_EVENT_TYPE_TCP                      "TCP"
#define RUM_EVENT_TYPE_SSL                      "SSL"
#define RUM_EVENT_TYPE_HTTP                     "HTTP"
*/

#define RUM_SERVICE_TYPE_TCP                    1
#define RUM_SERVICE_TYPE_TCPSSL                 2
#define RUM_SERVICE_TYPE_HTTP                   3
#define RUM_SERVICE_TYPE_HTTPS                  4

#define RUM_EVENT_TYPE_SLA                      1 
#define RUM_EVENT_TYPE_TCP                      2 
#define RUM_EVENT_TYPE_SSL                      3 
#define RUM_EVENT_TYPE_HTTP                     4 

#define FLAG_OUTDATED_TIMEBRK                   1
#define FLAG_TIMER_NOTIFICATION                 2

typedef struct {                                         /* all the following fields are host byte order */    
    uint32_t    flag;                                    /* 1 for OUTDATED_TIMEBRK, 2 for TIMER_NOTIFICATION*/
    uint32_t    collector_id;                            /**< Collector id */
    uint32_t    service_id;                              /**< Service id */    
    time_t      start_time;                              /**< The first timestamp of session breakdown */    
    time_t      end_time;                                /**< The last timestamp of session breakdown */    
    uint32_t    service_type;                            /**< Service type, TCP, TCP-SSL, HTTP, HTTPS,etc */    
    char        source_ip[RUM_IP_NAME_LEN];              /**< Source ip */    
    uint16_t    source_port;                             /**< Source port */
    //uint16_t    tcp_status_code;                         /**< TCP status code */
    uint64_t    tcp_retry_num;                           /**< Tcp connection retry counts */    
    uint64_t    tcp_retry_time;                          /**< Tcp retry time */    
    uint64_t    tcp_connection_requests;                 /**< Total Tcp connection request counts */    
    uint64_t    tcp_success_connections;                 /**< Total Tcp success connections */    
    uint64_t    tcp_connection_time;                     /**< Tcp connection time */    
    uint64_t    tcp_server_time;                         /**< Tcp server time */    
    uint64_t    tcp_exchange_size;                       /**< Tcp throughput */

    uint64_t    ssl_requests;                            /**< Total SSL request counts */    
    uint64_t    ssl_success_connections;                 /**< Total SSL successful connections */    
    uint64_t    ssl_shake_time;                          /**< SSL shake duration time */

    //uint64_t    authentication_requests;               /**< SQL authentication request counts */
    //uint64_t    success_authentications;               /**< Authenticated counts */
    //uint64_t    authentication_time;                   /**< Authenticated duration time */

    uint16_t    http_status_code;                        /**< Http status code */
    char        http_request_url[RUM_DATA_LEN];          /**< Http service url */    
    char        http_refer_url[RUM_DATA_LEN];            /**< Http refer url */    
    uint64_t    http_request_num;                        /**< Total HTTP GET/POST/HEAD/PUT request counts */    
    uint64_t    http_success_responses;                  /**< Total HTTP GET/POST/HEAD/PUT successful response counts */    
    uint64_t    http_network_time;                       /**< Network time to first buffer */    
    uint64_t    http_server_time;                        /**< Server time to first buffer */    
    uint64_t    http_download_time;                      /**< Http/SQL response data download time */    
    uint64_t    http_download_size;                      /**< Http/SQL response data size */    

    /*
       MSGPACK_DEFINE(flag, collector_id, service_id, start_time,
       end_time, service_type, source_ip, source_port, tcp_retry_num,
       tcp_connection_requests, tcp_success_connections,tcp_connection_time,
       tcp_server_time, tcp_exchange_size, ssl_requests, ssl_success_connections,
       ssl_shake_time, http_request_url, http_refer_url, http_request_num,
       http_success_responses, http_network_time, http_server_time, http_download_time);
    */

    template<class Archive>
    void serialize( Archive & ar, const unsigned int version)
    {
            ar & flag ;
            ar & collector_id;
            ar & service_id;
            ar & start_time;
            ar & end_time;
            ar & service_type;
            ar & source_ip;
            ar & source_port;
            //ar & tcp_status_code;
            ar & tcp_retry_num;
            ar & tcp_connection_requests;
            ar & tcp_success_connections;
            ar & tcp_connection_time;
            ar & tcp_server_time;
            ar & tcp_exchange_size;
            ar & ssl_requests;
            ar & ssl_success_connections;
            ar & ssl_shake_time;
            ar & http_status_code;
            ar & http_request_url;
            ar & http_refer_url;
            ar & http_request_num;
            ar & http_success_responses;
            ar & http_network_time;
            ar & http_server_time;
            ar & http_download_time;
            ar & http_download_size;
    }

}rum_session_timebrk_t;

typedef struct{    
    uint32_t    collector_id;                           /**< Collector id */    
    uint32_t    service_id;                             /**< Service id */    
    time_t      event_time;                             /**< Timestamp of event, equal to end_time in session_timebrk */    
    uint32_t    event_type;                             /**< "TCP", "SSL", "HTTP", etc */    
    char        event_name[RUM_IP_NAME_LEN];            /**< ICMP:301, or HTTP:404 */    
    char        source_ip[RUM_IP_NAME_LEN];             /**< 192.168.1.1 */    
    uint16_t    source_port;                            /**< 80, host byte order */    
    char        http_request_url[RUM_DATA_LEN];          /**< Http service url */    
    char        http_refer_url[RUM_DATA_LEN];            /**< Http refer url */    

    uint64_t    response_time;                          /**< Session total response time */    
    uint64_t    tcp_retry_time;                         /**< Tcp retry time */    
    uint64_t    tcp_connection_time;                    /**< Tcp connection time */    
    uint64_t    tcp_server_time;                        /**< Tcp server time */    
    uint64_t    http_network_time;                      /**< Network time to first buffer */    
    uint64_t    http_server_time;                       /**< Server time to first buffer */    
    uint64_t    http_download_time;                      /**< Http/SQL response data download time */    

    //uint64_t    performance_warning;                    /**< Warning threshold for response time */    
    uint64_t    performance_critical;                   /**< Critical threshold for response time */
}rum_session_event_t;

typedef std::map< uint32_t, std::list<rum_session_timebrk_t> > SessionHashlist; 
typedef SessionHashlist* SessionHashlistPtr; 

#endif /* __RUM_DUMPER_COMMON_H__ */
