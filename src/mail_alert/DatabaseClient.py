# encoding=UTF-8
"""
Created on 2012-3-28
@author: 永刚
"""

from CommonModules import LogClient
import psycopg2, psycopg2.extras
import time


class DatabaseClient:
    def __init__(self, db_host = "127.0.0.1", db_port = 5432, db_user = 'postgres', db_passwd = '', logger = None, db_name = 'cmdb'):
        self.server_ip = db_host
        self.server_port = db_port
        self.db_user = db_user
        self.db_password = db_passwd

        self.errmsg=""
        self.conn = None
        if logger == None :
            self.logger = LogClient("db.log")
        else :
            self.logger  = logger

        try:
            self.conn = psycopg2.connect(database = db_name, host=db_host, port=db_port, user=db_user, password=db_passwd)
        except Exception, e:
            self.logger.error("failed to initilize database")
            self.errmsg = str(e)
            self.logger.error("with reason: %s" % self.errmsg)
            return None
#######

    def getErrMsg(self) :
        return self.errmsg

#######

    def isConnected(self) :
        try :
            pid = self.conn.get_backend_pid()
            return True
        except Exception, e:
            self.logger.info("conn object have been destroyed")
            self.logger.info("with reason: %s" % str(e))
            return False

    def confirmConnected(self) :
        if self.isConnected() :
            return True
        else :
            while True:
                self.logger.warning("conn object disconnnected")
                try :
                    self.conn = psycopg2.connect(database = "cmdb", host=self.server_ip, port=self.server_port, user=self.db_user, password=self.db_password)
                    return True

                except Exception, e:
                    self.logger.error("Failed to reconnect DB")
                    self.errmsg = str(e)
                    self.logger.error("with reason = %s" % self.errmsg)
                    time.sleep(2)
                    continue

######

    def getServiceList:
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

######

    def __del__(self) :
        if self.conn != None :
            self.conn.close()
