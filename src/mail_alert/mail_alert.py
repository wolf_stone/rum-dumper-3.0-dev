#!/usr/bin/env python
# -*- coding: utf-8 -*-

from configobj import ConfigObj
import smtplib, time
from email.mime.text import MIMEText
import psycopg2, psycopg2.extras
from decimal import Decimal, getcontext
import logging
from datetime import datetime

'''
#mailto_list=["hhincq@163.com","hao_han@beilizhu.com"]
mailto_list="hao_han@beilizhu.com"
mail_host="smtp.ym.163.com"
mail_user="mail_test"
mail_pass="111111"
mail_postfix="beilizhu.com"
'''

config = ConfigObj("/etc/rum/rum-server.conf")
mailto_list = config['mailto_list']
mail_host = config['mail_host']
mail_user = config['mail_user']
mail_pass = config['mail_pass']
mail_postfix = config['mail_postfix']

db_host = config['db_host']
db_port = config['db_port']
db_user = config['db_user']
db_passwd = config['db_passwd']
db_name = config['db_name']

lang = config['lang']
alert_period = int(config['alert_period'])

attributeList = {}

if lang == 'zh':
    attributeList['start_time']         =   unicode("起始时间:        ", "utf-8")
    attributeList['end_time']           =   unicode("结束时间:        ", "utf-8")
    attributeList['service_name']       =   unicode("服务名:          ", "utf-8")
    attributeList['server_host']        =   unicode("服务器IP地址:    ", "utf-8")
    attributeList['service_port']       =   unicode("端口号:          ", "utf-8")
    attributeList['service_type']       =   unicode("服务类型:        ", "utf-8")
    attributeList['service_url']        =   unicode("URL:             ", "utf-8")
    attributeList['response_time']      =   unicode("响应时间:        ", "utf-8")
    attributeList['performance_cirital']  =   unicode("响应时间上阈值:  ", "utf-8")
    attributeList['availability']       =   unicode("可用性:          ", "utf-8")
    attributeList['availability_critical'] =   unicode("可用性下阈值:    ", "utf-8")
    attributeList['total_visits']       =   unicode("总访问量:        ", "utf-8")
    attributeList['time_metric']        =   unicode("毫秒", "utf-8")
else :
    attributeList['start_time']         =   "Start Time:                    "
    attributeList['end_time']           =   "End Time:                      "
    attributeList['service_name']       =   "Service Name:                  "
    attributeList['server_host']        =   "Server IP:                     "
    attributeList['service_port']       =   "Port:                          "
    attributeList['service_type']       =   "Service Type:                  "
    attributeList['service_url']        =   "URL:                           "
    attributeList['response_time']      =   "Response Time:                 "
    attributeList['performance_cirital']  =   "Response Time Upper Threshold: "
    attributeList['availability']       =   "Availability:                  "
    attributeList['availability_critical'] =   "Availability Lower Threshold:  "
    attributeList['total_visits']       =   "Total Visits:                  "
    attributeList['time_metric']        =   "ms"


def send_mail(to_list,sub,content):
    '''
    to_list: send to who
    sub: mail subject
    content: mail body
    send_mail("aaa@126.com", "test mail", "This is test mail"
    '''

    me=mail_user+"<"+mail_user+"@"+mail_postfix+">"
    msg = MIMEText(_text=content, _charset="utf-8")

    msg['Subject'] = sub
    msg['From'] = me
    msg['To'] = ";".join(to_list)
    try:
        s = smtplib.SMTP()
        s.connect(mail_host)
        s.login(mail_user+"@"+mail_postfix,mail_pass)
        s.sendmail(me, to_list, msg.as_string())
        s.close()
        return True
    except Exception, e:
        print str(e)
        return False

def createEvents (t_start, t_end):
    try:
        conn = psycopg2.connect(database = db_name, host=db_host, port=db_port, user=db_user, password=db_passwd)
        #conn = psycopg2.connect("database = db_name, host=db_host, port=db_port, user=db_user",cursor_factory=DictCursor)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    except Exception, e:
        logging.error("failed to initilize database")
        logging.error("with reason: %s" % self.errmsg)
        return None

    eventInfo = ""
    isInitialized = False

    cursor.execute('select * from rd_service_db')
    serviceList = cursor.fetchall()

    for serviceItem in serviceList :
        responseTime = 0
        serviceIsInitialized = False
        totalVisits = 0

        cursor.execute('select SUM(retry_num) AS retry_num, SUM(retry_time) AS retry_time, SUM(connection_requests) AS connection_requests, SUM(success_connections) AS success_connections, SUM(connection_time) AS connection_time, SUM(server_time) AS server_time, SUM(request_num) AS request_num, SUM(success_responses) AS success_responses, SUM(server_time2first_buffer) AS server_time2first_buffer, SUM(download_time) AS download_time from rd_session_time_breakdown_minute where service_id=(%s) and end_time > (%s) and end_time <= (%s)', (serviceItem['service_id'], t_start, t_end))
        timebrk = cursor.fetchone()

        if timebrk['connection_requests'] == None:
            continue

        if timebrk['retry_num'] == 0 :
            timebrk['retry_num'] = 1
        responseTime = timebrk['retry_time'] / timebrk['retry_num']

        if timebrk['success_connections'] > 0 :
            responseTime = responseTime + timebrk['connection_time'] / timebrk['success_connections']

            if serviceItem['service_type'] == 'TCP':
                responseTime = responseTime + timebrk['server_time'] / timebrk['success_connections']

            else:
                if timebrk['success_responses'] > 0 :
                    responseTime = responseTime + (timebrk['server_time2first_buffer'] + timebrk['download_time']) / timebrk['success_responses']

        #getcontext().prec = 3
        responseTime = Decimal(responseTime)

        getcontext().prec = 2
        if serviceItem['service_type'] == 'TCP':
            if timebrk['connection_requests'] > 0 :
                availability = float(timebrk['success_connections']) / float(timebrk['connection_requests']) * 100
                availability = round(availability,2)
                totalVisits = timebrk['connection_requests']
            else :
                availability = 0

        else:
            if timebrk['request_num'] > 0 :
                timebrk['success_responses'] = 150
                availability = float(timebrk['success_responses']) / float(timebrk['request_num']) * 100
                availability = round(availability,2)
                totalVisits = timebrk['request_num']
            else :
                availability = 0


        if responseTime > Decimal(str(serviceItem['performance_cirital'])) :

            if isInitialized == False:
                isInitialized  = True

                eventInfo = eventInfo + "%s " % attributeList['start_time'] + "%s\n" % time.ctime(t_start)
                eventInfo = eventInfo + "%s " % attributeList['end_time'] + "%s\n\n" % time.ctime(t_end)

            if serviceIsInitialized == False:
                serviceIsInitialized = True
                eventInfo = eventInfo + "%s " % attributeList['service_name'] + "%s\n" % serviceItem['service_name']
                eventInfo = eventInfo + "%s " % attributeList['server_host'] + "%s\n" % serviceItem['service_ip']
                eventInfo = eventInfo + "%s " % attributeList['service_port'] + "%s\n" % serviceItem['service_port']
                eventInfo = eventInfo + "%s " % attributeList['service_type'] + "%s\n" % serviceItem['service_type']
                if serviceItem['service_type'] == 'HTTP':
                    eventInfo = eventInfo + "%s " % attributeList['service_url'] + "%s\n" % serviceItem['service_url']

                eventInfo = eventInfo + "%s " % attributeList['total_visits'] + "%s\n" % totalVisits

            eventInfo = eventInfo + "%s " % attributeList['response_time'] + "%s " % responseTime + "(%s)\n" % attributeList['time_metric']
            eventInfo = eventInfo + "%s " % attributeList['performance_cirital'] + "%s " % serviceItem['performance_cirital'] + "(%s)\n" % attributeList['time_metric']


        if availability < serviceItem['availability_critical'] :
            if isInitialized == False:
                isInitialized  = True
                eventInfo = eventInfo + "%s " % attributeList['start_time'] + "%s\n" % time.ctime(t_start)
                eventInfo = eventInfo + "%s " % attributeList['end_time'] + "%s\n\n" % time.ctime(t_end)
                #eventInfo = eventInfo + "\n"

            if serviceIsInitialized == False:
                serviceIsInitialized = True
                eventInfo = eventInfo + "%s " % attributeList['service_name'] + "%s\n" % serviceItem['service_name']
                eventInfo = eventInfo + "%s " % attributeList['server_host'] + "%s\n" % serviceItem['service_ip']
                eventInfo = eventInfo + "%s " % attributeList['service_port'] + "%s\n" % serviceItem['service_port']
                eventInfo = eventInfo + "%s " % attributeList['service_type'] + "%s\n" % serviceItem['service_type']
                if serviceItem['service_type'] == 'HTTP':
                    eventInfo = eventInfo + "%s " % attributeList['service_url'] + "%s\n" % serviceItem['service_url']

                eventInfo = eventInfo + "%s " % attributeList['total_visits'] + "%s\n" % totalVisits

            eventInfo = eventInfo + "%s " % attributeList['availability'] + "%s%%\n" % availability
            eventInfo = eventInfo + "%s " % attributeList['availability_critical'] + "%s%%\n" % serviceItem['availability_critical']


        eventInfo = eventInfo + "\n"

    return eventInfo

if __name__ == '__main__':

    t_now = datetime.today()
    t_current = time.mktime(t_now.timetuple())

    while(1):
        time.sleep(alert_period)
        t_end = t_current + alert_period

        eventInfo = createEvents(t_current, t_end)
        print eventInfo
        t_current = t_end

        if eventInfo == "" or eventInfo == None:
            continue

        if send_mail(mailto_list,"test mail",eventInfo):
            print "Success"
        else:
            print "Failed"
